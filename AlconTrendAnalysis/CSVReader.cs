﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Diagnostics;
using AlconTrendAnalysis.Data;

namespace AlconTrendAnalysis
{
    public class CSVReader
    {
        #region -- Tag Descriptions ----------------------------------------------------------------------------------
        private static Dictionary<string, string> TagToDesc;
        public static void LoadTagDescriptions()
        {
            TagToDesc = new Dictionary<string, string>();
            string[] lines = Properties.Resources.tag_to_desc.Split('\n');

            foreach (var line in lines)
            {
                var split = line.Replace("\r", "").Split(',');

                if (string.IsNullOrWhiteSpace(split[0]) || string.IsNullOrWhiteSpace(split[1]) ||
                    (split[0] == "Tag key" && split[1] == "Description"))
                    continue;

                if (!TagToDesc.ContainsKey(split[0].ToUpper()))
                    TagToDesc.Add(split[0].ToUpper(), split[1]);
            }
        }
        public static string GetTagDescription(string tagName)
        {
            if (TagToDesc == null || TagToDesc.Count < 1)
                LoadTagDescriptions();

            string token = string.Empty;

            if (tagName.Contains("PP1T1") || tagName.Contains("PP1T2"))
            {
                token = string.Format("PP_{0}", tagName.Substring(12, tagName.Length - 16)).ToUpper();
            }
            else if (tagName.Contains("DS"))
            {
                token = tagName.Substring(9, tagName.Length - 13).ToUpper();
            }
            else
                token = tagName.Substring(9, tagName.Length - 13).ToUpper();

            if (TagToDesc.ContainsKey(token))
                return TagToDesc[token];
            else
                return string.Empty;
        }
        #endregion
    }
}
