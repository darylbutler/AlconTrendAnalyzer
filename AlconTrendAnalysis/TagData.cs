﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlconTrendAnalysis
{
    [Serializable]
	public class TagData
	{
        public string Name, Description;

        // Description formatted to show which machine the tag is from
        public string GroupName
        {
            get
            {
                return string.Format("{0} - {1}", Machine, Description);
            }
        }
        public string Machine
        {
            get
            {
                if (Name.Contains("PP1T"))
                   return "PP";
                else
                    return Name.Substring(9, 2);
            }
        }
		public long ID;
		public List<TagValue> Values = new List<TagValue>();
    }
}
