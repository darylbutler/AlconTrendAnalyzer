﻿using System;
using System.Collections.Generic;

namespace AlconTrendAnalysis.Data
{
    public class DataStoreProvider : IDataProvider
    {
        string storePath = string.Empty;

        public void StartUp(string path)
        {
            if (string.IsNullOrWhiteSpace(path) || !System.IO.File.Exists(path))
                throw new ArgumentException("Supplied path doesn't exist");

            storePath = path;
        }

        public List<TagData> GetAllTags()
        {
            if (storePath == string.Empty)
                throw new Exception("DataStore queried before startup!");

            DataStore store = new DataStore(storePath, StoreAccess.Query);
            return store.Tags;
        }

        public List<TagData> Query(string tag, ulong startTime, ulong endTime)
        {
            if (storePath == string.Empty)
                throw new Exception("DataStore queried before startup!");

            try
            {
                // reverse the times if they're out of order
                if (startTime < endTime)
                    return new List<TagData> { DataStore.QueryTagThreaded(storePath, tag, startTime, endTime) };
                else
                    return new List<TagData> { DataStore.QueryTag(storePath, tag, endTime, startTime) };
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Tuple<ulong, ulong> GetDatabaseTimeRange()
        {
            return DataStore.QueryLatestTimeRange(storePath);
        }


    }
}
