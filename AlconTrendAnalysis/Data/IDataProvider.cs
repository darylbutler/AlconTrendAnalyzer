﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlconTrendAnalysis.Data
{
    /**
     * Different ways for us to get the data
     */
    public interface IDataProvider
    {
        // Allows for Maintenance, keep time short but can be async
        void StartUp(string path);  // Called by UI app when application is starting

        // Main query entry point
        List<TagData> Query(    string tag,         // Tag name to query
                                ulong startTime,    // INCLUSIVE: Minimum time of results
                                ulong endTime);     // INCLUSIVE: Maximum time of results

        List<TagData> GetAllTags(); // Gets the tags from the latest files

        Tuple<ulong, ulong> GetDatabaseTimeRange();
    }
}
