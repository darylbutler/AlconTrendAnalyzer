﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AlconTrendAnalysis.Data
{
    public class CachedStoreProvider : IDataProvider
    {
        const bool DebuggingCache = false;
        const bool Persist = true;
        const ulong MaxCacheFillTime = 12 * 60 * 2; // 12 == 1m
        const ulong MaxStoreTime = (/* 1 day */24 * 60 * 60) * 7;   // 7 days    
        const string CachePersistPath = "local_store.cache";

        public bool Saving, Dirty;

        private string storePath = string.Empty;
        private List<string> TagOrder = new List<string>();
        private List<StoreRow> Cache = new List<StoreRow>();
        private ulong FirstCachedTime = 0, LastCachedTime = 0;

        // -- Interface
        public void StartUp(string path) {
            if (string.IsNullOrWhiteSpace(path) || !System.IO.File.Exists(path))
                throw new ArgumentException("Supplied path doesn't exist");

            storePath = path;
        }
        public List<TagData> GetAllTags() {
            if (storePath == string.Empty)
                throw new Exception("DataStore queried before startup!");

            return new DataStore(storePath, StoreAccess.Query).Tags;
        }
        public List<TagData> Query(string tag, ulong startTime, ulong endTime) {
            if (storePath == string.Empty)
                throw new Exception("DataStore queried before startup!");

            try {
                var store = GetFromCache(startTime, endTime);

                if (store == null || store.Rows?.Count < 1) {

                    return null;
                }
                return new List<TagData> { GetTagDataFromStore(tag, store) };
            }
            catch (Exception) {
                return null;
            }
        }
        public Tuple<ulong, ulong> GetDatabaseTimeRange() {
            return DataStore.QueryLatestTimeRange(storePath);
        }
        public void ClearCache() {
            Cache = new List<StoreRow>();
            Save();
        }

        // -- Cache Part
        private TagData GetTagDataFromStore(string tag, Store store) {
            int col = TagOrder.IndexOf(tag);

            if (col < 0)
                return new TagData() { Name = tag };

            TagData data = new TagData() { Name = tag };
            foreach (var row in store.Rows) {
                if (col < row.Values.Count)
                    data.Values.Add(new TagValue(row.Time, row.Values[col]));
            }
            return data;
        }
        private Store GetFromCache(ulong start, ulong end) {
            // Special Case: Cache Empty
            if (Cache.Count < 1) {
                var data = DataStore.QueryRows(storePath, start, end);
                TagOrder = data.TagOrder;
                FirstCachedTime = start;
                LastCachedTime = end;
                Cache.AddRange(data.Rows);
                Dirty = true;

                if (data.Rows.Count > 3) {
                    Debug.Assert(data.Rows[0].Time < data.Rows[data.Rows.Count - 1].Time && data.Rows[0].Time < data.Rows[1].Time, "DB Result not in order!");
                }

                return data;
            }

            // Query is too far away from our cache, so don't save it
            if ((end < FirstCachedTime && end + MaxCacheFillTime < FirstCachedTime) ||
                (start > LastCachedTime && start - MaxCacheFillTime > LastCachedTime)) {
                return DataStore.QueryRows(storePath, start, end);
            }

            // Query timeframe ends before our cache begins, so Add this data to our cache head
            if (start < FirstCachedTime) {
                var data = DataStore.QueryRows(storePath, start, FirstCachedTime - 5);

                if (data?.Rows?.Count > 0) {
                    Cache.InsertRange(0, data.Rows);
                    FirstCachedTime = start;
                    Dirty = true;
                }
            }

            // Query timeframe starts after our last cached time, so add this data to the end of our cache
            if (end > LastCachedTime) {
                var data = DataStore.QueryRows(storePath, LastCachedTime + 5, end);

                if (data?.Rows?.Count > 0) {
                    Cache.AddRange(data.Rows);
                    LastCachedTime = end;
                    Dirty = true;
                }
            }

            // Now, we should contain all the data needed for this query
            var startPos = Cache.FindIndex(x => x.Time >= start);
            var endPos = Cache.FindLastIndex(x => x.Time <= end);

            return new Store() {
                TagOrder = TagOrder,
                Rows = Cache.GetRange(startPos, (endPos - startPos) + 1)
            };
        }

        //private bool Equal(List<string> l1, List<string> l2) {
        //    if (l1.Count != l2.Count) return false;

        //    for (int i = 0; i < l1.Count; i++) {
        //        if (l1[i] != l2[i])
        //            return false;
        //    }
        //    return true;
        //}
        //private void RemoveDuplicateStoreRows(Store store) {
        //    List<ulong> times = new List<ulong>(store.Rows.Count);
        //    for (int i = 0; i < store.Rows.Count; i++) {
        //        if (times.Contains(store.Rows[i].Time)) {
        //            store.Rows.RemoveAt(i);
        //            i--;
        //        }
        //    }
        //}

        // -- Persisting
        public async void Save() {
            if (!Persist || !Dirty)
                return;

            // Save Cache
            Saving = true;
            await Task.Run(async () => {
                await Task.Run(() => {
                    try {
                        WriteCache(CachePersistPath);
                    }
                    catch (Exception) { }
                    finally {
                        Saving = false;
                        Dirty = false;
                    }
                });
            });
        }
        public void Load(IProgress<double> Progress = null) {
            if (!Persist)
                return;

            try {
                ReadCache(CachePersistPath, Progress);
                Dirty = false;
            }
            catch (Exception) {
                TagOrder = new List<string>();
                Cache = new List<StoreRow>();
                Dirty = true;
            }
        }
        private void WriteCache(string path) {
            using (FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None))
            using (BinaryWriter w = new BinaryWriter(stream)) {
                w.Write(FirstCachedTime);
                w.Write(LastCachedTime);

                w.Write((UInt32)TagOrder.Count);
                foreach (var tag in TagOrder)
                    w.Write(tag);

                w.Write((UInt32)Cache.Count);
                foreach (var r in Cache) {
                    w.Write((UInt64)r.Time);
                    foreach (var v in r.Values) {
                        w.Write(v);
                    }
                }
            }
        }
        private void SaveAReadableCacheForDebugging(string path) {

            int shard = 0;
            FileStream stream = null;
            StreamWriter writer = null;

            // For Laziness
            Func<string> GetShardedPath = () => {
                return string.Format("{0}.{1}", path, shard);
            };
            Action<string> write = (t) => {
                writer.Write(t);
            };
            Action<string> writeLine = (t) => {
                writer.WriteLine(t);
            };



            // -- Data
            foreach (var row in Cache) {
                if (stream == null || stream.Length > 1000000000) {
                    if (stream != null) {
                        stream?.Flush();                        
                        writer?.Dispose();
                        stream?.Dispose();
                        shard++;                        
                    }
                    stream = new FileStream(GetShardedPath(), FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read);
                    writer = new StreamWriter(stream);

                    // -- Tags
                    writeLine(string.Join(",", TagOrder));
                }

                write(OracleTime.ToDateTime(row.Time).ToString() + ",");
                foreach (var v in row.Values) {
                    if (v == double.MaxValue) {
                        write("Null,");
                    } else {
                        write(string.Format("{0:0.0},", v));
                    }
                }
                writeLine(string.Empty);
            }
            stream?.Flush();            
            writer?.Dispose();
            stream?.Dispose();
        }
        private async void ReadCache(string path, IProgress<double> Progress = null) {
            if (!File.Exists(path)) {
                Cache = new List<StoreRow>();
                TagOrder = new List<string>();
                Dirty = true;
                return;
            }
            ulong today = OracleTime.ToLong(DateTime.Now.Date);

            await Task.Run(async () => {
                await Task.Run(() => {
                    using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                    using (BinaryReader r = new BinaryReader(stream)) {
                        try {
                            stream.Position = 0;

                            FirstCachedTime = r.ReadUInt64();
                            LastCachedTime = r.ReadUInt64();

                            // Tag Order
                            uint tagCount = r.ReadUInt32();
                            TagOrder = new List<string>((int)tagCount);
                            for (int i = 0; i < tagCount; i++)
                                TagOrder.Add(r.ReadString());

                            // Cache
                            var rowCount = r.ReadUInt32();
                            Cache = new List<StoreRow>((int)rowCount);
                            for (int i = 0; i < rowCount; ++i) {
                                StoreRow row = new StoreRow(r.ReadUInt64());
                                for (int z = 0; z < TagOrder.Count; ++z) {
                                    row.Values.Add(r.ReadDouble());
                                }

                                // Don't load old cached entries
                                if (today - row.Time < MaxStoreTime) {
                                    Cache.Add(row);
                                }
                                else {
                                    Dirty = true;   // Let us save over these thrown out values
                                }

                                // progress
                                if (i % 100 == 0 && Progress != null) {
                                    Progress.Report((double)i / (double)rowCount);
                                }
                            }
                        } catch(Exception) {
                            // Any errors at all, fail this cache and rewrite it.
                            Cache = new List<StoreRow>();
                            TagOrder = new List<string>();
                            Dirty = true;
                            return;
                        }
                    }
                });
            });
            // Ha ha, Great Success! ! !  -__-
            if (DebuggingCache) {
                await Task.Run(async () => {
                    await Task.Run(() => {
                        SaveAReadableCacheForDebugging("cache.debug");
                    });
                });
            }
        }
    }


    //internal class CacheEntry
    //{
    //    public ulong Start, End;
    //    public DateTime LastHit;
    //    public uint Hits = 0;
    //    public List<StoreRow> Rows = new List<StoreRow>();
    //}
    //public class CachedStoreProvider : IDataProvider
    //{
    //    const bool Persist = true;
    //    const string CachePersistPath = "local_store.cache";
    //    string storePath = string.Empty;
    //    public bool Saving, Dirty;
    //    List<string> TagOrder = new List<string>();
    //    List<CacheEntry> Cache = new List<CacheEntry>();

    //    // -- Interface
    //    public void StartUp(string path) {
    //        if (string.IsNullOrWhiteSpace(path) || !System.IO.File.Exists(path))
    //            throw new ArgumentException("Supplied path doesn't exist");

    //        storePath = path;

    //        // Load from persistence
    //        //Load();
    //    }
    //    public List<TagData> GetAllTags() {
    //        if (storePath == string.Empty)
    //            throw new Exception("DataStore queried before startup!");

    //        return new DataStore(storePath, StoreAccess.Query).Tags;
    //    }
    //    public List<TagData> Query(string tag, ulong startTime, ulong endTime) {
    //        if (storePath == string.Empty)
    //            throw new Exception("DataStore queried before startup!");

    //        try {
    //            var store = GetFromCache(startTime, endTime);

    //            if (store == null || store.Rows?.Count < 1) {

    //                return null;
    //            }
    //            return new List<TagData> { GetTagDataFromStore(tag, store) };
    //        }
    //        catch (Exception) {
    //            return null;
    //        }
    //    }
    //    public Tuple<ulong, ulong> GetDatabaseTimeRange() {
    //        return DataStore.QueryLatestTimeRange(storePath);
    //    }
    //    public void ClearCache() {
    //        Cache = new List<CacheEntry>();
    //        Save();
    //    }

    //    // -- Cache Part
    //    private TagData GetTagDataFromStore(string tag, Store store) {
    //        int col = store.TagOrder.IndexOf(tag);

    //        if (col < 0)
    //            return new TagData() { Name = tag };

    //        TagData data = new TagData() { Name = tag };
    //        foreach (var row in store.Rows) {
    //            if (col < row.Values.Count)
    //                data.Values.Add(new TagValue(row.Time, row.Values[col]));
    //        }
    //        return data;
    //    }
    //    private Store GetFromCache(ulong start, ulong end) {
    //        bool hit = false;
    //        bool error = false;
    //        foreach (var cacheEntry in Cache) {
    //            // ---- Full Match -------------------------------------------------------------------------------
    //            if (cacheEntry.Start <= (start + 30) && cacheEntry.End >= (end - 30)) {   // Full hit, just return the results                    
    //                cacheEntry.LastHit = DateTime.Now;
    //                return new Store() {
    //                    TagOrder = cacheEntry.Result.TagOrder,
    //                    Rows = cacheEntry.Result.Rows.Where(x => x.Time >= start && x.Time <= end).ToList()
    //                };
    //            }

    //            // ---- Partial Match ----------------------------------------------------------------------------                
    //            if (cacheEntry.Start <= start) {    // Start is in this cache, but not the end                     
    //                ulong s = cacheEntry.End;    // Start of this Cache
    //                ulong e = end;               // End of the original query
    //                var data = DataStore.QueryRows(storePath, s, e);    // Ask the store for the missing data, and add it
    //                if (!Equal(data.TagOrder, TagOrder)) {
    //                    error = true;
    //                    break;  // Error
    //                }
    //                cacheEntry.Result.Rows.AddRange(data.Rows);  // Add the data to the cache                 
    //                cacheEntry.Result.Rows.Sort((x, y) => { return x.Time.CompareTo(y.Time); });
    //                RemoveDuplicateStoreRows(cacheEntry.Result);
    //                cacheEntry.End = end;   // cacheEntry.Result.Rows.Max(x => x.Time);       // We only changed the end
    //                hit = true;
    //            }
    //            else if (cacheEntry.End >= end) {   // End is in the cache, but not the start                    
    //                ulong s = start;        // Start of the query
    //                ulong e = cacheEntry.Start;  // start of the cached data (Don't include the start we already have )
    //                var data = DataStore.QueryRows(storePath, s, e);    // Ask the store for the missing data, and add it
    //                if (!Equal(data.TagOrder, TagOrder)) {
    //                    error = true;
    //                    break;  // Error
    //                }
    //                cacheEntry.Result.Rows.AddRange(data.Rows);  // Add the data to the cache
    //                cacheEntry.Result.Rows.Sort((x, y) => { return x.Time.CompareTo(y.Time); });
    //                RemoveDuplicateStoreRows(cacheEntry.Result);
    //                cacheEntry.Start = start;   // cacheEntry.Result.Rows.Min(x => x.Time);     // We only changed the start                    
    //                hit = true;
    //            }

    //            // If we queried this cache, return the results
    //            if (hit && !error) {
    //                Dirty = true;
    //                cacheEntry.Hits++;
    //                cacheEntry.LastHit = DateTime.Now;
    //                return new Store() {
    //                    TagOrder = cacheEntry.Result.TagOrder,
    //                    Rows = cacheEntry.Result.Rows.Where(x => x.Time >= start && x.Time <= end).ToList()
    //                };
    //            }
    //        }

    //        // No Matches found, create a new Cache
    //        var d = DataStore.QueryRows(storePath, start, end);

    //        // If we need to clear / invalidate the cache, do it now
    //        if (error || Cache.Count < 1) {
    //            // Throw it all away
    //            Cache = new List<CacheEntry>();
    //            TagOrder = d.TagOrder;
    //        }

    //        // if the queried data was valid, add it to the cache
    //        if (d.Rows.Count > 0) {
    //            Cache.Add(new CacheEntry() {
    //                Start = start,
    //                End = end,
    //                Hits = 1,
    //                LastHit = DateTime.Now,
    //                Result = d
    //            });
    //            Dirty = true;
    //        }

    //        return d;
    //    }

    //    private bool Equal(List<string> l1, List<string> l2) {
    //        if (l1.Count != l2.Count) return false;

    //        for (int i = 0; i < l1.Count; i++) {
    //            if (l1[i] != l2[i])
    //                return false;
    //        }
    //        return true;
    //    }
    //    private void RemoveDuplicateStoreRows(Store store) {
    //        List<ulong> times = new List<ulong>(store.Rows.Count);
    //        for (int i = 0; i < store.Rows.Count; i++) {
    //            if (times.Contains(store.Rows[i].Time)) {
    //                store.Rows.RemoveAt(i);
    //                i--;
    //            }
    //        }
    //    }

    //    // -- Persisting
    //    public async void Save() {
    //        if (!Persist || !Dirty)
    //            return;

    //        // Save Cache
    //        Saving = true;
    //        await Task.Run(async () => {
    //            await Task.Run(() => {
    //                try {
    //                    WriteCache(CachePersistPath);
    //                }
    //                catch (Exception) { }
    //                finally {
    //                    Saving = false;
    //                    Dirty = false;
    //                }
    //            });
    //        });
    //    }
    //    public async void Load() {
    //        if (!Persist)
    //            return;

    //        try {
    //            ReadCache(CachePersistPath);
    //            Dirty = false;
    //        }
    //        catch (Exception) {
    //            TagOrder = new List<string>();
    //            Cache = new List<CacheEntry>();
    //            Dirty = true;
    //        }
    //    }
    //    private void WriteCache(string path) {
    //        using (FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None))
    //        using (BinaryWriter w = new BinaryWriter(stream)) {
    //            w.Write((UInt32)TagOrder.Count);
    //            foreach (var tag in TagOrder)
    //                w.Write(tag);
    //            w.Write((UInt32)Cache.Count);
    //            foreach (var c in Cache) {
    //                if (c.Rows.Count > 0) {
    //                    w.Write((UInt64)c.Start);
    //                    w.Write((UInt64)c.End);
    //                    w.Write((UInt32)c.Hits);
    //                    w.Write(c.LastHit.ToBinary());
    //                    w.Write((UInt32)c.Rows.Count);
    //                    foreach (var r in c.Rows) {
    //                        w.Write((UInt64)r.Time);
    //                        foreach (var v in r.Values)
    //                            w.Write(v);
    //                    }
    //                }
    //            }
    //        }
    //    }
    //    private void ReadCache(string path) {
    //        if (!File.Exists(path)) {
    //            Cache = new List<CacheEntry>();
    //            TagOrder = new List<string>();
    //            return;
    //        }

    //        using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
    //        using (BinaryReader r = new BinaryReader(stream)) {
    //            stream.Position = 0;

    //            // Tag Order
    //            uint tagCount = r.ReadUInt32();
    //            TagOrder = new List<string>((int)tagCount);
    //            for (int i = 0; i < tagCount; i++)
    //                TagOrder.Add(r.ReadString());

    //            // Cache
    //            uint cacheCnt = r.ReadUInt32();
    //            Cache = new List<CacheEntry>((int)cacheCnt);
    //            for (int i = 0; i < cacheCnt; i++) {
    //                CacheEntry e = new CacheEntry() {
    //                    Start = r.ReadUInt64(),
    //                    End = r.ReadUInt64(),
    //                    Hits = r.ReadUInt32(),
    //                    LastHit = DateTime.FromBinary(r.ReadInt64()),
    //                    Result = new Store()
    //                };
    //                uint rowsCnt = r.ReadUInt32();
    //                e.Result.TagOrder = TagOrder;
    //                e.Result.Rows = new List<StoreRow>((int)rowsCnt);
    //                for (int y = 0; y < rowsCnt; y++) {
    //                    StoreRow row = new StoreRow(r.ReadUInt64());
    //                    for (int z = 0; z < TagOrder.Count; z++)
    //                        row.Values.Add(r.ReadDouble());
    //                    e.Result.Rows.Add(row);
    //                }
    //                Cache.Add(e);
    //            }
    //        }
    //        // Ha ha, Great Success! ! !  -__-
    //    }
    //    public async void LoadCacheWithProgress(IProgress<double> Progress) {
    //        if (!File.Exists(CachePersistPath)) {
    //            Cache = new List<CacheEntry>();
    //            TagOrder = new List<string>();
    //            return;
    //        }

    //        await Task.Run(async () => {
    //            await Task.Run(() => {
    //                using (FileStream stream = new FileStream(CachePersistPath, FileMode.Open, FileAccess.Read, FileShare.Read, 4096))
    //                using (BinaryReader r = new BinaryReader(stream)) {
    //                    stream.Position = 0;

    //                    // Tag Order                
    //                    uint tagCount = r.ReadUInt32();
    //                    TagOrder = new List<string>((int)tagCount);
    //                    for (uint i = 0; i < tagCount; i++) {
    //                        TagOrder.Add(r.ReadString());
    //                    }

    //                    // Cache
    //                    uint cacheCnt = r.ReadUInt32();
    //                    Cache = new List<CacheEntry>((int)cacheCnt);
    //                    for (int i = 0; i < cacheCnt; i++) {
    //                        CacheEntry e = new CacheEntry() {
    //                            Start = r.ReadUInt64(),
    //                            End = r.ReadUInt64(),
    //                            Hits = r.ReadUInt32(),
    //                            LastHit = DateTime.FromBinary(r.ReadInt64()),
    //                            Result = new Store()
    //                        };
    //                        uint rowsCnt = r.ReadUInt32();
    //                        e.Result.TagOrder = TagOrder;
    //                        e.Result.Rows = new List<StoreRow>((int)rowsCnt);
    //                        for (int y = 0; y < rowsCnt; ++y) {
    //                            if (y % 1000 == 0 && Progress != null) {
    //                                Progress.Report((double)stream.Position / (double)stream.Length);
    //                            }
    //                            StoreRow row = new StoreRow(r.ReadUInt64());
    //                            for (int z = 0; z < TagOrder.Count; ++z) {
    //                                row.Values.Add(r.ReadDouble());
    //                            }
    //                            e.Result.Rows.Add(row);
    //                        }
    //                        Cache.Add(e);
    //                    }
    //                }
    //            });
    //        });
    //    }
    //}
}
