﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace AlconTrendAnalysis
{
    public class TestGenerateCSVs
    {
        private string Path;
        Timer Timer;
        ulong lastEditTime;
        Random rand = new Random();

        public TestGenerateCSVs(TimeSpan interval, string path)
        {
            Path = path;

            lastEditTime = OracleTime.ToLong(DateTime.Now);

            Timer = new Timer((x) =>
            {
                string filePath = System.IO.Path.Combine(path, string.Format("{0}.csv", OracleTime.ToLong(DateTime.Now)));

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filePath, false))
                {
                    // Write tag lines
                    file.WriteLine("SECTIME,APL01_PM1EM_HO6CODM_0002B1_ACV,APL01_PM1EM_HO6FLOM_0002S1_ACV");

                    while (lastEditTime < OracleTime.ToLong(DateTime.Now))
                    {
                        lastEditTime += 5;  // Add 5 seconds
                        file.Write(lastEditTime);
                        file.Write(",");

                        // Write two tag values
                        file.Write(rand.NextDouble() * 3);
                        file.Write(",");
                        file.WriteLine(rand.NextDouble() * 3);
                    }

                    file.Flush();
                }
            }, null, (int)interval.TotalMilliseconds, (int)interval.TotalMilliseconds);
        }

        public void Stop()
        {
            Timer.Dispose();
            Timer = null;
        }
    }
}
