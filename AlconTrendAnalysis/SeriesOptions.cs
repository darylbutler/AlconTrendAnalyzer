﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Wpf;

namespace AlconTrendAnalysis
{
    public class SeriesOptions
    {
        public string TagName = string.Empty;   // Index of the corresponding data in QueryData
        public bool IsTrendLine = false;        // true if this is a line created from a trend line (If true, TagName is the tag we're trending)
        public int TrendLineIndex = -1;         // Index of this tag's trend line (This should be index of tag if trendline)
    }
}
