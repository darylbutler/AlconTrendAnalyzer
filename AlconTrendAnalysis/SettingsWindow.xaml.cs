﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.IO;

namespace AlconTrendAnalysis
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public string Path;

        public SettingsWindow()
        {
            InitializeComponent();

            // Load Settings
            if (File.Exists("settings"))
            {
                using (StreamReader stream = new StreamReader("settings"))
                {
                    stream.BaseStream.Seek(0, SeekOrigin.Begin);
                    while (!stream.EndOfStream)
                    {
                        string line = stream.ReadLine();

                        string[] split = line.Split(new char[] { '=' }, 2);   // 0 == key, 1 == value

                        if (split.Length == 2)
                        {
                            switch (split[0])
                            {
                                case "path":
                                    Path = split[1];                                    
                                    break;

                                // more settings here
                            }
                        }
                    }
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Start with no paths
            PathsBox.Items.Clear();
            if (Path != string.Empty)
            {
                PathsBox.Items.Add(Path);

            }
        }        
        private bool Save()
        {
            if (PathsBox.Items.Count < 1)
                return false;

            Path = PathsBox.Items[0].ToString();
            using (StreamWriter stream = new StreamWriter("settings", false, Encoding.Default))
            {
                stream.WriteLine(string.Format("path={0}", Path));
                stream.Flush();
            }
             
            return true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // Don't save
            Close();
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            // Save, then close
            if (Save())
            {
                DialogResult = true;
                Close();
            }                
        }

        private void DatabasePathBrowseBtn_Click(object sender, RoutedEventArgs e)
        {
            using (CommonOpenFileDialog dlg = new CommonOpenFileDialog())
            {
                dlg.IsFolderPicker = false;
                dlg.EnsureFileExists = true;
                dlg.Filters.Add(new CommonFileDialogFilter("DataStore (*.store)", "*.store"));
                dlg.Filters.Add(new CommonFileDialogFilter("All (*.*)", "*.*"));
                dlg.Title = "Select the data location";
                dlg.EnsurePathExists = true;
                dlg.Multiselect = false;

                this.Topmost = false;
                if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    PathsBox.Items.Clear();
                    PathsBox.Items.Add(dlg.FileName);
                }

                this.Topmost = true;
            }
        }

        private void RemoveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (PathsBox.SelectedItem != null)
            {
                PathsBox.Items.Remove(PathsBox.SelectedItem);
            }
        }

        private void PathsBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RemoveBtn.IsEnabled = (PathsBox.SelectedItem != null);
        }
    }
}
