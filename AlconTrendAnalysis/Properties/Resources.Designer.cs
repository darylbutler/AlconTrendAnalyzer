﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AlconTrendAnalysis.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("AlconTrendAnalysis.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Actions_player_play_icon {
            get {
                object obj = ResourceManager.GetObject("Actions_player_play_icon", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap GatherCSVs {
            get {
                object obj = ResourceManager.GetObject("GatherCSVs", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap RegressionLine {
            get {
                object obj = ResourceManager.GetObject("RegressionLine", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap SavePDF {
            get {
                object obj = ResourceManager.GetObject("SavePDF", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap SavePNG {
            get {
                object obj = ResourceManager.GetObject("SavePNG", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tag Key,Description
        ///RM_RM2UB01_AbwBay,Disable Bay 1
        ///RM_RM2UB02_AbwBay,Disable Bay 2
        ///RM_RM2UB03_AbwBay,Disable Bay 3
        ///RM_RM2UB04_AbwBay,Disable Bay 4
        ///RM_RM2UB05_AbwBay,Disable Bay 5
        ///RM_RM2UB06_AbwBay,Disable Bay 6
        ///RM_RM2UB01_FileOn,Writing data file Bay 1 active
        ///RM_RM2UB02_FileOn,Writing data file Bay 2 active
        ///RM_RM2UB03_FileOn,Writing data file Bay 3 active
        ///RM_RM2UB04_FileOn,Writing data file Bay 4 active
        ///RM_RM2UB05_FileOn,Writing data file Bay 5 active
        ///RM_RM2UB06_FileOn,Writing data file Bay 6 a [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string tag_to_desc {
            get {
                return ResourceManager.GetString("tag_to_desc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap ToggleSplitter {
            get {
                object obj = ResourceManager.GetObject("ToggleSplitter", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
    }
}
