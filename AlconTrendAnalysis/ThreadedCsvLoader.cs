﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AlconTrendAnalysis
{
    /// <summary>
    /// Provides a mechanism to load many CSV files quickly by splitting up the parsing onto many different threads
    /// </summary>
    public class ThreadedFileLoader
    {

        /// <summary>
        /// Loads a bunch of CSV Files using a ThreadPool
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        public static List<TagData> LoadFiles(List<string> files, int skipLines = 0)
        {
            List<TagData> sorted = new List<TagData>();

            if (files.Count() < 1)
                return sorted;

            //Sort filesnames so values are merged in order
            files.Sort((a, b) => a.CompareTo(b) * -1);
            // preload tags to prevent threaded calls to LoadTags
            CSVReader.LoadTagDescriptions();
            // Data storage for all tags' return data
            List<TagData>[] data = new List<TagData>[files.Count];

            // Create the threads
            Task[] tasks = Enumerable.Range(0, files.Count()).Select(i =>
                Task.Run(() =>
                {
                    // Added ability to differentiate between .txt (7+ format) and .csv (6- format)
                    data[i] = files[i].ToLower().EndsWith(".csv") ?
                    (CSVReader.QueryDataCSV(files[i], null, skipLines)) : CSVReader.LoadDataTXT(files[i]);
                })

            // No signalling, no anything.
            ).ToArray();

            // Wait on all the tasks.
            Task.WaitAll(tasks);

            // Single Threaded
            //int i = 0;
            //foreach (var file in files)
            //{
            //    data[i] = (CSVReader.QueryDataCSV(files[i], null, skipLines));
            //    i++;
            //}

            // Add the first range, the rest of the files will be merged into this one set
            sorted.AddRange(data[0]);

            // Starting Merge       -- To beat: 8 files in 8s
            for (int x = 1; x < data.Count(); x++)
            {
                foreach (var tag in data[x])
                {
                    var f = sorted.FindIndex((t) => { return t.Name == tag.Name; });
                    if (f > -1)
                    {
                        long start = sorted[f].Values.First().Time;
                        long end = sorted[f].Values.Last().Time;

                        // index range of the Values to take from the tag values (Exclude duplicates on either end)
                        int takeBegin = 0, takeEnd = tag.Values.Count - 1;

                        // -- Remove any values that overlap the values that already exist in the Array ------------------------
                        // quick jump to middle of values if we can
                        if (tag.Values[takeBegin].Time > start && tag.Values[takeBegin].Time < end &&
                            tag.Values[(tag.Values.Count / 2) - 1].Time > start &&     // If the values are still dupl in the middle
                            tag.Values[(tag.Values.Count / 2) - 1].Time < end)         // we can just jump there
                        {
                            //Debug.WriteLine("Quick Begin Jump used");
                            takeBegin = (tag.Values.Count / 2) - 1;
                        }


                        // Refine position
                        while (takeBegin < tag.Values.Count &&
                            tag.Values[takeBegin].Time > start && tag.Values[takeBegin].Time < end)
                            takeBegin++;

                        // quick jump to middle
                        if (tag.Values[takeEnd].Time > start && tag.Values[takeBegin].Time < end &&
                            tag.Values[(tag.Values.Count / 2) + 1].Time > start &&     // If the values are still dupl in the middle
                            tag.Values[(tag.Values.Count / 2) + 1].Time < end)         // we can just jump there
                        {
                            //Debug.WriteLine("Quick End Jump used");
                            takeEnd = (tag.Values.Count / 2) + 1;
                        }


                        // Refine position
                        while (takeEnd > 0 &&
                            tag.Values[takeEnd].Time > start && tag.Values[takeEnd].Time < end)
                            takeEnd--;

                        //Debug.WriteLine("For Tag {0}, found unique values from {1} to {2}.",
                        //    tag.Name,
                        //    takeBegin,
                        //    takeEnd);

                        // if there's any values to insert, insert them into the list at the correct position
                        if (!(takeEnd < 1 || takeBegin >= tag.Values.Count - 1 || takeBegin >= takeEnd))
                        {
                            // Find the position to insert these values
                            for (int y = 0; y < sorted[f].Values.Count; y++)
                            {
                                if (sorted[f].Values[y].Time > tag.Values.First().Time)
                                {
                                    sorted[f].Values.InsertRange(y, tag.Values.GetRange(takeBegin, takeEnd - takeBegin));
                                    break;
                                }

                                // If this is the last iteration and we didn't insert, add them to the end
                                if (y == sorted[f].Values.Count - 1)
                                    sorted[f].Values.AddRange(tag.Values.GetRange(takeBegin, takeEnd - takeBegin));
                            }
                            // 0000, 0005, 0015    << 0010                        
                        }
                    }
                    else
                        sorted.Add(tag);
                }
                Debug.WriteLine(string.Format("Data {0} merged...", x));
            }

            return sorted;
        }

        public static List<CSVReader.CSVFile> newLoadFiles(List<string> files)
        {
            if (files.Count < 1)
                return new List<CSVReader.CSVFile>();

            // -- First, load all the files
            CSVReader.CSVFile[] csvs = new CSVReader.CSVFile[files.Count()];
            Task[] tasks = Enumerable.Range(0, files.Count()).Select(i =>
                    Task.Run(() =>
                    {
                        csvs[i] = CSVReader.ReadCSV(files[i]);
                    })
                ).ToArray();

            // Wait on all the files to be parsed
            Task.WaitAll(tasks);

            // -- Merge the files
            List<CSVReader.CSVFile> ret = new List<CSVReader.CSVFile>();            // Return list
            List<CSVReader.CSVFile> dontMatch = new List<CSVReader.CSVFile>();      // Files that don't match the current tag structure


            List<CSVReader.CSVFile> toProcess =                                     // Files that still need to be merged
                new List<CSVReader.CSVFile>(csvs);

            do
            {
                var file = mergeFiles(toProcess, out dontMatch);
                ret.Add(file);
            } while (dontMatch.Count > 0);

            return ret;
        }

        private static CSVReader.CSVFile mergeFiles(List<CSVReader.CSVFile> toProcess, out List<CSVReader.CSVFile> dontMatch)
        {
            dontMatch = new List<CSVReader.CSVFile>();
            CSVReader.CSVFile file = new CSVReader.CSVFile();                       // current file     
            List<long> timesAdded = new List<long>();                               // Times this file has already

            for (int i = 0; i < toProcess.Count; i++)
            {
                if (i == 0)
                {
                    file.Tags = toProcess[0].Tags;
                    file.Rows = toProcess[0].Rows;

                    foreach (var row in toProcess[0].Rows)
                        timesAdded.Add(row.Time);
                }
                else
                {
                    // If this file doesn't apply to the one we're building, save it for later
                    if (file.Tags != toProcess[i].Tags)
                    {
                        dontMatch.Add(toProcess[i]);
                        continue;
                    }

                    // Add each row if it's time hasn't been added yet
                    foreach (var row in toProcess[0].Rows)
                    {
                        if (!timesAdded.Contains(row.Time))
                        {
                            file.Rows.Add(row);
                            timesAdded.Add(row.Time);
                        }
                    }
                }
            }

            return file;
        }
    }
}
