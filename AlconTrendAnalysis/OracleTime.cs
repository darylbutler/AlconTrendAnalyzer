﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlconTrendAnalysis
{
    public class OracleTime
    {
        public static ulong ToLong(DateTime time)
        {
            return (ulong)(time - (new DateTime(1980, 01, 1, 0, 0, 00))).TotalSeconds;
        }
        public static DateTime ToDateTime(ulong time)
        {
            return new DateTime(1980, 01, 1, 0, 0, 00).AddSeconds(time);
        }
    }
}
