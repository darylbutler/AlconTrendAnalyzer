﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using OxyPlot;
using System.Diagnostics;
using OxyPlot.Wpf;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Threading;
using System.Runtime.InteropServices;
using AlconTrendAnalysis.Data;
using Microsoft.WindowsAPICodePack.Dialogs;

/**
 * 2017-08-29 --- TODO Started:
 * 
 *  - Clean up unused testing code and different algorithms if new LoadCSV and LoadTXT work
 *  -(DONE) Add Progress to the splash screen
 *  -(DONE) Make MainForm start with a query range of a year? Or a day? whatever's reasonable.
 *  -(INPROG) Add string parsing to query time boxes, for instance, allow 'live' for end time (So newest data)
 *              and like '1d' or '1day' in start time, so for newest data back 1 day
 * 
 **/

namespace AlconTrendAnalysis
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    ///     - Verions:
    ///         -- 0.1 - CSV Reader
    ///             - b: Added Right Axis and Axis Properties
    ///         -- 0.2 - Database Mode with 7+ data
    /// </summary>
    public partial class MainWindow : Window
    {
        const string AppVersionKey = "v1.1.2";
        const string LeftAxisKey = "LeftAxis";
        const string RightAxisKey = "RightAxis";

        public const string FilterTagsEditKey = "Filter Tags...";

        const uint UpdateInterval = 60;      // How often to check for new data (Live Update), default should be 60 (1 minute)

        // Keep Windows Awake
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern EXECUTION_STATE SetThreadExecutionState(EXECUTION_STATE esFlags);

        private SettingsWindow settingsWindow = new SettingsWindow();
        CachedStoreProvider Store = new CachedStoreProvider();

        // Model for Chart
        public MainViewModel MainViewModel = new MainViewModel();

        // Data Loaded from data storage
        private List<TagData> DatabaseTags = new List<TagData>();
        private List<string> DatabaseTagNames = new List<string>();
        private List<TagData> CurrentQuery = new List<TagData>();
        private List<int> TrendingTags = new List<int>();
        private List<QueryTagEntry> QueryTagList = new List<QueryTagEntry>();

        // Properties Panel
        private TagData SelectedTag;

        // Chart Axis Colors
        private Dictionary<string, Color> ChartColors = new Dictionary<string, Color>
        {
            { "Sea Green",      Color.FromRgb(0, 83, 62) },
            { "Magenta",        Color.FromRgb(227, 0, 109) },
            { "Green",          Color.FromRgb(122, 191, 0) },
            { "Light Blue",     Color.FromRgb(0, 133, 194) },
            { "Light Green",    Color.FromRgb(210, 239, 126) },
            { "Blue",           Color.FromRgb(0, 45, 98) },
            { "Orange",         Color.FromRgb(249, 114, 17) },
            { "Sky Blue",       Color.FromRgb(116, 193, 251) },
            { "Brown",          Color.FromRgb(162, 38, 48) },
            { "Light Orange",   Color.FromRgb(253, 184, 0) },
            { "Navy",           Color.FromRgb(16, 29, 39) },
            { "Tan",            Color.FromRgb(247, 224, 177) }
            //((Color)ColorConverter.ConvertFromString("#FF3e95cd")).ToOxyColor(),
            //((Color)ColorConverter.ConvertFromString("#FF8e5ea2")).ToOxyColor(),
            //((Color)ColorConverter.ConvertFromString("#FF3cba9f")).ToOxyColor(),
            //((Color)ColorConverter.ConvertFromString("#FFe8c3b9")).ToOxyColor(),
            //((Color)ColorConverter.ConvertFromString("#FFc45850")).ToOxyColor(),
            //OxyColor.FromRgb(0x4E, 0x9A, 0x06),
            //OxyColor.FromRgb(0xC8, 0x8D, 0x00),
            //OxyColor.FromRgb(0xCC, 0x00, 0x00),
            //OxyColor.FromRgb(0x20, 0x4A, 0x87),
            //OxyColors.Red,
            //OxyColors.Orange,
            //OxyColors.Yellow,
            //OxyColors.Green,
            //OxyColors.Blue,
            //OxyColors.Indigo,
            //OxyColors.Violet
        };
        private Dictionary<string, Color> ChartTrendColors;

        // Misc
        private bool QueryTitleModified;            // generate query titles until it's been edited
        private DispatcherTimer UpdateQueryTimer;   // Updates Queries when live
        private DateTime DatabaseStartTime, DatabaseEndTime;

        // UI
        private Brush TagLabel_Border = SystemColors.ControlLightBrush;
        private Brush TagLabel_Unselected = SystemColors.ControlDarkBrush;  // Default
        private Brush TagLabel_Selected = SystemColors.ControlBrush;
        private Brush TagLabel_Hover = SystemColors.HotTrackBrush;

        public MainWindow() {
            InitializeComponent();

            // Set app to stay awake
            App.Current.Startup += new StartupEventHandler((sender, e) => {
                SetThreadExecutionState(EXECUTION_STATE.ES_DISPLAY_REQUIRED | EXECUTION_STATE.ES_CONTINUOUS);
            });
            App.Current.Exit += new ExitEventHandler((sender, e) => {
                SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS);
            });
            this.Title = "Alcon Trend Analyzer - " + AppVersionKey;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e) {
            // Check if the database is okay. If not, allow them to change it
            while (string.IsNullOrWhiteSpace(settingsWindow.Path)) {
                if (string.IsNullOrWhiteSpace(settingsWindow.Path))
                    MessageBox.Show("Please provide atleast one path to the data files (CSV and TXT).\nThis application can do nothing without a path to those files.",
                                        "Need Data Path",
                                        MessageBoxButton.OK,
                                        MessageBoxImage.Information);

                if (!ChangeSettings()) {
                    Application.Current.Shutdown();
                    return;
                }
            }
            settingsWindow.Close();

            LoadDatabase();
            LoadUI();
            DatabaseTags = Store.GetAllTags();

            // We're ready
            StatusBarText.Text = "Analyzer Ready.";
            LoadingBusyBar.IsIndeterminate = false;
            LoadingBusyBar.Value = 0;
            settingsWindow = null;
        }
        private void LoadDatabase() {
            // Load the database with this directory
            Store = new CachedStoreProvider();
            Store.StartUp(settingsWindow.Path);

            StatusBarText.Text = "Loading Cache...";
            LoadingBusyBar.IsIndeterminate = false;
            LoadingBusyBar.Minimum = 0;
            LoadingBusyBar.Maximum = 1;
            // Loading Database            
            Store.Load(new Progress<double>(val => {
                if (val >= 0.95d) {
                    StatusBarText.Text = "Analyzer Ready.";
                    LoadingBusyBar.IsIndeterminate = false;
                    LoadingBusyBar.Value = 0;
                    settingsWindow = null;
                } else {
                    StatusBarText.Text = string.Format("Loading Cache: {0:0.00}%", val * 100);
                    LoadingBusyBar.Value = val;
                }
            }));            
        }
        private void LoadUI() {
            // Setup UI
            ChartTrendColors = new Dictionary<string, Color>();
            foreach (var entry in ChartColors)
                ChartTrendColors.Add(entry.Key, Lighten(entry.Value, 0.5d));

            // Load the UI From database
            LoadTags();
            LoadPropertyComboBoxes();
            LoadProperties(null);
            ResetQuery(null, null);

            // Setup Calender Display Range
            UpdateDatabaseTimeRange();
            QueryRangeCalender.SelectedDate = DatabaseEndTime.Subtract(new TimeSpan(24, 0, 0)).Date;
        }
        private void MainWin_Closing(object sender, System.ComponentModel.CancelEventArgs e) {

        }

        // -- Cache Specific ------------------------------------------------------------------------------------------
        private void ClearCacheBtn_Click(object sender, RoutedEventArgs e) {
            Store.ClearCache();
            ExecuteNewQuery(sender, e);
        }

        // -- Settings ------------------------------------------------------------------------------------------------
        private bool ChangeSettings() {
            // Show, and If settings were changed:
            settingsWindow = new SettingsWindow();
            if (settingsWindow.ShowDialog() == true) {
                // Update Database Path and reload it
                LoadDatabase();
                settingsWindow.Close();
                return true;
            }
            settingsWindow?.Close();
            return false;
        }
        private void SettingsBtn_Click(object sender, RoutedEventArgs e) {
            ChangeSettings();
        }
        public static void Log(string text) {
            using (StreamWriter writer = new StreamWriter("log", true, Encoding.Default)) {
                writer.WriteLine(string.Format("{0} -- {1}", DateTime.Now.ToString(), text));
            }
        }        

        // -- Plain Tag View ---------------------------------------------------------------------------------------
        private void LoadTags() {
            // Get new data and clear the current UI
            DatabaseTags = Store.GetAllTags();
            DatabaseTagNames = (from t in DatabaseTags
                                select t.Description).ToList();

            // Variables to save the TagList state (So it can be restored)
            double VerticalScrollOffset = TagsListScrollViewer.VerticalOffset;
            List<string> GroupNamesCurrentlyExpanded = new List<string>();
            foreach (Expander exp in TagsList.Children) {
                if (exp.IsExpanded)
                    GroupNamesCurrentlyExpanded.Add(exp.ToolTip.ToString());
            }

            TagsList.Children.Clear();

            // Load tags in order of machine module
            string[] machines = { "MM", "EM", "IM", "TM", "PP" };
            var comparer = new NaturalStringComparer();

            foreach (var machine in machines) {
                var grp = from t in DatabaseTags
                          where t.Machine == machine
                          group t by t.Description into g
                          select g.First();

                var list = grp.ToList();
                list.Sort((a, b) => comparer.Compare(a.Description, b.Description));

                foreach (var g in list)
                    LoadTagsInGroup(g.GroupName);
            }

            // Restore UI state
            //MouseEventArgs lblEnter = new MouseEventArgs(Mouse.PrimaryDevice, 0);
            //e.RoutedEvent = Mouse.MouseEnterEvent;
            MouseEventArgs lblLeave = new MouseEventArgs(Mouse.PrimaryDevice, 0) {
                RoutedEvent = Mouse.MouseLeaveEvent
            };

            TagsListScrollViewer.ScrollToVerticalOffset(VerticalScrollOffset);
            foreach (Expander exp in TagsList.Children) {
                exp.IsExpanded = GroupNamesCurrentlyExpanded.Contains(exp.ToolTip.ToString());

                // Make sure each label is correctly highlighted
                foreach (Label lbl in ((StackPanel)exp.Content).Children) {
                    lbl.RaiseEvent(lblLeave);
                }
            }

            // Redo the filter
            TagFilterBox_TextChanged(null, null);
        }
        private void LoadTagsInGroup(string groupN) {
            StackPanel panel = new StackPanel();
            Expander exp = new Expander() {
                Header = groupN,
                IsExpanded = false,
                Margin = new Thickness(12, 2, 2, 2),
                ToolTip = groupN,
                Content = panel
            };
            exp.MouseEnter += new MouseEventHandler((s, e) => {
                exp.Foreground = SystemColors.ControlBrush;
            });
            exp.MouseLeave += new MouseEventHandler((s, e) => {
                exp.Foreground = SystemColors.ControlTextBrush;
            });
            exp.PreviewMouseDown += Chart_MouseDown;

            var tagsInGrp = (from t in DatabaseTags
                             where t.GroupName == groupN
                             select t).ToList();

            var comparer = new NaturalStringComparer();
            tagsInGrp.Sort((a, b) => comparer.Compare(ModuleNameFromTag(a.Name), ModuleNameFromTag(b.Name)));

            foreach (var tag in tagsInGrp) {
                Label lbl = new Label() {
                    Content = ModuleNameFromTag(tag.Name),
                    Tag = DatabaseTags.FindIndex((t) => t.Name == tag.Name),
                    ToolTip = GenerageTagTitle(tag, false),
                    FontSize = 14,
                    Margin = new Thickness(2, 0, 2, -1),
                    Background = TagLabel_Unselected,
                    BorderBrush = TagLabel_Border,
                    BorderThickness = new Thickness(1),
                    Padding = new Thickness(2),
                    HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center
                };
                lbl.MouseLeftButtonDown += TagLabelClick;
                lbl.MouseEnter += new MouseEventHandler((s, e) => {
                    lbl.Background = TagLabel_Hover;
                });
                lbl.MouseLeave += new MouseEventHandler((s, e) => {
                    if (QueryTagList.FindIndex(x=>x.TagID == (int)lbl.Tag) > -1)    // If Query Exists in our new Query
                        lbl.Background = TagLabel_Selected;
                    else
                        lbl.Background = TagLabel_Unselected;
                });
                panel.Children.Add(lbl);
            }
            TagsList.Children.Add(exp);
        }
        private void TagLabelClick(object sender, MouseButtonEventArgs e) {
            Label lbl = sender as Label;
            var tag = QueryTagList.FindIndex(x => x.TagID == (int)lbl.Tag);
            var isTrended = (Keyboard.Modifiers & ModifierKeys.Control) != 0;
            var isRightAxis = e.RightButton == MouseButtonState.Pressed || (Keyboard.Modifiers & ModifierKeys.Shift) != 0;

            if (tag >= 0) { // Remove from list
                var t = QueryTagList[tag];
                if (t.IsTrendLine) {    // remove the '(T)(L) '
                    lbl.Content = lbl.Content.ToString().Substring(7);
                } else {                // Just remove the '(L) ' or '(R) '
                    lbl.Content = lbl.Content.ToString().Substring(4);
                }
                QueryTagList.RemoveAt(tag);
            } else {        // Add                
                QueryTagList.Add(new QueryTagEntry() {
                    TagID = (int)lbl.Tag,
                    TagName = DatabaseTags[(int)lbl.Tag].Name,
                    IsTrendLine = isTrended,
                    IsRightAxis = isRightAxis
                });

                if (isTrended) {   
                    lbl.Content = string.Format("(T)({0}) {1}",
                        isRightAxis ? "R" : "L", 
                        lbl.Content);
                }
                else {             
                    lbl.Content = string.Format("({0}) {1}", isRightAxis ? "R" : "L", lbl.Content);
                }
            }

            // Color the Label correctly
            MouseEventArgs lblLeave = new MouseEventArgs(Mouse.PrimaryDevice, 0) {
                RoutedEvent = Mouse.MouseLeaveEvent
            };
            lbl.RaiseEvent(lblLeave);
        }
        private async void TagLabelClickOld(object sender, MouseButtonEventArgs e) {
            Label lbl = sender as Label;
            TagData tag = DatabaseTags[(int)lbl.Tag];
            Series s = GetSeries(tag.Name);

            if (s != null) {
                RemoveSeries(tag);
                if (DataSeriesBox.SelectedItem == tag)
                    LoadProperties(null);
                DataSeriesBox.Items.Remove(tag.Name);
                lbl.Background = SystemColors.ControlDarkBrush;
            }
            else {

                bool rightAxis = e.RightButton == MouseButtonState.Pressed || (Keyboard.Modifiers & ModifierKeys.Shift) != 0;
                bool trendThisSeries = (Keyboard.Modifiers & ModifierKeys.Control) != 0;

                // Create the Right Axis if we don't have one, if we're creating on that axis
                if (rightAxis && chart.Axes.Count == 2)
                    chart.Axes.Add(MakeRightAxis());

                // Create the new series
                await CreateSeries(tag, !rightAxis, false);
                DataSeriesBox.Items.Add(tag.Name);
                lbl.Background = SystemColors.ControlDarkDarkBrush;

                // Trend this series if selected
                if (trendThisSeries) {
                    SeriesOptions opts = (SeriesOptions)chart.Series.Last().Tag;

                    // Create the trend series
                    int trendSeriesID = await CreateSeries(tag, !rightAxis, true);

                    // Assign this trend to the tag's series                    
                    opts.TrendLineIndex = trendSeriesID;

                    // Complete the circle
                    (chart.Series[trendSeriesID].Tag as SeriesOptions).TrendLineIndex = trendSeriesID - 1;  // We know they were added consecuteively
                }
            }

            // Update query title
            if (!QueryTitleModified) {
                QueryTitleModified = false; // Overwrite our changes
            }
        }
        private void TagFilterBox_TextChanged(object sender, TextChangedEventArgs e) {
            if (TagFilterBox == null || TagsList == null)
                return;

            string filter = TagFilterBox.Text;

            if (string.IsNullOrWhiteSpace(filter) || filter.ToLower() == FilterTagsEditKey.ToLower()) {
                // Show all Tags
                foreach (Expander exp in TagsList.Children) {
                    exp.Visibility = Visibility.Visible;
                }

                // Gray the text
                TagFilterBox.Foreground = Brushes.Gray;
            }
            else {
                // Only show tags that match this 
                foreach (Expander exp in TagsList.Children) {
                    if (exp.ToolTip.ToString().ToUpper().Contains(filter.ToUpper()))
                        exp.Visibility = Visibility.Visible;
                    else
                        exp.Visibility = Visibility.Collapsed;
                }

                // Black the Text
                TagFilterBox.Foreground = SystemColors.ControlTextBrush;
            }
        }
        private void TagFilterBox_GotFocus(object sender, RoutedEventArgs e) {
            if (TagFilterBox.Text.ToLower() == FilterTagsEditKey.ToLower())
                TagFilterBox.Text = string.Empty;
        }
        private void TagFilterBox_LostFocus(object sender, RoutedEventArgs e) {
            if (string.IsNullOrWhiteSpace(TagFilterBox.Text))
                TagFilterBox.Text = FilterTagsEditKey;
        }

        // -- Execute Query -------------------------------------------------------------------------------------------
        private async void ExecuteNewQuery(object sender, RoutedEventArgs e) {
            // If no tags selected, then there isn't anything to do
            if (QueryTagList.Count < 1) {
                StatusBarText.Text = "Cannot execute query if no tags are selected!";
                LoadingBusyBar.IsIndeterminate = false;
                LoadingBusyBar.Value = 0;
                return;
            }

            // Check if the timeframe can be parsed
            if (!GetSelectedQueryRange(out ulong queryStart, out ulong queryEnd)) {
                StatusBarText.Text = "Cannot parse query timeframe! Check your syntax!";
                LoadingBusyBar.IsIndeterminate = false;
                LoadingBusyBar.Value = 0;
                StartTimeEdit.Focus();
                return;
            }

            // Get the UI ready for new query
            LoadProperties(null);   // Deselect any tags
            DataSeriesBox.Items.Clear();
            chart.Series.Clear();
            CurrentQuery = new List<TagData>();

            // Setup UI For long Running Query
            StatusBarText.Text = "Starting query...";
            LoadingBusyBar.IsIndeterminate = true;
            var sw = Stopwatch.StartNew();

            // Add each series
            foreach (var tag in QueryTagList) {
                // Create the Right Axis if we don't have one, if we're creating on that axis
                if (tag.IsRightAxis && chart.Axes.Count == 2)
                    chart.Axes.Add(MakeRightAxis());

                // Create the new series
                await CreateSeries(DatabaseTags[tag.TagID], !tag.IsRightAxis, false);
                DataSeriesBox.Items.Add(tag.TagName);

                // Trend this series if selected
                if (tag.IsTrendLine) {
                    SeriesOptions opts = (SeriesOptions)chart.Series.Last().Tag;

                    // Create the trend series
                    int trendSeriesID = await CreateSeries(DatabaseTags[tag.TagID], !tag.IsTrendLine, true);

                    // Assign this trend to the tag's series                    
                    opts.TrendLineIndex = trendSeriesID;

                    // Complete the circle
                    (chart.Series[trendSeriesID].Tag as SeriesOptions).TrendLineIndex = trendSeriesID - 1;  // We know they were added consecuteively
                }
            }

            // Load the points grid
            PopulateDataGrid();

            // Done            
            LoadingBusyBar.IsIndeterminate = false;
            chart.InvalidatePlot(true); // Redraw the graph            
            StatusBarText.Text = string.Format("Query Completed in {0}s, {1} to {2}", sw.ElapsedMilliseconds / 1000,
                        OracleTime.ToDateTime(queryStart), OracleTime.ToDateTime(queryEnd));
        }
        private void AddDataFromCSV(object sender, RoutedEventArgs e) {
            var dlg = new OpenFileDialog() {
                Multiselect = false,
                Title = "Select file to import"
            };

            if (dlg.ShowDialog() != true) return;
            if (!File.Exists(dlg.FileName)) return;

            // Get the UI ready for new query
            LoadProperties(null);   // Deselect any tags
            DataSeriesBox.Items.Clear();
            chart.Series.Clear();
            CurrentQuery = new List<TagData>();

            // Setup UI For long Running Query
            StatusBarText.Text = "Starting query...";
            LoadingBusyBar.IsIndeterminate = true;
            var sw = Stopwatch.StartNew();

            // -- Read Values from File into Current Query
            CurrentQuery = new List<TagData>();
            var generateBottomAxis = false;
            var lines = File.ReadAllLines(dlg.FileName);            
            for (int i = 0; i < lines.Length; i++) {
                var line_raw = lines[i].EndsWith(",") || lines[i].EndsWith(";") ?
                    lines[i].Trim().Substring(0, lines[i].Length - 1) : lines[i].Trim();
                var line = line_raw.Split(',', ';');

                // Skip empty lines
                if (line.Length < 1) continue;

                // Generate TagData structs with header line
                if (i == 0) {
                    if (line.Length == 1) {
                        generateBottomAxis = true;
                        CurrentQuery.Add(new TagData() { Name = line[0], Values = new List<TagValue>(lines.Length - 1) });
                    } else {
                        generateBottomAxis = false;
                        for (int c = 1; c < line.Length; c++) {
                            CurrentQuery.Add(new TagData() { Name = line[c], Values = new List<TagValue>(lines.Length - 1) });
                        }
                    }
                }
                // Add each value to the datasource
                else {
                    int start = generateBottomAxis ? 0 : 1;
                    var time = generateBottomAxis ? DateTime.Now.Date.AddSeconds(5 * (i - 1)) : DateTime.Parse(line[0]);    // TODO throw exception here if can't parse (Not in right format)

                    for (int c = start; c < line.Length; c++) {
                        if (string.IsNullOrWhiteSpace(line[c])) {
                            CurrentQuery[c - start].Values.Add(new TagValue(OracleTime.ToLong(time), double.MaxValue));
                        } else if (double.TryParse(line[c], out double val)) {
                            CurrentQuery[c - start].Values.Add(new TagValue(OracleTime.ToLong(time), val));
                        }
                        // else throw error? Skip for now
                    }
                }
            }
            
            // -- Create the Series on chart
            foreach (var tag in CurrentQuery) {
                var tagPoints = new List<DataPoint>(tag.Values.Count);
                foreach (var val in tag.Values) {
                    if (val.Value != double.MaxValue) { // Don't add the null values (DB translates nullvalues to 8bytes (Even if stored as 4bytes))
                        tagPoints.Add(new DataPoint(OxyPlot.Axes.DateTimeAxis.ToDouble(val.DateTime), val.Value));
                    }
                }

                // Color
                int colorIndex = (from s in chart.Series where !((SeriesOptions)s.Tag).IsTrendLine select s).Count();
                while (colorIndex > ChartColors.Count - 1)
                    colorIndex -= 10;
                if (colorIndex < 0)
                    colorIndex = 0;
                var color = ChartColors.ElementAt(colorIndex).Value;

                // Create the SeriesOptions
                SeriesOptions opts = new SeriesOptions() {
                    TagName = tag.Name,
                    IsTrendLine = false
                };
                LineSeries series = new LineSeries() {
                    YAxisKey = LeftAxisKey,
                    ItemsSource = tagPoints,
                    Color = color,
                    MarkerFill = color,
                    MarkerStroke = color,
                    Tag = opts,
                    LineStyle = LineStyle.Solid,
                    Title = tag.Name
                };
                chart.Series.Add(series);
                DataSeriesBox.Items.Add(tag.Name);
            }

            // Load the points grid
            PopulateDataGrid();

            // Done            
            LoadingBusyBar.IsIndeterminate = false;
            chart.InvalidatePlot(true); // Redraw the graph            
            StatusBarText.Text = string.Format("Query Completed in {0}s, Now loading Data Point Grid...", sw.ElapsedMilliseconds / 1000);
        }

        // -- Data Points Grid Display --------------------------------------------------------------------------------
        private async void PopulateDataGrid() {
            if (CurrentQuery.Count < 1 || CurrentQuery[0].Values.Count < 1)
                return;
            PointsGrid.ItemsSource = null;
            PointsGrid.Items.Clear();
            PointsGrid.Columns.Clear();
            PointsGrid.Columns.Add(new DataGridTextColumn());            

           var dt = await Task.Run(async () => {
                return await Task.Run(() => {
                    return BuildDataTableAsync();
                });
            });

            // Display Table
            PointsGrid.Items.Clear();
            PointsGrid.Columns.Clear();
            PointsGrid.ItemsSource = dt.DefaultView;
            foreach (var col in PointsGrid.Columns) {
                col.Width = 75;
                col.HeaderStyle = new Style();
                col.HeaderStyle.Setters.Add(new Setter(ToolTipProperty, col.Header));
            }

            StatusBarText.Text = "Data Grid Populated!";
        }
        private System.Data.DataTable BuildDataTableAsync() {
            var dt = new System.Data.DataTable();

            if (CurrentQuery.Count < 1 || CurrentQuery[0].Values.Count < 1)
                return dt;

            // Get times
            var times = new List<ulong>();
            for (int i = 0; i < CurrentQuery.Count; i++) {
                times.AddRange(CurrentQuery[i].Values.Where(x => !times.Contains(x.Time)).Select(x => x.Time).ToList());
            }
            times.Sort();

            // Add Columns
            dt.Columns.Add("Time");
            foreach (var series in CurrentQuery) {
                //dt.Columns.Add(series.Name);
                dt.Columns.Add(GenerageTagTitle(series));
            }

            // Add each row
            DateTime lastDT = DateTime.MinValue;
            foreach (var time in times) {
                var row = dt.NewRow();

                var currDT = OracleTime.ToDateTime(time);
                if (lastDT.Date != currDT.Date)
                    row[0] = currDT.ToString("yyyy-MM-dd\nHH:mm:ss");
                else
                    row[0] = currDT.ToString("HH:mm:ss");
                lastDT = currDT;

                // Add each column
                for (int i = 0; i < CurrentQuery.Count; i++) {
                    var loc = CurrentQuery[i].Values.FindIndex(x => x.Time == time);
                    if (loc < 0 || CurrentQuery[i].Values[loc].Value == double.MaxValue) { // Not Found
                        row[i + 1] = null;
                    }
                    else {
                        row[i + 1] = string.Format("{0:0.00}", CurrentQuery[i].Values[loc].Value);
                    }
                }
                dt.Rows.Add(row);
            }
            return dt;
        }

        // -- Series --------------------------------------------------------------------------------------------------
        private Series GetSeries(string tag) {
            foreach (var series in chart.Series) {
                SeriesOptions opt = (SeriesOptions)series.Tag;

                if (opt != null && opt.TagName == tag)
                    return series;
            }
            return null;
        }
        private bool HasSeries(TagData tag) {
            foreach (var series in chart.Series) {
                SeriesOptions opt = (SeriesOptions)series.Tag;

                if (opt != null && opt.TagName == tag.Name)
                    return true;

            }
            return false;
        }
        private int RemoveSeries(TagData tag) {
            List<Series> toRemove = new List<Series>();

            foreach (var series in chart.Series) {
                SeriesOptions opt = (SeriesOptions)series.Tag;

                if (opt != null && opt.TagName == tag.Name)
                    toRemove.Add(series);
            }

            if (toRemove.Count > 0) {
                foreach (var d in toRemove) {
                    chart.Series.Remove(d);
                }
                chart.InvalidatePlot(true);
                return toRemove.Count;
            }
            return 0;
        }
        private async Task<int> CreateSeries(TagData tag, bool leftAxis = true, bool trendLine = false) {
            int id = -1;
           
            // Convert the data into DataPoints
            List<DataPoint> tagPoints = new List<DataPoint>();
            Color color = ChartColors.First().Value;
            if (trendLine) {
                Series tagSeries = GetSeries(tag.Name);
                if (tagSeries == null)
                    throw new Exception("Trending Line could not find untrended / base data in series collection!");
                tagPoints = CalculateTrendLine_LinearRegression((List<DataPoint>)tagSeries.ItemsSource);
                var colorKey = ChartColors.FirstOrDefault(x => x.Value == tagSeries.Color).Key;
                color = ChartTrendColors[colorKey];
            }
            else {
                int colorIndex = (from s in chart.Series where !((SeriesOptions)s.Tag).IsTrendLine select s).Count();
                while (colorIndex > ChartColors.Count - 1)
                    colorIndex -= 10;
                if (colorIndex < 0)
                    colorIndex = 0;
                color = ChartColors.ElementAt(colorIndex).Value;

                // Query for the data                
                if (GetSelectedQueryRange(out ulong queryStart, out ulong queryEnd)) {
                    TagsList.IsEnabled = false;
                    var data = await Task.Run(() => {
                        return Store.Query(tag.Name, queryStart, queryEnd);
                    });
                    TagsList.IsEnabled = true;                    

                    if (data != null && data.Count > 0) {
                        tag.Values = data[0].Values;
                        CurrentQuery.Add(tag);
                        foreach (var val in data[0].Values) {
                            if (val.Value != double.MaxValue) { // Don't add the null values (DB translates nullvalues to 8bytes (Even if stored as 4bytes))
                                tagPoints.Add(new DataPoint(OxyPlot.Axes.DateTimeAxis.ToDouble(val.DateTime), val.Value));
                            }
                        }
                    }
                }

                // Save the store
                Store.Save();
            }

            // Create the SeriesOptions
            SeriesOptions opts = new SeriesOptions() {
                TagName = tag.Name,
                IsTrendLine = trendLine
            };
            LineSeries series = new LineSeries() {
                YAxisKey = leftAxis ? LeftAxisKey : RightAxisKey,
                ItemsSource = tagPoints,
                Color = color,
                MarkerFill = color,
                MarkerStroke = color,
                Tag = opts,
                LineStyle = trendLine ? LineStyle.Dash : LineStyle.Solid,
                Title = trendLine ? string.Format("Trend: {0}", GenerageTagTitle(tag)) : GenerageTagTitle(tag)
            };
            id = chart.Series.Count;
            chart.Series.Add(series);
            return id;
        }
        private async void UpdateAllSeries() {
            if (!GetSelectedQueryRange(out ulong queryStart, out ulong queryEnd))
                return;

            if (chart.Series.Count < 1)
                return;

            StatusBarText.Text = "Updating query to new time frame...";
            LoadingBusyBar.IsIndeterminate = true;
            TagsList.IsEnabled = false;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            bool changed = false;

            // Perform the query on each series            
            foreach (var series in chart.Series) {
                SeriesOptions opt = (SeriesOptions)series.Tag;

                if (!opt.IsTrendLine) {
                    var data = await Task.Run(() => {
                        //return Database.GetFromDatabase(new List<string> { opt.TagName }, queryStart, queryEnd);
                        return Store.Query(opt.TagName, queryStart, queryEnd);
                    });
                    if (data != null &&
                        data.Count > 0 &&
                        data[0].Values.Count > 0) {
                        List<DataPoint> tagPoints = new List<DataPoint>();
                        foreach (var val in data[0].Values) {
                            tagPoints.Add(new DataPoint(OxyPlot.Axes.DateTimeAxis.ToDouble(val.DateTime), val.Value));
                        }

                        if (series.ItemsSource == null ||
                            ((List<DataPoint>)series.ItemsSource).Count != tagPoints.Count ||
                            ((List<DataPoint>)series.ItemsSource).First().X != tagPoints.First().X ||
                            ((List<DataPoint>)series.ItemsSource).First().Y != tagPoints.First().Y ||
                            ((List<DataPoint>)series.ItemsSource).Last().X != tagPoints.Last().X ||
                            ((List<DataPoint>)series.ItemsSource).Last().Y != tagPoints.Last().Y) {
                            changed = true;
                            series.ItemsSource = tagPoints;
                        }
                    }
                    else
                        series.ItemsSource = null;
                }
            }
            TagsList.IsEnabled = true;
            sw.Stop();

            if (changed) {
                // Update trend lines
                foreach (var series in chart.Series) {
                    SeriesOptions opt = (SeriesOptions)series.Tag;

                    if (opt.IsTrendLine) {
                        Series tagSeries = GetSeries(opt.TagName);
                        if (tagSeries == null)
                            throw new Exception("Trending Line could not find untrended / base data in series collection!");
                        var tagPoints = (List<DataPoint>)tagSeries.ItemsSource;
                        if (tagPoints != null)
                            series.ItemsSource = CalculateTrendLine_LinearRegression(tagPoints);
                        else
                            series.ItemsSource = null;
                    }
                }
                chart.InvalidatePlot();
                chart.ResetAllAxes();
                StatusBarText.Text = string.Format("Completed Update. Took {0}ms.", sw.ElapsedMilliseconds);

                // Save the cache
                Store.Save();
            }
            else {
                StatusBarText.Text = string.Format("Completed Update, no new data found. Took: {0}ms.", sw.ElapsedMilliseconds);
            }
            LoadingBusyBar.IsIndeterminate = false;
        }

        // -- Query Load / Update -------------------------------------------------------------------------------------
        private void LoadQuery() {
            // Start over
            chart.Series.Clear();

            // -- Setup Axes
            chart.Axes.Clear();

            // -- Bottom Axis -----------------------------------------------------------------------------------------
            var dateTimeAxis = new DateTimeAxis();
            dateTimeAxis.Position = OxyPlot.Axes.AxisPosition.Bottom;

            // load options if we have any
            if (!string.IsNullOrWhiteSpace(BottomAxisStringFormat.Text))
                dateTimeAxis.StringFormat = BottomAxisStringFormat.Text;

            //dateTimeAxis.StringFormat = "MMM dd HH:mm:ss";
            //dateTimeAxis.Minimum = StartTime;
            //dateTimeAxis.Maximum = EndTime;
            chart.Axes.Add(dateTimeAxis);

            //-- Left Axis --------------------------------------------------------------------------------------------
            Axis leftAxis = MakeLeftAxis();
            chart.Axes.Add(leftAxis);

            // Redraw chart
            chart.InvalidatePlot(true);
        }
        private void ResetQuery(object sender, RoutedEventArgs e) {
            DataSeriesBox.Items.Clear();

            // Reset Title
            ChartTitleBox.Text = String.Empty;
            QueryTitleModified = false; // new Tags, allow query to be titled

            // Setup Properties
            TagFilterBox.Text = FilterTagsEditKey;
            SelectedTag = null;
            LoadProperties(null);
            LoadQuery();

            QueryTagList = new List<QueryTagEntry>();

            // Reset the tag selection highlights
            MouseEventArgs lblLeave = new MouseEventArgs(Mouse.PrimaryDevice, 0);
            lblLeave.RoutedEvent = Mouse.MouseLeaveEvent;
            foreach (Expander exp in TagsList.Children) {
                // Make sure each label is correctly highlighted
                foreach (Label lbl in ((StackPanel)exp.Content).Children) {
                    lbl.RaiseEvent(lblLeave);
                }
            }

            LoadingBusyBar.IsIndeterminate = false;
            LoadingBusyBar.Value = 0;
            StatusBarText.Text = "Query Reset.";
        }
        private void AxisType_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (!(sender is ComboBox) || chart == null)
                return;

            if (((ComboBox)sender).Name == "LeftAxisType") {
                if (chart.Axes.Count < 2)
                    return;

                // Change the Left Axis to the new value
                // NOTE: Left Axis is always first axis, if there is a valid query
                chart.Axes[1] = MakeLeftAxis();
            }
            else if (((ComboBox)sender).Name == "RightAxisType" && RightAxisEnabled.IsChecked == true) {
                if (chart.Axes.Count < 3)
                    return;

                // Change the Right Axis to the new value
                // NOTE: Right Axis is always the last axis, if there is a valid query
                chart.Axes[2] = MakeRightAxis();
            }

            chart.InvalidatePlot(false);
        }
        private void RightAxisEnabled_Click(object sender, RoutedEventArgs e) {
            if (RightAxisEnabled.IsChecked.HasValue) {
                bool rAxisEnabled = RightAxisEnabled.IsChecked.Value;

                // Create the axis if it doesn't exist
                if (rAxisEnabled && chart.Axes.Count == 2)
                    chart.Axes.Add(MakeRightAxis());
                // Remove the axis and all series on that axis if we turn it off
                else if (!rAxisEnabled && chart.Axes.Count == 3) {
                    // Assign all series to the left axis
                    foreach (LineSeries series in chart.Series) {
                        series.YAxisKey = LeftAxisKey;
                    }

                    // Remove the right axis
                    chart.Axes.RemoveAt(2);
                }
                chart.InvalidatePlot(false);
            }
        }

        // -- Time selection ------------------------------------------------------------------------------------------
        private void UpdateDatabaseTimeRange() {
            var timeframe = Store.GetDatabaseTimeRange();
            QueryRangeCalender.DisplayDateStart = DatabaseStartTime = OracleTime.ToDateTime(timeframe.Item1);
            QueryRangeCalender.DisplayDateEnd = DatabaseEndTime = OracleTime.ToDateTime(timeframe.Item2);
        }
        private void QueryRangeCalender_SelectedDatesChanged(object sender, SelectionChangedEventArgs e) {
            if (QueryRangeCalender.SelectedDates.Count < 1) return;

            if (DateTime.TryParse(StartTimeEdit.Text, out DateTime start)) {
                if (start.Date != QueryRangeCalender.SelectedDates.Min().Date) {
                    var time = start.TimeOfDay;
                    var date = QueryRangeCalender.SelectedDates.Min().Date;
                    StartTimeEdit.Text = new DateTime(date.Year, date.Month, date.Day, time.Hours, time.Minutes, time.Seconds).ToString();
                }
            }
            else
                StartTimeEdit.Text = QueryRangeCalender.SelectedDates.Min().ToString();

            if (QueryRangeCalender.SelectedDates.Count == 1)
                EndTimeEdit.Text = "1d";
            //EndTimeEdit.Text = QueryRangeCalender.SelectedDates.Min().AddHours(24).ToString();
            else {
                if (DateTime.TryParse(EndTimeEdit.Text, out DateTime end)) {
                    if (end.Date != QueryRangeCalender.SelectedDates.Max().AddMinutes(1439).Date) {
                        var time = end.TimeOfDay;
                        var date = QueryRangeCalender.SelectedDates.Max().AddDays(1).Date;
                        EndTimeEdit.Text = new DateTime(date.Year, date.Month, date.Day, time.Hours, time.Minutes, time.Seconds).ToString();
                    }
                }
                else
                    EndTimeEdit.Text = QueryRangeCalender.SelectedDates.Max().AddMinutes(1439).ToString();
            }


            //UpdateAllSeries();
            StartTimeEdit.Focus();
        }
        private bool GetSelectedQueryRange(out ulong queryStart, out ulong queryEnd) {
            bool parsedStart = DateTime.TryParse(StartTimeEdit.Text, out DateTime start);
            bool parsedEnd = DateTime.TryParse(EndTimeEdit.Text, out DateTime end);
            TimeSpan relativeStart = TimeSpan.Zero, relativeEnd = TimeSpan.Zero;
            queryStart = queryEnd = 0; // default values

            // Parse the dates if we can
            if (parsedStart)
                queryStart = OracleTime.ToLong(start);
            if (parsedEnd)
                queryEnd = OracleTime.ToLong(end);

            // Parse the Keywords if we can
            UpdateDatabaseTimeRange();
            if (EndTimeEdit.Text.ToLower() == "live" || EndTimeEdit.Text.ToLower() == "end") {
                EndTimeEdit.Text = "Live";  // Correct capitalization to show we ack the test
                queryEnd = OracleTime.ToLong(DatabaseEndTime);
                parsedEnd = true;
            }
            if (StartTimeEdit.Text.ToLower() == "start") {
                StartTimeEdit.Text = "Start";   // Correct capitalization to show we ack the test
                queryStart = OracleTime.ToLong(DatabaseStartTime);
                parsedStart = true;
            }

            // Check for errors
            if (!parsedEnd) {
                // Try to parse as a relative string
                relativeEnd = ParseTimeString(EndTimeEdit.Text);
                if (relativeEnd == TimeSpan.Zero) {
                    StatusBarText.Text = "Cannot parse query end time!";
                    return false;
                }
            }
            if (!parsedStart) {
                // Try to parse as a relative string
                relativeStart = ParseTimeString(StartTimeEdit.Text);
                if (relativeStart == TimeSpan.Zero) {
                    StatusBarText.Text = "Cannot parse query start time!";
                    return false;
                }
            }

            // Check for relative time            
            if (relativeEnd != TimeSpan.Zero && relativeStart != TimeSpan.Zero) {
                if (relativeEnd < relativeStart) {
                    var t = relativeEnd;
                    relativeEnd = relativeStart;
                    relativeStart = t;
                }
                // If both are relative, Start is relative from latest time, end is relative from start
                queryStart = OracleTime.ToLong(DatabaseStartTime.Subtract(relativeStart));
                queryEnd = OracleTime.ToLong(DatabaseStartTime.Subtract(relativeStart).Add(relativeEnd));
            }
            else if (relativeEnd != TimeSpan.Zero) {
                if (parsedStart)
                    queryEnd = OracleTime.ToLong(start.Add(relativeEnd));
                else {
                    MessageBox.Show("Cannot parse query start time!");
                    return false;
                }
            }
            else if (relativeStart != TimeSpan.Zero) {
                if (parsedEnd)
                    queryStart = OracleTime.ToLong(end.Add(relativeStart));
                else if (EndTimeEdit.Text == "Live")
                    queryStart = OracleTime.ToLong(DateTime.Now.Subtract(relativeStart));
                else {
                    MessageBox.Show("Cannot parse query end time!");
                    return false;
                }
            }

            // -- For live updating, this function is called when both the time controls are changed and when a new series is created
            // so, if they've selected Live updating, start or end the timer here.
            if (EndTimeEdit.Text == "Live" && UpdateQueryTimer == null) {
                // Start the timer
                UpdateQueryTimer = new DispatcherTimer();
                UpdateQueryTimer.Interval = new TimeSpan(0, 0, (int)UpdateInterval);
                UpdateQueryTimer.Tick += (sender, e) => {
                    // If we don't have a query, then make sure we delete oursevles
                    if (chart.Series.Count < 1) {
                        UpdateQueryTimer.Stop();
                        UpdateQueryTimer = null;
                    }
                    // Otherwise, Update only every 10 minutes
                    else if (DateTime.Now.Minute % 10 == 0)
                        UpdateAllSeries();
                };
                UpdateQueryTimer.Start();
            }
            else if (EndTimeEdit.Text != "Live" && UpdateQueryTimer != null) {
                UpdateQueryTimer.Stop();
                UpdateQueryTimer = null;
            }

            return true;
        }        
        private void TimeEdit_GotFocus(object sender, RoutedEventArgs e) {
            var box = sender as TextBox;
            box.Select(0, box.Text.Length);
        }
       
        private TimeSpan ParseTimeString(string time) {
            if (time == null || time.Length < 2)
                return TimeSpan.Zero;

            // Turn strings like :
            //  1d2m12s and 1d 12m 15h into a time span
            /*
             *  1y = 1 year
             *  1w = 1 week
             *  1d = 1 day
             *  
             *  1h = 1 hour
             *  1m = 1 minute
             *  1s = 1 second
             *  1ms = 1 milliseconds
             */

            int years = 0, weeks = 0, days = 0, hours = 0, minutes = 0, seconds = 0, milliseconds = 0;

            // Step 1 - Strip Spaces
            time = time.Replace(" ", string.Empty);

            // Step 2 -- Error Check (CANNOT Start with a letter)
            if (time.Length < 1 || Char.IsLetter(time[0]))
                return TimeSpan.Zero;

            // Step 3 - Split string into [DigitLetter] groups
            List<string> groups = new List<string>();
            int start = 0;
            for (int i = 1; i < time.Length; i++) {
                char c = time[i];
                if (Char.IsNumber(time[i]) && Char.IsLetter(time[i - 1])) {
                    // Group end, start of new group
                    groups.Add(time.Substring(start, i - start));
                    start = i;
                }
            }
            // Add the last item
            if (start < time.Length - 1) // Nees to be at least 2 chars long
                groups.Add(time.Substring(start));

            // Step 4 - Parse each group
            foreach (var str in groups) {
                int value = -1;
                string group = string.Empty;

                for (int i = 1; i < str.Length; i++) {
                    if (Char.IsLetter(str[i])) {
                        if (!int.TryParse(str.Substring(0, i), out int result) || i >= str.Length)
                            return TimeSpan.Zero;
                        else
                            value = result;
                        group = str.Substring(i);
                        break;
                    }
                }

                // Check for errors
                if (value < 0 || string.IsNullOrWhiteSpace(group))
                    return TimeSpan.Zero;   // Error

                switch (group.ToLower()) {
                    case "y":
                        //TODO check for duplicate
                        years = value;
                        break;
                    case "w":
                        weeks = value;
                        break;
                    case "d":
                        days = value;
                        break;
                    case "h":
                        hours = value;
                        break;
                    case "m":
                        minutes = value;
                        break;
                    case "s":
                        seconds = value;
                        break;
                    case "ms":
                        milliseconds = value;
                        break;
                }
            }

            // Step 5 - convert years, weeks, months, into days
            days += years * 365;
            days += weeks * 14;


            return new TimeSpan(days, hours, minutes, seconds, milliseconds);
        }
        private void MainWin_PreviewMouseUp(object sender, MouseButtonEventArgs e) {
            // Make the Calender play nice!
            base.OnPreviewMouseUp(e);
            if (Mouse.Captured is Calendar || Mouse.Captured is System.Windows.Controls.Primitives.CalendarItem) {
                Mouse.Capture(null);
            }
        }

        #region -- Charting ------------------------------------------------------------------------------------------------
        private List<DataPoint> CalculateTrendLine_LinearRegression(List<DataPoint> data) {
            List<DataPoint> trend = new List<DataPoint>();

            // Calculate Slope
            double a = 0, b = 0, c = 0, d = 0, xSum = 0, ySum = 0, slope, yIntercept;

            foreach (var point in data) {
                //a = Sum of all x*y * number of points
                a += point.X * point.Y;

                //b = sum of all x's * sum of all y's
                xSum += point.X;
                ySum += point.Y;

                //c = Sum of all squares x valves * number of points
                c += point.X * point.X;

            }
            a *= data.Count;
            b = xSum * ySum;
            c *= data.Count;

            // d= Sum of all x values, squared
            d = xSum * xSum;

            // -- Slope (m) = (a - b) / (c - d)
            slope = (a - b) / (c - d);

            yIntercept = (ySum - (xSum * slope)) / data.Count;

            // Calculate the trend line
            //foreach (var p in data)
            //{
            //	trend.Add(new DataPoint(((p.Y - yIntercept) / slope), p.Y));
            //}

            // Create line from smallest time to largest time
            //var smallestY = data.Min(x => x.Y);
            //var largestY = data.Max(x => x.Y);
            //trend.Add(new DataPoint(((smallestY - yIntercept) / slope), smallestY));
            //trend.Add(new DataPoint(((largestY - yIntercept) / slope), largestY));
            double minX = data.Min(p => p.X), maxX = data.Max(p => p.X);
            //trend.Add(new DataPoint(minX, (minX * slope) + yIntercept));
            //trend.Add(new DataPoint(maxX, (maxX * slope) + yIntercept));

            foreach (var p in data) {
                trend.Add(new DataPoint(p.X, (p.X * slope) + yIntercept));
            }

            return trend;
        }
        private void DrawTrendLines_Click(object sender, RoutedEventArgs e) {
            LoadQuery();
        }
        // Makes a Left Axis According to the Options in the UI
        private Axis MakeLeftAxis() {
            Axis leftAxis;
            if (LeftAxisType.SelectedIndex == 1) {
                leftAxis = new LogarithmicAxis() {
                    Position = OxyPlot.Axes.AxisPosition.Left
                };
            }
            else    // Always default to Linear
            {
                leftAxis = new LinearAxis() {
                    Position = OxyPlot.Axes.AxisPosition.Left
                };
            }
            leftAxis.Key = LeftAxisKey;

            // Load All the Options
            if (!LeftAxisAuto.IsChecked ?? true) {
                leftAxis.Minimum = double.Parse(LeftAxisMin.Text);
                leftAxis.Maximum = double.Parse(LeftAxisMax.Text);
            }

            return leftAxis;
        }
        // Makes a Right Axis According to the Options in the UI
        private Axis MakeRightAxis() {
            Axis rightAxis;

            // Axis type
            if (RightAxisType.SelectedIndex == 1) {
                rightAxis = new LogarithmicAxis() {
                    Position = OxyPlot.Axes.AxisPosition.Right
                };
            }
            else    // Always default to Linear
            {
                rightAxis = new LinearAxis() {
                    Position = OxyPlot.Axes.AxisPosition.Right
                };
            }
            rightAxis.Key = RightAxisKey;

            if (!RightAxisAuto.IsChecked ?? true) {
                rightAxis.Minimum = double.Parse(RightAxisMin.Text);
                rightAxis.Maximum = double.Parse(RightAxisMax.Text);
            }

            return rightAxis;
        }
        private void ChartTitleBox_TextChanged(object sender, TextChangedEventArgs e) {
            if (chart != null) {
                chart.Title = ChartTitleBox.Text;
                chart.Subtitle = ChartSubtitleBox.Text;
                QueryTitleModified = true;  // prevent us from modifying the query title now that's it been manually edited
            }
        }
        private void ResetView(object sender, RoutedEventArgs e) {
            //chart.InvalidatePlot();
            chart.ResetAllAxes();
        }
        #endregion

        #region -- Utility -------------------------------------------------------------------------------------------------
        private string GetMainWindowTitle() {
            return string.Format("Alcon Trend Analyzer {0}", AppVersionKey);
        }
        private string ModuleNameFromTag(string tagName) {
            string type = tagName.Substring(6, 2);
            int line = int.Parse(tagName.Substring(3, 2));
            int module = int.Parse(tagName.Substring(8, 1));

            if (type == "PM") {
                string machine = tagName.Substring(9, 2);
                module = module + ((line - 1) * 3);
                return string.Format("PM{0}{1}", module, machine.ToUpper());
            }
            if (type == "PP") {
                int track = int.Parse(tagName.Substring(10, 1));
                module = module + ((line - 1) * 2) + (track - 1);


                return string.Format("PP{0}", module);
            }

            return "Unknown";
        }
        private string ShorterModuleNameFromTag(string tagName) {
            if (tagName.Length < 11 || tagName.Count(x => x == '_') < 4)
                return tagName;

            string type = tagName.Substring(6, 2);
            int line = int.Parse(tagName.Substring(3, 2));
            int module = int.Parse(tagName.Substring(8, 1));

            if (type == "PM") {
                string machine = tagName.Substring(9, 2);
                module = module + ((line - 1) * 3);
                return string.Format("{0}{1}", machine.ToUpper(), module);
            }
            if (type == "PP") {
                int track = int.Parse(tagName.Substring(10, 1));
                module = module + ((line - 1) * 2) + (track - 1);


                return string.Format("PP{0}", module);
            }

            return "Unknown";
        }
        private string GenerateQueryTitle() {
            return string.Empty;

            List<TagData> tags = new List<TagData>();
            foreach (var i in DataSeriesBox.Items)
                tags.Add(GetTag((string)i));

            if (tags.Count < 1)
                return "Empty Query";

            var groups = tags.GroupBy(tag => tag.Description).Select(group => group.First().Description);

            if (groups.Count() == 1) {
                // Single tag query
                return string.Concat(groups.First(), "^{over Time}");
            }
            else if (groups.Count() == 2) {
                // Two Tag query
                return string.Concat(groups.First(), ", ", groups.Last(), "^{over Time}");
            }
            else {
                // Multiple Tag Query
                string grps = string.Join(", ", groups);
                return string.Concat(grps, "^{over Time}");
            }
        }
        private string GenerageTagTitle(TagData tag, bool shorten = true) {
            const int MaxDescLength = 50;
            string desc = string.IsNullOrWhiteSpace(tag.Description) ? tag.Name : tag.Description;

            // Strip any shit inside parens
            if (desc.Contains('('))
                desc = desc.Substring(0, desc.IndexOf('('));
            if (desc.Contains('['))
                desc = desc.Substring(0, desc.IndexOf('['));

            // Chop string if too long
            if (shorten)
                desc = desc.Substring(0, Math.Min(MaxDescLength, desc.Length));

            return string.Format("{0}: {1}",
                shorten ? ShorterModuleNameFromTag(tag.Name) : ModuleNameFromTag(tag.Name),
                desc);
        }
        private TagData GetTag(string tagName) {
            if (tagName == null)
                return null;

            var tag = DatabaseTags.Find(x => x.Name == tagName);

            if (tag == null)
                return new TagData() { Name = tagName };
            return tag;            
        }
        public Color Lighten(Color inColor, double inAmount, byte alpha = 0) {
            return Color.FromArgb(
              alpha == 0 ? inColor.A : alpha,
              (byte)Math.Min(255, inColor.R + 255 * inAmount),
              (byte)Math.Min(255, inColor.G + 255 * inAmount),
              (byte)Math.Min(255, inColor.B + 255 * inAmount));
        }
        public string PrettyFormatTimeSpan(TimeSpan span) {
            if (span.Days > 0)
                return string.Format("{0}d, {1}h, {2}m", span.Days, span.Hours, span.Minutes);
            if (span.Hours > 0)
                return string.Format("{0}h, {1}m ", span.Hours, span.Minutes);
            if (span.Minutes > 0)
                return string.Format("{0}m, {1}s ", span.Minutes, span.Seconds);

            return string.Format("{0}s", span.TotalSeconds);
        }
        #endregion

        #region -- Chart Export Functions ----------------------------------------------------------------------------------
        private void Plot_ExportPNG_Click(object sender, RoutedEventArgs e) {
            // Load the files to get the dates
            var dlg = new SaveFileDialog() {
                Filter = "Image Files|*.png|All Files|*.*"
            };
            if (dlg.ShowDialog() == true) {
                var pngExporter = new PngExporter { Width = 1920, Height = 1080, Background = OxyColors.White };
                pngExporter.ExportToFile(chart.ActualModel, dlg.FileName);
            }
        }
        private void Plot_ExportPDF_Click(object sender, RoutedEventArgs e) {
            // Load the files to get the dates
            var dlg = new SaveFileDialog() {
                Filter = "Portable Document Format Files|*.pdf|All Files|*.*"
            };
            if (dlg.ShowDialog() == true) {
                var pdfExporter = new PdfExporter { Width = 3840, Height = 2160, Background = OxyColors.White };
                pdfExporter.ExportToFile(chart.ActualModel, dlg.FileName);
            }
        }
        private async void Plot_ExportCSV_Click(object sender, RoutedEventArgs e) {
            using (CommonSaveFileDialog dlg = new CommonSaveFileDialog()) {
                dlg.Filters.Add(new CommonFileDialogFilter("Comma Seperated Values", "*.csv"));
                dlg.Filters.Add(new CommonFileDialogFilter("All Files", "*.*"));
                dlg.Title = "Select the export location";

                if (dlg.ShowDialog() == CommonFileDialogResult.Ok) {
                    LoadingBusyBar.IsIndeterminate = true;
                    TagsList.IsEnabled = false;
                    StatusBarText.Text = "Exporting...";

                    // Get a copy of data so we can do this async
                    List<string> tag_headers = new List<string>(chart.Series.Count);
                    List<List<DataPoint>> data = new List<List<DataPoint>>(chart.Series.Count);
                    foreach (var s in chart.Series) {
                        // Get tag name / header
                        SeriesOptions opt = (SeriesOptions)s.Tag;
                        if (opt != null) {
                            if (!opt.IsTrendLine) {
                                tag_headers.Add(opt.TagName);
                            }
                            else {
                                tag_headers.Add(string.Format("Trend Of {0}", opt.TagName));
                            }
                        }
                        else {
                            tag_headers.Add("Unknown Tag");
                        }
                        // Get list of points
                        data.Add((List<DataPoint>)s.ItemsSource);
                    }

                    // Big Task
                    await Task.Run(() => {
                        // Export each tag                       

                        var text = ExportSeriesToCSV(data, tag_headers);
                        File.WriteAllLines(dlg.FileName, text.ToArray());
                    });

                    // Cleanup
                    LoadingBusyBar.IsIndeterminate = false;
                    TagsList.IsEnabled = true;
                    StatusBarText.Text = "CSV Export Completed.";
                }
            }
        }
        private List<string> ExportSeriesToCSV(List<List<DataPoint>> points, List<string> tags) {
            if (points.Count < 1 || tags.Count < 1 || points.Count != tags.Count)
                return new List<string>();

            var lines = new List<string>();
            string header = string.Format("{0},{1},", "time", string.Join(",", tags));

            // Get times
            var times = new List<double>();
            for (int i = 0; i < points.Count; i++) {
                times.AddRange(points[i].Where(x => !times.Contains(x.X)).Select(x => x.X).ToList());
            }
            times.Sort();

            // Export each tag name
            lines.Add(header);

            // Export each value for each time
            foreach (var time in times) {
                string line = string.Format("{0},", time);
                foreach (var series in points) {
                    var vals = series.Where(x => x.X == time).Select(x => x.Y);
                    if (vals.Count() < 1) {
                        line = string.Format("{0},", line); // Skip this value
                    }
                    else {
                        line = string.Format("{0}{1},", line, vals.First());
                    }
                }
                lines.Add(line);
            }

            // Done
            return lines;
        }
        #endregion

        #region -- UI Utility (Splitter Closing) ---------------------------------------------------------------------------
        private void LeftGridSplitterClick(object sender, MouseButtonEventArgs e) {
            if (QueryPanel.Visibility == Visibility.Collapsed) {
                QueryPanel.Visibility = Visibility.Visible;
                LayoutGrid.ColumnDefinitions[0].Width = (GridLength)QueryPanel.Tag;
            }
            else {
                QueryPanel.Visibility = Visibility.Collapsed;
                QueryPanel.Tag = LayoutGrid.ColumnDefinitions[0].Width;
                LayoutGrid.ColumnDefinitions[0].Width = new GridLength(0);
            }
        }
        private void RightGridSplitterClick(object sender, MouseButtonEventArgs e) {
            if (TagPanel.Visibility == Visibility.Collapsed) {
                TagPanel.Visibility = Visibility.Visible;
                LayoutGrid.ColumnDefinitions[4].Width = (GridLength)TagPanel.Tag;
            }
            else {
                TagPanel.Visibility = Visibility.Collapsed;
                TagPanel.Tag = LayoutGrid.ColumnDefinitions[4].Width;
                LayoutGrid.ColumnDefinitions[4].Width = new GridLength(0);
            }
        }
        private void Chart_MouseDown(object sender, MouseButtonEventArgs e) {
            LoadProperties(null);
            DataSeriesBox.SelectedIndex = -1;
        }
        #endregion

        #region -- Properties ----------------------------------------------------------------------------------------------
        private void LoadPropertyComboBoxes() {
            // Colors
            PropLineColor.Items.Clear();
            PropMarkerBorderColor.Items.Clear();
            PropMarkerFillColor.Items.Clear();
            PropTrendLineColor.Items.Clear();
            //foreach (var color in typeof(OxyColors).GetFields())
            //{
            //    PropLineColor.Items.Add(color.Name);
            //    PropMarkerBorderColor.Items.Add(color.Name);
            //    PropMarkerFillColor.Items.Add(color.Name);
            //    PropTrendLineColor.Items.Add(color.Name);
            //}
            foreach (var color in ChartColors) {
                PropLineColor.Items.Add(color.Key);
                PropMarkerBorderColor.Items.Add(color.Key);
                PropMarkerFillColor.Items.Add(color.Key);
                PropTrendLineColor.Items.Add(color.Key);
            }

            // Line Type
            PropLineType.Items.Clear();
            foreach (var type in Enum.GetValues(typeof(LineStyle)))
                PropLineType.Items.Add(type);

            // Marker Type
            PropMarkerType.Items.Clear();
            foreach (var type in Enum.GetValues(typeof(MarkerType)))
                PropMarkerType.Items.Add(type);

            // Chart Legend Position Location
            LegendPosition.Items.Clear();
            foreach (var type in Enum.GetValues(typeof(LegendPosition)))
                LegendPosition.Items.Add(type);

        }
        private void LoadProperties(TagData tag) {
            if (tag == null) {
                // -- Select none
                SelectedTag = null;
                // -- Hide the panel
                PropertiesPanel.Visibility = Visibility.Collapsed;
                PropTagLbl.Content = string.Empty;
            }
            else {
                // -- Select the tag
                SelectedTag = tag;
                // -- Show the panel
                PropertiesPanel.Visibility = Visibility.Visible;

                LineSeries series = (LineSeries)GetSeries(tag.Name);

                // -- Load the values for this tag
                // Tag Name
                PropTagLbl.Content = tag.Name;
                PropAxisAssign.SelectedIndex = series.YAxisKey == LeftAxisKey ? 0 : 1;
                //PropLineColor.Text = series.Color.ToOxyColor().GetColorName();
                //PropMarkerBorderColor.Text = series.MarkerStroke.ToOxyColor().GetColorName();
                //PropMarkerFillColor.Text = series.MarkerFill.ToOxyColor().GetColorName();
                PropLineColor.Text = ChartColors.FirstOrDefault(x => x.Value == series.Color).Key;
                PropMarkerBorderColor.Text = ChartColors.FirstOrDefault(x => x.Value == series.MarkerStroke).Key;
                PropMarkerFillColor.Text = ChartColors.FirstOrDefault(x => x.Value == series.MarkerFill).Key;

                PropLineType.Text = Enum.GetName(typeof(LineStyle), series.LineStyle);
                PropMarkerType.Text = Enum.GetName(typeof(MarkerType), series.MarkerType);

                SeriesOptions opts = series.Tag as SeriesOptions;
                PropTrendingCheck.IsChecked = opts.TrendLineIndex > -1;
                if (opts.TrendLineIndex > -1 && opts.TrendLineIndex < chart.Series.Count)
                    PropTrendLineColor.Text = ChartTrendColors.FirstOrDefault(x => x.Value == chart.Series[opts.TrendLineIndex].Color).Key;
                else {
                    PropTrendLineColor.Text = string.Empty;
                    PropTrendLineColor.IsEnabled = false;
                }
            }
        }
        private void LegendPosition_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (chart != null && e.AddedItems.Count > 0)
                chart.LegendPosition = (LegendPosition)e.AddedItems[0];
        }
        private void PropAxisAssign_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (SelectedTag != null) {
                if (GetSeries(SelectedTag.Name) is LineSeries series) {
                    if (PropAxisAssign.SelectedIndex == 0) {
                        series.YAxisKey = LeftAxisKey;
                    }
                    else {
                        series.YAxisKey = RightAxisKey;

                        if (chart.Axes.Count == 2) // Right axis doesn't exist, so create it
                        {
                            chart.Axes.Add(MakeRightAxis());
                            RightAxisEnabled.IsChecked = true;
                        }
                    }

                    var opts = (SeriesOptions)series.Tag;
                    if (opts.TrendLineIndex > -1)
                        ((LineSeries)chart.Series[opts.TrendLineIndex]).YAxisKey = series.YAxisKey;
                }
            }
        }
        private void PropLineColor_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (SelectedTag != null) {
                var series = GetSeries(SelectedTag.Name) as LineSeries;
                if (e.AddedItems.Count < 1 || series == null)
                    return;

                if (ChartColors.ContainsKey((string)e.AddedItems[0]))
                    series.Color = ChartColors[(string)e.AddedItems[0]];
                else
                    series.Color = ChartColors.First().Value;

                //foreach (var color in typeof(OxyColors).GetFields())
                //{
                //    if (color.Name == (string)e.AddedItems[0])
                //    {
                //        series.Color = ((OxyColor)color.GetValue(null)).ToColor();
                //    }
                //}
            }
        }
        private void PropMarkerBorderColor_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (SelectedTag != null) {
                var series = GetSeries(SelectedTag.Name) as LineSeries;
                if (e.AddedItems.Count < 1 || series == null)
                    return;

                if (ChartColors.ContainsKey((string)e.AddedItems[0]))
                    series.MarkerStroke = ChartColors[(string)e.AddedItems[0]];
                else
                    series.MarkerStroke = ChartColors.First().Value;

                //foreach (var color in typeof(OxyColors).GetFields())
                //{
                //    if (color.Name == (string)e.AddedItems[0])
                //    {
                //        series.MarkerStroke = ((OxyColor)color.GetValue(null)).ToColor();
                //    }
                //}
            }
        }
        private void PropMarkerFillColor_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (SelectedTag != null) {
                var series = GetSeries(SelectedTag.Name) as LineSeries;
                if (e.AddedItems.Count < 1 || series == null)
                    return;

                if (ChartColors.ContainsKey((string)e.AddedItems[0]))
                    series.MarkerFill = ChartColors[(string)e.AddedItems[0]];
                else
                    series.MarkerFill = ChartColors.First().Value;

                //foreach (var color in typeof(OxyColors).GetFields())
                //{
                //    if (color.Name == (string)e.AddedItems[0])
                //    {
                //        series.MarkerFill = ((OxyColor)color.GetValue(null)).ToColor();
                //    }
                //}
            }
        }
        private void PropMarkerType_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (SelectedTag != null) {
                var series = GetSeries(SelectedTag.Name) as LineSeries;
                if (e.AddedItems.Count < 1 || series == null || e.AddedItems[0] == null)
                    return;

                if ((MarkerType)e.AddedItems[0] == MarkerType.Custom)
                    PropMarkerType.SelectedItem = e.RemovedItems;
                else
                    series.MarkerType = (MarkerType)e.AddedItems[0];
            }
        }
        private void PropLineType_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (SelectedTag != null) {
                var series = GetSeries(SelectedTag.Name) as LineSeries;
                if (e.AddedItems.Count < 1 || series == null || e.AddedItems[0] == null)
                    return;

                series.LineStyle = (LineStyle)e.AddedItems[0];
            }
        }
        private void PropTrendingCheck_Click(object sender, RoutedEventArgs e) {
            if (SelectedTag != null) {
                LineSeries series = (LineSeries)GetSeries(SelectedTag.Name);
                SeriesOptions opts = (SeriesOptions)series.Tag;
                bool leftAxis = (series.YAxisKey == LeftAxisKey);

                // We want a trend line, and we don't have one already
                if (PropTrendingCheck.IsChecked ?? false && opts.TrendLineIndex < 0) {
                    // Create the trend series
                    int trendSeriesID = CreateSeries(SelectedTag, leftAxis, true).Result;

                    // Assign this trend to the tag's series
                    opts.TrendLineIndex = trendSeriesID;

                    // Complete the circle
                    (chart.Series[trendSeriesID].Tag as SeriesOptions).TrendLineIndex = chart.Series.IndexOf(series);

                    // Allow adjustment of trend line color
                    PropTrendLineColor.Text = ChartTrendColors.FirstOrDefault(x => x.Value == chart.Series[trendSeriesID].Color).Key;
                    PropTrendLineColor.IsEnabled = true;
                }
                // We don't want a trend line, and we have one
                else if (!(PropTrendingCheck.IsChecked ?? false) && opts.TrendLineIndex > -1) {
                    // Delete the trend line series
                    chart.Series.RemoveAt(opts.TrendLineIndex);

                    // Indicate we don't have a trend line
                    opts.TrendLineIndex = -1;

                    // Disable Trend Line Color Selection
                    PropTrendLineColor.IsEnabled = false;
                    PropTrendLineColor.Text = string.Empty;
                }
                chart.InvalidatePlot(false);
            }
        }
        private void PropTrendLineColor_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (SelectedTag != null && (PropTrendingCheck.IsChecked ?? false)) {
                int i = ((GetSeries(SelectedTag.Name) as LineSeries).Tag as SeriesOptions).TrendLineIndex;
                LineSeries series = (chart.Series[i] as LineSeries);

                if (e.AddedItems.Count < 1 || series == null)
                    return;

                if (ChartColors.ContainsKey((string)e.AddedItems[0]))
                    series.Color = ChartTrendColors[(string)e.AddedItems[0]];
                else
                    series.Color = ChartTrendColors.First().Value;

                //foreach (var color in typeof(OxyColors).GetFields())
                //{
                //    if (color.Name == (string)e.AddedItems[0])
                //    {
                //        series.Color = ((OxyColor)color.GetValue(null)).ToColor();
                //    }
                //}                
            }
        }
        private void AxisItems_Selected(object sender, RoutedEventArgs e) {
            TagData tag = GetTag((string)DataSeriesBox.SelectedItem);
            LoadProperties(tag);
        }






        #endregion
    }
    public struct QueryTagEntry
    {
        public int TagID;
        public string TagName;
        public bool IsTrendLine;
        public bool IsRightAxis;
    }
}
