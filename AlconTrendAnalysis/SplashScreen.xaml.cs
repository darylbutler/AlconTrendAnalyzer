﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AlconTrendAnalysis
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {
        public int FilesAdded = 0;
        public long RowsAdded = 0;
        public SplashScreen()
        {
            InitializeComponent();            
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Create the db if it doesn't exist
            Database.CheckDatabase();

            if (Properties.Settings.Default.DatabasePath != null)
            {
                // Check for New Files for the Database
                var x = await Database.CheckPathsForNewFiles(
                    new Progress<Tuple<int,int>>(percent =>
                    {
                        // Update label
                        ProgressText.Text = string.Format("Processing file {0} of {1}",
                            percent.Item1, percent.Item2);

                        // Update Progress bar
                        ProgressBar.IsIndeterminate = false;
                        ProgressBar.Maximum = percent.Item2;
                        ProgressBar.Minimum = 0;
                        ProgressBar.Value = percent.Item1;
                    }), 
                    24 // Since we're starting and user is waiting on us, only load a few files so we can load the rest in the bg
                       // that's files per watch path
                );
                FilesAdded = x.Item1;
                RowsAdded = x.Item2;

                // Update Screen
                ProgressText.Text = string.Format("Found {0} new files, added {1} rows.<LineBreak/>Removing old values and vacuuming database...",
                    FilesAdded, RowsAdded);

                // Vacuum Database
                Database.DropOldRecords();
            }                

            // Done
            Close();
        }
    }
}
