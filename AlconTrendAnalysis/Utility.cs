﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlconTrendAnalysis
{
    public class Utility
    {
        const string LogPath = "debuglog";
        public const bool SingleThreaded = false;
        private static readonly object _syncObject = new object();  // Used for multiple threaded access to logger
        public static bool WarnedLogError = false;  // true if we've warned the user once
        private static StreamWriter stream;

        static Utility()
        {
            lock (_syncObject)
            {
                if (!File.Exists(LogPath))
                {
                    stream = new StreamWriter(LogPath);
                }
                else
                {
                    const long maxLogSize = 1000000 * 50;  // 50mb
                    bool appendLogFile = (new FileInfo(LogPath).Length < maxLogSize);
                    stream = new StreamWriter(LogPath, appendLogFile, Encoding.Default);
                }                
            }            
        }

        public static void Log(string txt, params object[] objs)
        {           
            string text = string.Format(txt, objs);
            try
            {
                lock(_syncObject)
                {
                    stream.WriteLine(string.Format("{0} -- {1}", DateTime.Now.ToString(), text));
                    stream.Flush();
                }                
            }
            catch (Exception ex)
            {
                if (!WarnedLogError)
                {
                    WarnedLogError = true;
                    System.Windows.MessageBox.Show(string.Format("There was an error opening the log file.\nThe error was: {0}\nThe message that was being logged was: {1}", 
                        ex.Message,
                        text));
                }
            }        
        }
    }
}
