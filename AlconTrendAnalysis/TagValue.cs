﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlconTrendAnalysis
{
    [Serializable]
    public class TagValue
	{
		public ulong Time;
		public double Value;

		public TagValue(ulong time, double val)
		{
			Time = time;
			Value = val;
		}

		public DateTime DateTime
		{
			get
			{
                return OracleTime.ToDateTime(Time);
			}
		}
	}
}
