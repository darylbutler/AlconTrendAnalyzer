﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;

namespace TrendDataAgg
{
    // Types
    public enum ActivityState
    {
        NotConfiged,    // Can't run becuase we don't know where to look for the db or no watch paths
        NotRunning,     // We can run, waiting to start
        Running,        // Running, but not updating anything (Should be default state)
        Updating,       // Running and Updating the db with Files
    }

    public class AggregatorApp : ApplicationContext
    {
        // Options
        const bool OptLogStateChanges = true;
        public static Object LogLock = new object();
        public Settings Settings = new Settings();
        public bool CancelUpdate = false;       // App Requests to quit from the update

        // Members
        public ActivityState State = ActivityState.NotConfiged;    // Default state
        private DispatcherTimer DataUpdateTimer = new DispatcherTimer();
        private List<string> BlackList = new List<string>();
        private MainForm UIForm;
        private NotifyIcon Icon;
        private DataStore Store;

        private FileSystemWatcher queryWatcher;    // For watching for queries      
        private List<string> queryWatcherTasks = new List<string>();

        // -- Constructor -----------------------------------------------------------------------------------------
        public AggregatorApp() {
            UIForm = new MainForm(this);
            SetupIcon();
            StartUp();
            //SetupQueryWatcher();
        }
        private void SetupIcon() {
            // Menu
            ContextMenu menu = new ContextMenu();
            menu.MenuItems.Add(new MenuItem("Exit", (sender, e) => {
                Icon.Dispose();
                Application.Exit();
            }));

            // Icon
            Icon = new NotifyIcon {
                ContextMenu = menu,
                Visible = true
            };
            Icon.MouseDown += (sender, e) => {
                if (e.Button == MouseButtons.Left) {
                    if (UIForm == null) {
                        UIForm = new MainForm(this);
                        UIForm.Show();
                    } else {
                        UIForm?.Close();
                        UIForm.Dispose();
                        UIForm = null;
                    }
                }
            };
        }
        private void SetupQueryWatcher() {
            string path = Path.Combine(Path.GetDirectoryName(Settings.DataStorePath), "Queries");
            queryWatcher = new FileSystemWatcher() {
                Path = path,
                Filter = "*.*",
                NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName,
            };
            //queryWatcher.Changed += new FileSystemEventHandler(QueryWatcher_OnChanged);
            queryWatcher.Created += new FileSystemEventHandler(QueryWatcher_OnChanged);
            queryWatcher.EnableRaisingEvents = true;
        }
        private void QueryWatcher_OnChanged(object source, FileSystemEventArgs e) {
            if (e.FullPath.EndsWith(".result")) {
                string path = e.FullPath.Substring(0, e.FullPath.Length - 7);

                // Remove the task if we have it
                if (queryWatcherTasks.Contains(path)) {
                    Log(string.Format("Finished Query File: {0}...", path));
                    queryWatcherTasks.Remove(path);
                }
            } else if (e.FullPath.EndsWith(".qry")) {
                // Start a new query
                string resultPath = string.Format("{0}.result", e.FullPath);

                // See if we're processing / processed this query already
                if (queryWatcherTasks.Contains(e.FullPath) || File.Exists(resultPath)) {
                    return;
                }
                Log(string.Format("Starting Query File: {0}...", e.Name));

                //Try to parse the file
                if (!ParseQueryRequestFile(e.FullPath, out ulong start, out ulong end, out List<string> tags)) {
                    Log(string.Format("Error Processing Query File: {0}...", e.Name));
                    File.WriteAllText(resultPath, "Error Reading Query Request Parameters");
                    return;
                }

                queryWatcherTasks.Add(e.FullPath);
                Task.Factory.StartNew(async () => {
                    await Task.Run(() => {
                        // Get the Data
                        //var data = Store.QueryTags(start, end, tags);
                        var data = DataStore.QueryTags(start, end, tags, Settings.DataStorePath);

                        // Write the data to File
                        WriteTagDataToFile(resultPath, data);
                    });
                });
            }

            //var files = Directory.EnumerateFiles(queryWatcher.Path, "*.qry");
            //var newFiles = new List<String>();

            //// Find the files that we haven't started processing yet
            //foreach (var file in files) {
            //    if (!File.Exists(string.Format("{0}.result", file))) {
            //        newFiles.Add(file);
            //    }
            //}

            //// Foreach File, start processing them
            //foreach (var file in newFiles) {
            //    string resultPath = string.Format("{0}.result", file);
            //    // Try to parse the file
            //    if (!ParseQueryRequestFile(file, out ulong start, out ulong end, out List<string> tags)) {
            //        File.WriteAllText(resultPath, "Error Reading Query Request Parameters");
            //        return;
            //    }

            //    // Write the result file, to indicate that we've started processing the file
            //    File.WriteAllText(resultPath, "Working...");

            //    queryWatcherTasks.Add(Task.Run(async () => {
            //        await Task.Run(() => {
            //            // Get the Data
            //            //var data = Store.QueryTags(start, end, tags);
            //            var data = DataStore.QueryTags(start, end, tags, Settings.DataStorePath);

            //            // Write the data to File
            //            WriteTagDataToFile(resultPath, data);
            //        });
            //    }));
            //}

            //Task.WaitAll(queryWatcherTasks.ToArray());
            //queryWatcherTasks.Clear();
        }

        private void CheckForQueries() {
            // Look for new queries
            // TODO
        }

        // -- Startup Sequence ------------------------------------------------------------------------------------
        private void StartUp() {
            UIForm.Show();
            Log("> Begin Starting Up");

            // Check if we have options
            bool ConfiguredOkay =
                !string.IsNullOrWhiteSpace(Settings.DataStorePath) &&
                Settings.DataPaths?.Count > 0 &&
                !string.IsNullOrWhiteSpace(Settings.ArchivePath) &&
                !string.IsNullOrWhiteSpace(Settings.BlacklistPath);

            if (ConfiguredOkay)
                SetState(ActivityState.NotRunning);
            else {
                SetState(ActivityState.NotConfiged);
                Log("Not Configured.");
                UIForm.Show();
            }

            // Start if we can
            if (State == ActivityState.NotRunning) {
                Store = new DataStore(Settings.DataStorePath, StoreAccess.Append) {
                    // Keep the store from growing too large
                    TruncateOldTimes = true,
                    //TruncateTimesBefore = OracleTime.ToLong(DateTime.Now.AddMonths(-4)),
                    MaxRowsPerShard = Settings.MaxRowsPerShard
                };

                // If we're still okay, let's start the update timer
                if (State == ActivityState.NotRunning) {
                    StartTicking();
                } else {
                    // Show the form, because there was a error
                    Log("> End Starting Up.");
                    UIForm.Show();
                }
            }
        }
        private void StartTicking() {
            if (State != ActivityState.NotRunning)
                return;

            SetState(ActivityState.Running);
            Log("> End Starting Up.");

            // Create a new timer
            DataUpdateTimer = new DispatcherTimer() {
                Interval = TimeSpan.FromSeconds(Settings.UpdateInterval),
            };
            DataUpdateTimer.Tick += Tick;

            // Start immediately
            Tick(null, null);
            DataUpdateTimer.Start();
        }
        private async void Tick(object sender, EventArgs e) {
            // Prevent the timer from running until we're done
            DataUpdateTimer.Stop();

            // Prevent entering if we're already loading or not ready
            if (State == ActivityState.Running) {
                // Start the update
                SetState(ActivityState.Updating);

                // Always try clean up if we need to
                if (Settings.FilesToDelete.Count > 0)
                    await DeleteFilesInToDo();

                // Decide what we're doing
                if ((DateTime.Now.TimeOfDay.TotalHours < 6 ||        // Before 6am   (0600)
                    DateTime.Now.TimeOfDay.TotalHours > 22) &&       // After 10pm   (2200)
                    Settings.LastDefragTime.AddHours(23) < DateTime.Now)
                    await DefragDataStore();
                else
                    await CheckForNewData();

                // Don't update if we're trying to cancel.
                if (CancelUpdate) {
                    CancelUpdate = false;
                    Stop();
                } else {
                    // Otherwise, return to ticking
                    SetState(ActivityState.Running);
                    DataUpdateTimer.Start();
                }
            }
        }
        private async Task CheckForNewData() {
            // Prevent entering if we're already loading or not ready
            if (State == ActivityState.Updating) {
                Log("Checking for new data started.");
                Stopwatch sw = new Stopwatch();
                sw.Start();

                // Get a list of new files
                List<string> filesToAdd = await Task.Run(async () => {
                    return await Task.Run(() => {
                        return GetNewestFiles();
                    });
                });

                // Check for cancel
                if (CancelUpdate) {
                    CancelUpdate = false;
                    Log("Update Canceled.");
                    return;
                }

                // If we have files to add, add them
                ulong rowsBefore = Store.RowCount;
                sw.Start();
                int filesAdded = await MoveAndStoreFiles(filesToAdd);
                sw.Stop();
                ulong rowsAdded = Store.RowCount - rowsBefore;

                Log(string.Format("Timer Update Done, Found {0} File(s), Added {1} row(s) [{2} hour(s)], took {3}ms.",
                    filesAdded, rowsAdded, Math.Round((double)rowsAdded / 720d) /* rows per hour*/, sw.ElapsedMilliseconds));
            }
        }
        private List<string> GetNewestFiles() {
            var paths = Settings.DataPaths;

            // -- Get list of files in directory and see which haven't been read
            DateTime RetainTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).
                AddMonths(-3);

            List<FileInfo> fileinfos = new List<FileInfo>();
            int filesAddedFromThisPath = 0;
            const int maxFiles = 10;
            foreach (string path in paths) {
                filesAddedFromThisPath = 0;

                // FIX for the shitty folder layout on the Terminal Server
                // it currently has APL01 and APL02 data, in csv format, in the root directory, 
                // then folders for APL03 and APL04, which need to be read differently becuase the files
                //  are stored under folder by machine center
                //
                // It sucks but I think this is the best way                

                // -- Get list of files from last 3 months
                SearchOption opt = (path.ToLower().Contains("apl03") || path.ToLower().Contains("apl04")) ?
                    SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;

                var files =
                    new DirectoryInfo(path).
                    GetFiles("*", opt).
                    Where(f => (f.Extension.ToLower() == ".csv" || f.Extension.ToLower() == ".txt") &&
                                f.CreationTime > RetainTime && !BlackList.Contains(f.FullName)).ToList();

                // Newest files first
                files.Sort((x, y) => {
                    return y.CreationTime.CompareTo(x.CreationTime);
                });

                // Include Only files that are not in the database already
                foreach (var file in files) {
                    // Only load max file count if provided
                    if (maxFiles > 0 && filesAddedFromThisPath >= maxFiles)
                        break;

                    if (!Store.HasFileBeenProcessed(file.FullName)) {
                        fileinfos.Add(file);

                        if (!path.ToLower().Contains("manual"))
                            filesAddedFromThisPath++;
                    }
                }
            }

            // Sort the files into a list of paths (So newest files get added first)
            fileinfos.Sort((x, y) => -1 * x.CreationTime.CompareTo(y.CreationTime));
            List<string> filesToAdd = new List<string>(fileinfos.Select(y => y.FullName));
            return filesToAdd;
        }
        private async Task<int> MoveAndStoreFiles(List<string> filesToAdd) {
            // If we have files to add, add them
            int filesAdded = 0;
            if (filesToAdd.Count > 0) {
                foreach (var file in filesToAdd) {
                    // Check for cancel
                    if (CancelUpdate) {
                        //CancelUpdate = false;
                        Log("Update Canceled.");
                        return filesAdded;
                    }
                    // Process the Data
                    var log = string.Empty;
                    try {
                        log = string.Format("Starting: {0}", file);

                        await Task.Run(async () => {
                            await Task.Run(() => {
                                // Parse the file
                                Store data = null;
                                if (Path.GetExtension(file) == ".csv")
                                    data = CSVReader.ParseCSV(file);
                                else
                                    data = CSVReader.ParseTXT(file, Store);
                                log = string.Format("{0}, Parsed", log);

                                // Archive the file
                                CSVReader.StoreCSVInWeeklyZip(Settings.ArchivePath, Path.GetFileNameWithoutExtension(file), data);
                                log = string.Format("{0}, Archived", log);

                                // Add the data to the DataStore
                                Store.AddData(data, file);
                                log = string.Format("{0}, Stored", log);
                                filesAdded++;
                            });
                        });

                        if (Settings.RemoveFileAfterParse) {
                            // Delete the file we read -- We don't care if it fails
                            bool deleteError = false;
                            try { File.Delete(file); } catch (Exception) { deleteError = true; }
                            if (!deleteError)
                                Log(string.Format("{0}, Original Deleted, and Done.", log));
                            else {
                                Log(string.Format("{0}, Original File Delete was Denied (Will retry later), and Done.", log));
                                Settings.FilesToDelete.Add(file);
                            }
                        } else
                            Log(string.Format("{0}, and Done.", log));
                    } catch (Exception ex) {
                        // Log what we did before we had an error
                        log = string.Format("{0}, then errored...", log);
                        Log(log);
                        Log(string.Format("Error reading file ({0})\n\tError: {1} - {2}", file, ex.Message, ex.InnerException?.Message));
                        BlackList.Add(file);
                        if (Settings.RemoveFileAfterParse)
                            try { File.Move(file, Path.Combine(Settings.BlacklistPath, Path.GetFileName(file))); } catch (Exception) { }
                    }
                }
            }

            // Save the tag descriptions after the update
            Store.SaveTagDescriptions();

            // Return the info
            return filesAdded;
        }
        private async Task DefragDataStore() {
            // Prevent entering if we're already loading or not ready
            if (State != ActivityState.Updating)
                return;

            Log("Defrag started.");
            // try to perform a Defrag
            await Task.Run(async () => {
                await Task.Run(() => {
                    bool defragError = false;
                    try {
                        Store.TruncateOldTimes = true;
                        Store.TruncateTimesBefore = OracleTime.ToLong(DateTime.Now.AddMonths(-2));
                        Store.Defrag();
                    } catch (Exception e) {
                        defragError = true;
                        Log(string.Format("Defrag attempted but Datastore had an error: {0}", e.Message));
                    }

                    // Finished, save the last time we've defragged
                    if (!defragError) {
                        Settings.LastDefragTime = DateTime.Now;
                        Settings.Save();
                    }
                });
            });
            Log("Defrag complete.");
        }
        private async Task DeleteFilesInToDo() {
            // Prevent entering if we're already loading or not ready
            if (State != ActivityState.Updating)
                return;
        
            var filesDeleted = await Task.Run(async () => {
                return await Task.Run(() => {
                    var f = new List<string>();
                    foreach (var file in Settings.FilesToDelete) {
                        // Try to delete each file
                        try { File.Delete(file); } catch (Exception) { }
                        // If it was deleted, add it to the list
                        if (!File.Exists(file))
                            f.Add(file);
                    }
                    return f;
                });
            });

            // Remove the ones we deleted successfully
            foreach (var file in filesDeleted)
                Settings.FilesToDelete.Remove(file);

            if (filesDeleted.Count > 0) {
                Settings.Save();
                Log(string.Format("{0} file(s) cleaned up from To Delete list.", filesDeleted.Count));
            }
        }

        // -- Mainform State Control -----------------------------------------------------------------------------
        public void Start() {
            if (State != ActivityState.Updating)
                StartUp();
        }
        public void Stop() {
            // Stop the execution
            SetState(ActivityState.NotRunning);
            DataUpdateTimer?.Stop();
            DataUpdateTimer = null;
        }
        public void DumpStore(string path) {
            if (Store == null)
                return;
            Store.ExportToFile(path, false);
        }
        public void Defrag() {
            if (State != ActivityState.NotRunning || Store == null)
                return;
            Store.TruncateOldTimes = true;
            Store.TruncateTimesBefore = OracleTime.ToLong(DateTime.Now.AddMonths(-2));
            Store.Defrag();
        }

        // -- [internal] State Management ------------------------------------------------------------------------
        private void SetState(ActivityState state) {
            if (OptLogStateChanges && State != state)
                Log(string.Format("     [StateChange: From {0}, To {1}]", State, state));

            State = state;
            UpdateIcon();
            UIForm?.UpdateUIBecauseActivityStateChanged();
        }
        private void UpdateIcon() {
            //TODO Tooltips
            switch (State) {
                case ActivityState.NotConfiged:
                    Icon.Icon = Properties.Resources.NotConfiged;
                    break;

                case ActivityState.NotRunning:
                    Icon.Icon = Properties.Resources.NotRunning;
                    break;

                case ActivityState.Running:
                    Icon.Icon = Properties.Resources.Running;
                    break;

                case ActivityState.Updating:
                    Icon.Icon = Properties.Resources.Updating;
                    break;
            }
        }

        // -- Query Files ----------------------------------------------------------------------------------------
        public void ExecuteQueryFile(string path) {
            if (Store == null)
                return;

            string resultPath = string.Format("{0}.result", path);

            // Get the Query Details
            if (!ParseQueryRequestFile(path, out ulong start, out ulong end, out List<string> tags)) {
                File.WriteAllText(resultPath, "Error Reading Query Request Parameters");
                return;
            }

            // Execute the Query
            //var data = DataStore.QueryTags(start, end, tags, Settings.DataStorePath);
            //var data = Store.QueryTags(start, end, tags);

            //// Write the data to File
            //WriteTagDataToFile(resultPath, data);
            Store.QueryTags_FileProgress(start, end, tags, resultPath);

        }
        private bool ParseQueryRequestFile(string path, out ulong start, out ulong end, out List<string> tags) {
            start = 0;
            end = 0;
            tags = new List<string>();

            if (File.Exists(path)) {
                var lines = File.ReadAllLines(path);
                uint relative_end = 0;
                foreach (var line in lines) {
                    var split = line.Split('=');
                    if (split.Count() > 1) {
                        switch (split[0].ToLower()) {
                            case "start":
                                if (ulong.TryParse(split[1], out ulong tStart)) start = tStart;
                                break;
                            case "end":
                                if (split[1].Length > 1 && split[1][0] == '+') {
                                    // relative
                                    relative_end = uint.Parse(split[1].Substring(1));
                                } else if (ulong.TryParse(split[1], out ulong tEnd)) {
                                    end = tEnd;
                                }
                                break;
                            case "starttime":
                                if (DateTime.TryParse(split[1], out DateTime tDTStart)) start = OracleTime.ToLong(tDTStart);
                                break;
                            case "endtime":
                                if (DateTime.TryParse(split[1], out DateTime tDTEnd)) end = OracleTime.ToLong(tDTEnd);
                                break;
                            case "tags":
                                tags.AddRange(split[1].Split(',').Where(x => x != string.Empty).Select(x => x));
                                break;
                        }
                    }
                }

                if (relative_end > 0)
                    end = start + (5 * relative_end);
            }
            return start > 0 && end > 0 && tags.Count > 0;
        }
        private void WriteTagDataToFile(string path, TagData data) {
            using (var writer = new StreamWriter(path, false)) {
                writer.WriteLine("time,{0}", data.Name);

                foreach (var val in data.Values) {
                    writer.WriteLine("{0},{1:0.00}", OracleTime.ToDateTime(val.Time).ToString("yyyy-MM-dd hh:mm:ss"), val.Value);
                }
            }
        }
        private void WriteTagDataToFile(string path, Store store) {
            using (var writer = new StreamWriter(path, false)) {
                writer.WriteLine("time,{0},", string.Join(",", store.TagOrder));

                foreach (var row in store.Rows) {
                    writer.Write("{0},", OracleTime.ToDateTime(row.Time).ToString("yyyy-MM-dd hh:mm:ss"));
                    foreach (var val in row.Values) {
                        writer.Write("{0:0.00},", val);
                    }
                    writer.WriteLine();
                }
            }
        }

        // -- Utilities ------------------------------------------------------------------------------------------
        private void Log(string text) {
            lock (LogLock) {
                using (StreamWriter writer = new StreamWriter("log", true, Encoding.Default)) {
                    writer.WriteLine(string.Format("{0} -- Aggregator -- {1}", DateTime.Now.ToString(), text));
                }
                // Log on the UI Thread (https://stackoverflow.com/questions/661561/how-do-i-update-the-gui-from-another-thread-in-c)
                if (UIForm != null) {
                    UIForm.Invoke((MethodInvoker)delegate {
                        UIForm.UILog(text);// Always show the log file in the UI (Not the otherway around, though)
                    });
                }
            }
        }
        private string PrettyFormatTimeSpan(TimeSpan span) {
            if (span.Days > 0)
                return string.Format("{0}d, {1}h, {2}m", span.Days, span.Hours, span.Minutes);
            if (span.Hours > 0)
                return string.Format("{0}h, {1}m ", span.Hours, span.Minutes);
            if (span.Minutes > 0)
                return string.Format("{0}m, {1}s ", span.Minutes, span.Seconds);

            return string.Format("{0}s", span.TotalSeconds);
        }
    }
}
