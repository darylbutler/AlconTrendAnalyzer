﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace TrendDataAgg
{
    /****
     * Changes: 
     *  - After test run, found the store size grew too large for the intended time range. Revamped store now to include the
     *      files, tags, and table into the .store file, and the actual rows / data into separate files called shards.
     *  - Changed header RowCount to 8bytes, since it could be large
     * 
     ***/

    public class Store
    {
        public List<string> TagOrder = new List<string>();  // The order of the values in the file
        public List<StoreRow> Rows = new List<StoreRow>();  // the rows to store

        public Store() { }
        public Store(List<TagData> tags) {
            TagOrder = new List<string>(tags.Count);
            Rows = new List<StoreRow>(tags.First().Values.Count);

            foreach (var tag in tags) {
                // Skip duplicate tags
                int col = TagOrder.IndexOf(tag.Name);
                if (col < 0) {
                    // Add this tag to the tag order
                    col = TagOrder.Count;
                    TagOrder.Add(tag.Name);
                }

                // Add each value
                foreach (var val in tag.Values) {
                    // Check if time exists already
                    int r = Rows.FindIndex((r2) => r2.Time == val.Time);
                    if (r < 0) {
                        // New Row
                        r = Rows.Count;
                        Rows.Add(new StoreRow(val.Time));
                    }
                    var row = Rows[r];    // reference to the current row

                    // Pad row's values to this tag's column
                    while (row.Values.Count < col)
                        row.Values.Add(0);

                    // Add our value to this row
                    if (row.Values.Count > col)
                        row.Values[col] = val.Value;
                    else
                        row.Values.Add(val.Value);
                }
            }

            // Check each row to make sure they're the same row count (pad them with 0s if not)
            int tagCnt = TagOrder.Count;
            foreach (var row in Rows) {
                while (row.Values.Count < tagCnt)
                    row.Values.Add(0);

                System.Diagnostics.Debug.Assert(row.Values.Count == tagCnt);
            }
        }

        public static Store MergeStores(Store s1, Store s2) {
            // Add Store 2 to 1
            Store store = new Store();

            // add the columns from s2, and remember if we're ignore them
            List<bool> ignoreColumns = new List<bool>(s2.TagOrder.Count);
            store.TagOrder = s1.TagOrder;
            foreach (var tag in s2.TagOrder) {
                if (s1.TagOrder.Contains(tag)) {
                    ignoreColumns.Add(true);
                }
                else {
                    ignoreColumns.Add(false);
                    store.TagOrder.Add(tag);
                }
            }

            // Add each row in s1, looking for rows in s2
            List<StoreRow> rows = s2.Rows;
            foreach (var s1row in s1.Rows) {
                StoreRow row = s1row;

                // Add the s2 rows to s1
                int r_id = rows.FindIndex(x => x.Time == row.Time);

                for (int i = 0; i < s2.TagOrder.Count; i++) {

                    if (!ignoreColumns[i]) {
                        if (r_id > -1) {
                            row.Values.Add(rows[r_id].Values[i]);
                        }
                        else {
                            row.Values.Add(0);
                        }
                    }
                }

                store.Rows.Add(row);

                if (r_id > -1) {
                    rows.RemoveAt(r_id);
                }
            }

            // Add any remaining rows (These are times s2 had that s1 didn't)
            foreach (var s2row in rows) {
                StoreRow row = new StoreRow(s2row.Time);

                for (int i = 0; i < store.TagOrder.Count; i++) {

                    int idx = s2.TagOrder.IndexOf(store.TagOrder[i]);
                    if (idx < 0) {
                        row.Values.Add(0);
                    }
                    else {
                        row.Values.Add(s2row.Values[idx]);
                    }
                }
                store.Rows.Add(row);
            }

            // return the merged store
            store.Rows.Sort((x, y) => x.Time.CompareTo(y.Time));
            return store;
        }
    }
    public class StoreRow
    {
        public ulong Time;
        public List<double> Values = new List<double>();

        // Constructor
        public StoreRow(ulong time) => Time = time;

        // Helpers
        public DateTime GetDateTime { get { return OracleTime.ToDateTime(Time); } }
    }
    public enum StoreAccess
    {
        Query,
        Append,
        CreateNew
    }

    public class DataStore
    {
        // 4[Version] 1[Locked] 1[TimeLength] 1[ValueLength] 8[OldestTime] 8[NewestTime] 4[TagCount] 8[RowCount]
        const int HeaderBytes = 4 + 1 + 1 + 1 + 8 + 8 + 4 + 8;
        // Buffer amounts for reading / writing to datastore
        const int ReadBlock = 4096;
        const int WriteBlock = 4096;
        // This version of this DataStore implementation is v2
        const int DefaultVersion = 2;

        // Public
        public bool TruncateOldTimes = true;            // If true, when we do a defrag, we'll erase any rows older than TruncateTimesBefore
        public ulong TruncateTimesBefore = 0;           // Default to no deletions
        public uint TagCount { get; private set; }      // Count of tags in this database                  
        public ulong RowCount { get; private set; }     // Count of rows in the database
        public ulong NewestTime { get; private set; }   // Latest (Greatest) time in database
        public ulong OldestTime { get; private set; }   // Earliest (Least) time in database
        public ulong MaxRowsPerShard = 17280 * 30 * 2;  // Moved to settings
        public List<string> Files;
        public List<string> TagNames { get { return TagOrder; } }
        public List<TagData> Tags {
            get {
                List<TagData> tags = new List<TagData>();
                foreach (var name in TagOrder) {
                    string desc = string.Empty;
                    // First, check our custom list of descriptions
                    if (TagDescriptions.ContainsKey(name))
                        desc = TagDescriptions[name];
                    else {
                        // Fall back to CSV reader
                        desc = CSVReader.GetTagDescription(name);
                    }
                    // No matter what, prevent a tag with no description
                    if (desc == string.Empty)
                        desc = name;

                    tags.Add(new TagData() { Name = name, Description = desc });
                }
                return tags;
            }
        }
        public Dictionary<string, string> TagDescriptions;

        // Private
        private StoreAccess Mode = StoreAccess.Query;   // The rights we have to this database
        private UInt32 Version = 2;                     // Version of the file (Not Used)
        private bool Locked = false;                    // True is store is locked (Because it's being reorganized via Defrag)
        string Path;                                    // Path to the hardrive file
        Dictionary<ulong, Tuple<uint, long>> Table = null;   // Keep the Offset Table in memory (Key = time, Value = Tuple(shard#, offset))
        List<string> TagOrder;                          // List of Tag Names for their position in the rows
        private int _timeBytes = 4;                     // Byte length of time as start of a row
        private int _valueBytes = 4;                    // Byte length of values in a row

        // Calculated values        
        private int TimeBytes { get { return _timeBytes; } }
        private int ValueBytes { get { return _valueBytes; } }
        private long RowBytes { get { return TimeBytes + (TagCount * ValueBytes); } }
        private string TagDescPath { get { return string.IsNullOrEmpty(Path) ? string.Empty : System.IO.Path.ChangeExtension(Path, "tags"); } }
        private string FilesPath { get { return string.IsNullOrEmpty(Path) ? string.Empty : System.IO.Path.ChangeExtension(Path, "files"); } }
        private int Block { get { return 100000; } }   // Max # of rows to read into memory before moving them
        private double NullValue { get { return ValueBytes == 4 ? float.MaxValue : double.MaxValue; } }

        // ----------------------------------------------------------------------------------------
        // Creation
        public DataStore(StoreAccess mode = StoreAccess.Query) { InitialLoad(mode); }
        public DataStore(string p, StoreAccess mode = StoreAccess.Query) { Path = p; InitialLoad(mode); }
        private void InitialLoad(StoreAccess mode) {
            // Save the access Mode
            Mode = mode;

            // True if this data store was correct on the disk
            bool opened = false;

            // If the file exists, then attempt to load from it
            if (File.Exists(Path) && (mode == StoreAccess.Append || mode == StoreAccess.Query)) {
                using (var stream = new FileStream(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, ReadBlock)) {
                    if (stream.Length >= HeaderBytes) {
                        using (var reader = new BinaryReader(stream)) {
                            // Read Header -- 4[Version] 1[Locked] 1[TimeLength] 1[ValueLength] 8[OldestTime] 8[NewestTime] 4[TagCount] 8[RowCount]
                            Version = reader.ReadUInt32();
                            Locked = reader.ReadBoolean();
                            _timeBytes = reader.ReadByte();
                            _valueBytes = reader.ReadByte();
                            OldestTime = reader.ReadUInt64();
                            NewestTime = reader.ReadUInt64();
                            TagCount = reader.ReadUInt32();
                            RowCount = reader.ReadUInt64();
                            Debug.Assert(stream.Position == HeaderBytes, "Header reading in InitialLoad incorrect position after header load");

                            opened = true;

                            // Read Tag Order
                            TagOrder = new List<string>((int)TagCount);
                            for (int i = 0; i < TagCount; i++)
                                TagOrder.Add(reader.ReadString());

                            // Read the table
                            Table = new Dictionary<ulong, Tuple<uint, long>>((int)RowCount);

                            // Don't try to read past the RowCount
                            for (ulong i = 0; i < RowCount; i++) {
                                ulong time = reader.ReadUInt64();
                                uint shard = reader.ReadUInt32();
                                long offset = reader.ReadInt64();
                                Table.Add(time, new Tuple<uint, long>(shard, offset));
                            }

                            // Read in files list (Files we've already processed)
                            if (FilesExists())
                                Files = ReadFiles();
                            else {
                                Files = new List<string>();
                                if (mode != StoreAccess.Query)
                                    WriteFiles(Files);
                            }

                            // Read Tag descriptions
                            if (TagDescExists())
                                TagDescriptions = ReadTagDescriptions();
                            else {
                                TagDescriptions = new Dictionary<string, string>();
                                if (mode != StoreAccess.Query)
                                    WriteTagDesc(TagDescriptions);
                            }
                        }
                    }

                }
            }

            if (!opened) {
                if (mode == StoreAccess.Query) {
                    // We cannot recreate the datastore with query access, throw error
                    throw new Exception("Datastore not found! Cannot create new with Query access");
                }

                // Empty Offset table, no rows
                Version = DefaultVersion;
                TagCount = 0;
                RowCount = 0;
                NewestTime = OldestTime = 0;
                TagOrder = new List<string>();
                Table = new Dictionary<ulong, Tuple<uint, long>>();
                Files = new List<string>();
                TagDescriptions = new Dictionary<string, string>();
                UpdateStore();
            }
        }

        // ----------------------------------------------------------------------------------------
        // Table
        private void UpdateStore() {

            using (var stream = new FileStream(Path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read, WriteBlock)) {
                using (var writer = new BinaryWriter(stream)) {
                    // Write the header
                    Util_WriteHeader(writer, true);
                    stream.Flush(true);

                    // Write the tags
                    Util_WriteTags(writer, TagOrder);

                    // Write the table
                    foreach (var row in Table) {
                        ulong time = row.Key;
                        uint shard = row.Value.Item1;
                        long offset = row.Value.Item2;

                        writer.Write(time);
                        writer.Write(shard);
                        writer.Write(offset);
                    }

                    // We're done, unload the store
                    UnlockStore(stream);
                }
            }
        }

        // ----------------------------------------------------------------------------------------
        // Files
        private bool FilesExists() {
            return File.Exists(FilesPath);
        }
        private List<string> ReadFiles() {
            if (!FilesExists()) return new List<string>();

            using (var stream = new FileStream(FilesPath, FileMode.Open, FileAccess.Read)) {
                using (var reader = new BinaryReader(stream)) {
                    uint rows = reader.ReadUInt32();
                    var files = new List<string>((int)rows);

                    for (int i = 0; i < rows; i++) {
                        files.Add(reader.ReadString());
                    }
                    return files;
                }
            }
        }
        private void WriteFiles(List<string> files) {
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            using (var stream = new FileStream(FilesPath, FileMode.Create, FileAccess.Write)) {
                using (var writer = new BinaryWriter(stream)) {
                    writer.Write(files.Count);
                    foreach (var row in files)
                        writer.Write(row);
                }
            }
        }
        public bool HasFileBeenProcessed(string file) {
            return Files.Contains(file);
        }
        public void AddProcessedFile(string file) {
            // Check for mode allows this modification
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            if (!HasFileBeenProcessed(file)) {
                Files.Add(file);
                WriteFiles(Files);
            }
        }

        // ----------------------------------------------------------------------------------------
        // Tag Descriptions
        private bool TagDescExists() {
            return File.Exists(TagDescPath);
        }
        private Dictionary<string, string> ReadTagDescriptions() {
            var tags = new Dictionary<string, string>();
            if (!TagDescExists()) return tags;

            using (var stream = new FileStream(TagDescPath, FileMode.Open, FileAccess.Read))
            using (var reader = new StreamReader(stream)) {
                while (!reader.EndOfStream) {
                    string line = reader.ReadLine();
                    var split = line.Split(':', ';');

                    if (split.Length > 1) {
                        tags.Add(split[0], split[1]);
                    }
                }
            }
            return tags;
        }
        private void WriteTagDesc(Dictionary<string, string> tags) {
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            using (var stream = new FileStream(TagDescPath, FileMode.Create, FileAccess.Write))
            using (var writer = new StreamWriter(stream)) {
                foreach (var line in tags) {
                    writer.WriteLine("{0}:{1}", line.Key, line.Value);
                }
            }
        }
        public void SaveTagDescriptions() => WriteTagDesc(TagDescriptions);

        // ----------------------------------------------------------------------------------------
        // Modifying
        public void AddData(List<TagData> data, string file = null) {
            // Check for mode allows this modification
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            // 2018-10-30 - djb -- Disable file checking 
            //if (file != null && HasFileBeenProcessed(file))
            //    return;

            // Turn this data into a Store
            AddData(new Store(data), file);
        }
        public void AddData(Store store, string file = null) {
            // Check for mode allows this modification
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            // 2018-10-30 - djb -- Disable file checking 
            //if (file != null && HasFileBeenProcessed(file))
            //    return;

            // First, check if this data is new enough for us to add.
            if (TruncateOldTimes) {
                int i = 0;
                while (i < store.Rows.Count) {
                    if (store.Rows[i].Time < TruncateTimesBefore)
                        store.Rows.RemoveAt(i);
                    else
                        i++;
                }
            }

            // Only add if we have any data left
            if (store.Rows.Count > 0) {
                // Check for new tags before appending new rows
                if (store.TagOrder.Except(TagOrder).Count() > 0)
                    Insert(store);
                else
                    Append_InPlace(store);
            }

            if (file != null)
                AddProcessedFile(file);
        }
        public void Defrag() {
            // Reorganize the rows in order of time, compacting the file and dropping old
            // rows in the process

            // Check for mode allows this modification
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            // Operation Prevented if Store is Locked against writing
            if (IsStoreLocked())
                throw new StoreLockedException();

            // Remember the largest shard we have before defrag. We'll have to remove any shards afterwards, 
            // since we may lose data here
            uint beforeDefragLargestShard = Table.Last().Value.Item1;

            // Calculate the new positons to move the rows to
            var newTable = new Dictionary<ulong, Tuple<uint, long>>((int)RowCount);
            // Sort the times, since we will be reordering the rows anyway
            var times = Table.Keys.ToList();
            times.Sort();
            // reset to shard 0 and offset 0
            long pos = 0;
            ulong rowCnt = 0;
            uint shard = 0;
            ulong newestTime = ulong.MinValue, oldestTime = ulong.MaxValue;
            foreach (var time in times) {
                // Throw the old times away to constrain the db size
                if (!TruncateOldTimes || time >= TruncateTimesBefore) {
                    newTable.Add(time, new Tuple<uint, long>(shard, pos));
                    pos += RowBytes;

                    // Increment rows and check if we need to move to next shard
                    if (++rowCnt > MaxRowsPerShard) {
                        shard++;
                        pos = 0;
                        rowCnt = 0;
                    }

                    // Time bounds
                    if (time > newestTime)
                        newestTime = time;
                    if (time < oldestTime)
                        oldestTime = time;
                }

                if (pos > long.MaxValue - 10)
                    throw new Exception("File Too Large for defrag");
            }

            // Start the moving
            List<StoreRow> rows = new List<StoreRow>(Block);
            uint currentReadShard = 0;
            uint currentWriteShard = 0;
            int rowsRead = 0;
            long rowsMoved = 0;
            times = newTable.Keys.ToList(); // Already sorted above

            // Open the stream
            string TempPath = System.IO.Path.ChangeExtension(GetShardPath(currentWriteShard), string.Format("~{0}", currentWriteShard)); // Start with shard 0
            FileStream sStream = new FileStream(GetShardPath(currentReadShard), FileMode.Open, FileAccess.Read, FileShare.Read, ReadBlock);
            FileStream nStream = new FileStream(TempPath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite, WriteBlock);
            BinaryReader reader = new BinaryReader(sStream);
            BinaryWriter writer = new BinaryWriter(nStream);

            // -------------------
            // Transfer the rows
            while (rowsMoved < newTable.Count) {
                // Read a block of rows
                while (rowsRead < times.Count && rows.Count < Block) {
                    var time = times[rowsRead];
                    var value = Table[time];

                    // Read in this row
                    if (currentReadShard != value.Item1) {
                        // Open the new shard
                        currentReadShard = value.Item1;
                        reader.Dispose();
                        sStream.Dispose();
                        sStream = new FileStream(GetShardPath(currentReadShard), FileMode.Open, FileAccess.Read, FileShare.Read, ReadBlock);
                        reader = new BinaryReader(sStream);
                    }

                    sStream.Position = value.Item2;
                    var row = Util_ReadRow(reader, (uint)TagOrder.Count);
                    Debug.Assert(row.Time == time, "Read Row || New Table Error!");
                    rows.Add(row);
                    rowsRead++;
                }

                // Write them
                foreach (var row in rows) {
                    var value = newTable[row.Time];
                    if (currentWriteShard != value.Item1) {
                        currentWriteShard = value.Item1;
                        nStream.Flush(true);
                        nStream.Dispose();
                        writer.Dispose();
                        TempPath = System.IO.Path.ChangeExtension(GetShardPath(currentWriteShard), string.Format("~{0}", currentWriteShard));
                        nStream = new FileStream(TempPath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite, WriteBlock);
                        writer = new BinaryWriter(nStream);
                        nStream.Position = 0;
                    }

                    Debug.Assert(value.Item2 == nStream.Position, "Stream / Table Off Sync");

                    // Write the row                   
                    Util_WriteRow(writer, row);
                    rowsMoved++;
                }
                rows.Clear();
            }

            // -------------------
            // Remember the new table so we don't have to refresh from the HD
            Table = newTable;
            NewestTime = NewestTime;
            OldestTime = oldestTime;
            RowCount = (ulong)times.Count;

            // Cleanup
            reader.Dispose();
            writer.Dispose();
            sStream.Dispose();
            nStream.Dispose();

            LockStore();
            for (uint i = 0; i <= beforeDefragLargestShard; i++) {
                if (i > currentWriteShard) {
                    try { File.Delete(GetShardPath(i)); } catch (Exception) { }
                }
                else {
                    TempPath = System.IO.Path.ChangeExtension(GetShardPath(i), string.Format("~{0}", i));
                    using (var rStream = new FileStream(TempPath, FileMode.Open))
                    using (var wStream = new FileStream(GetShardPath(i), FileMode.Create)) {
                        rStream.CopyTo(wStream);
                    }
                    try { File.Delete(TempPath); } catch (Exception) { }
                }
            }
            UpdateStore();
        }
        public void Append_InPlace(Store store) {
            // Check for mode allows this modification
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            // Operation Prevented if Store is Locked against writing
            if (IsStoreLocked())
                throw new StoreLockedException();

            // find the positon to insert the new values (last row or calculated first row location, if Table empty)
            long lastRowEnd = Table.Count > 0 ? (Table.Last().Value.Item2 + RowBytes) : 0;
            uint currShard = Table.Count > 0 ? Table.Last().Value.Item1 : 0;
            uint lastRowShard = currShard;
            ulong rowCnt = (ulong)Table.Values.Where(x => x.Item1 == currShard).LongCount();

            FileStream stream = new FileStream(GetShardPath(currShard), FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read, WriteBlock);
            BinaryWriter writer = new BinaryWriter(stream);

            // Write the new rows onto the end of the current rows
            foreach (var row in store.Rows) {
                // Update Existing Row
                if (Table.ContainsKey(row.Time)) {
                    // Skip to the row (skip past time bytes)
                    uint shard = Table[row.Time].Item1;
                    if (shard != currShard) {
                        currShard = shard;
                        stream.Flush(true);
                        stream.Dispose();
                        writer.Dispose();
                        stream = new FileStream(GetShardPath(currShard), FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read, WriteBlock);
                        writer = new BinaryWriter(stream);
                    }
                    stream.Position = Table[row.Time].Item2 + TimeBytes;

                    // Write the tags we have (They may have been previously been inserted as 0's)
                    for (int i = 0; i < TagOrder.Count; i++) {
                        int tag = store.TagOrder.IndexOf(TagOrder[i]);

                        // If the store has this tag, then write it
                        if (tag >= 0)
                            Util_WriteValue(writer, row.Values[tag]);
                        else
                            stream.Position += ValueBytes; // Skip this double
                    }
                }
                // Add new Row to end of file
                else {
                    // Check if we need to move to the next shard
                    if (rowCnt > MaxRowsPerShard) {
                        lastRowShard++;
                        lastRowEnd = 0;
                        rowCnt = 0;
                    }

                    if (currShard != lastRowShard) {
                        currShard = lastRowShard;
                        stream.Flush(true);
                        stream.Dispose();
                        writer.Dispose();
                        stream = new FileStream(GetShardPath(currShard), FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read, WriteBlock);
                        writer = new BinaryWriter(stream);
                    }

                    // Update the in-memory Table                            
                    Table.Add(row.Time, new Tuple<uint, long>(currShard, lastRowEnd));

                    // Keep track of the newest rows we're adding
                    if (row.Time > NewestTime)
                        NewestTime = row.Time;
                    if (row.Time < OldestTime)
                        OldestTime = row.Time;

                    // Write the new rows to the end of the file
                    stream.Position = lastRowEnd;
                    Util_WriteTime(writer, row.Time);
                    for (int i = 0; i < TagOrder.Count; i++) {
                        int tag = store.TagOrder.IndexOf(TagOrder[i]);

                        Util_WriteValue(writer, (tag < 0 ? NullValue : row.Values[tag]));
                    }

                    // move past last row
                    rowCnt++;
                    lastRowEnd += RowBytes;
                    Debug.Assert(stream.Position == lastRowEnd);
                }
            }

            // Update Row count
            RowCount = (uint)Table.Count;

            stream.Flush(true);
            writer.Dispose();
            stream.Dispose();

            UpdateStore();
        }
        public void Insert(Store store) {
            // Check for mode allows this modification
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            // Operation Prevented if Store is Locked against writing
            if (IsStoreLocked())
                throw new StoreLockedException();

            // Calculate a new table
            List<String> newTagOrder = new List<string>(TagOrder);
            foreach (var tag in store.TagOrder) {
                if (!TagOrder.Contains(tag))
                    newTagOrder.Add(tag);
            }
            long newRowSize = (newTagOrder.Count * ValueBytes) + TimeBytes;

            // Calculate the new positons to move the rows to, and insert the new rows in order
            var times = Table.Keys.ToList();
            foreach (var row in store.Rows) // Add new row times
            {
                if (!times.Contains(row.Time))
                    times.Add(row.Time);
            }
            times.Sort();   // Sort the times, since we will be reordering the rows anyway
            var newTable = new Dictionary<ulong, Tuple<uint, long>>((int)RowCount);
            long pos = 0;
            uint shard = 0;
            ulong rowCnt = 0;
            ulong oldestTime = ulong.MaxValue, newestTime = ulong.MinValue;
            foreach (var time in times) {
                if (rowCnt > MaxRowsPerShard) {
                    shard++;
                    pos = 0;
                    rowCnt = 0;
                }
                newTable.Add(time, new Tuple<uint, long>(shard, pos));
                pos += newRowSize;
                rowCnt++;

                // Time bounds
                if (time > newestTime)
                    newestTime = time;
                if (time < oldestTime)
                    oldestTime = time;
            }

            // Start the moving
            List<StoreRow> rows = new List<StoreRow>(Block);
            int rowsRead = 0;
            long rowsMoved = 0;
            var moveTimes = newTable.Keys.ToList();    // Should already be sorted

            // Get tags that exist in both TagOrders are being updated by new data
            var changingTags = store.TagOrder.Intersect(TagOrder);
            List<bool> changingTagIndexes = new List<bool>(newTagOrder.Count);
            foreach (var t in newTagOrder) {
                changingTagIndexes.Add(
                    store.TagOrder.Contains(t)
                );
            }
            // Only check for updates on times between these
            ulong storeStartTime = store.Rows.Min(x => x.Time);
            ulong storeEndTime = store.Rows.Max(x => x.Time);

            // Open The File
            uint currReadShard = 0;
            uint currWriteShard = 0;
            shard = 0;
            string TempPath = System.IO.Path.ChangeExtension(GetShardPath(currWriteShard), string.Format("~{0}", currWriteShard)); // Start with shard 0
            FileStream sStream = null;
            FileStream nStream = null;
            BinaryReader reader = null;
            BinaryWriter writer = null;

            // -------------------
            // Transfer Rows
            while (rowsMoved < moveTimes.Count) {
                // Read a block of rows
                while (rowsRead < moveTimes.Count && rows.Count < Block) {
                    var time = moveTimes[rowsRead];
                    var row = new StoreRow(time);

                    // Data exists in datastore
                    if (Table.ContainsKey(time)) {
                        shard = Table[time].Item1;
                        long readAddress = Table[time].Item2;

                        if (currReadShard != shard || sStream == null || reader == null) {
                            currReadShard = shard;
                            reader?.Dispose();
                            sStream?.Dispose();
                            sStream = new FileStream(GetShardPath(currReadShard), FileMode.Open, FileAccess.Read, FileShare.ReadWrite, ReadBlock);
                            reader = new BinaryReader(sStream);
                        }
                        sStream.Position = readAddress + TimeBytes;

                        // If this time isn't changing, then just copy the row
                        if (time < storeStartTime || time > storeEndTime) {
                            for (int i = 0; i < newTagOrder.Count; i++) {
                                if (i < TagOrder.Count)
                                    row.Values.Add(Util_ReadValue(reader));
                                else
                                    row.Values.Add(NullValue);
                            }
                        }
                        else {
                            // This row might be chaning, so we have to check
                            for (int i = 0; i < newTagOrder.Count; i++) {
                                var storeRow = store.Rows.Find(y => y.Time == time);
                                if (changingTagIndexes[i] && storeRow != null) {
                                    // If this tag is updated | new, read the value from the new data
                                    int x = store.TagOrder.IndexOf(newTagOrder[i]);
                                    if (x >= 0)
                                        row.Values.Add(storeRow.Values[x]);
                                    else
                                        row.Values.Add(NullValue);
                                    sStream.Position += ValueBytes;   // Skip this tag's column
                                }
                                else if (i < TagOrder.Count) {
                                    // If it's not changing, then read the value from the DataStore
                                    row.Values.Add(Util_ReadValue(reader));
                                }
                                else {
                                    // If it doesn't exist in either, then this is a new value so we should pad with 0
                                    sStream.Position += ValueBytes;     // Skip this tag's column
                                    row.Values.Add(NullValue);          // Add a default value
                                }

                            }
                        }
                    }
                    // Data is from row to be added, doesn't exist in datastore yet
                    else {
                        var toAdd = store.Rows.Find(x => x.Time == time);
                        if (toAdd != null) {
                            // Add each value in the order of the new TagOrder
                            foreach (var tag in newTagOrder) {
                                // If this tag is in new store, take it's value, otherwise pad with 0
                                int i = store.TagOrder.IndexOf(tag);
                                if (i >= 0)
                                    row.Values.Add(toAdd.Values[i]);
                                else
                                    row.Values.Add(NullValue);
                            }
                        }
                    }
                    rows.Add(row);
                    rowsRead++;
                }

                // Write them
                foreach (var row in rows) {
                    var value = newTable[row.Time];
                    if (currWriteShard != value.Item1 || nStream == null || writer == null) {
                        currWriteShard = value.Item1;
                        writer?.Dispose();
                        nStream?.Dispose();
                        TempPath = System.IO.Path.ChangeExtension(GetShardPath(currWriteShard), string.Format("~{0}", currWriteShard));
                        nStream = new FileStream(TempPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read, WriteBlock);
                        writer = new BinaryWriter(nStream);
                        nStream.Position = 0;
                    }

                    Debug.Assert(row.Values.Count == newTagOrder.Count, "Row doesn't have correct value count when writing");
                    Debug.Assert(value.Item2 == nStream.Position, "Stream / Table Off Sync");

                    // Write the row
                    Util_WriteRow(writer, row);
                    rowsMoved++;
                }
                rows.Clear();
            }

            // Writing Done
            reader?.Dispose();
            sStream?.Dispose();
            writer?.Dispose();
            nStream?.Dispose();

            // All rows Moved, transfer file over
            Table = newTable;
            TagOrder = newTagOrder;
            RowCount = (ulong)newTable.Count;
            TagCount = (uint)TagOrder.Count;
            NewestTime = NewestTime;
            OldestTime = oldestTime;

            LockStore();
            for (uint i = 0; i <= currWriteShard; i++) {
                TempPath = System.IO.Path.ChangeExtension(GetShardPath(i), string.Format("~{0}", i));
                using (var rStream = new FileStream(TempPath, FileMode.Open))
                using (var wStream = new FileStream(GetShardPath(i), FileMode.Create)) {
                    rStream.CopyTo(wStream);
                }
                try { File.Delete(TempPath); } catch (Exception) { }
            }
            UpdateStore();
        }

        // Sep the Reading / Writing
        private string GetShardPath(uint shard) {
            return DataStore.GetShardPath(Path, shard);
        }
        private static string GetShardPath(string store_path, uint shard) {
            return System.IO.Path.ChangeExtension(store_path, string.Format("shard{0}", shard));
        }
        private StoreRow Util_ReadRow(BinaryReader r, uint tagCount) {
            // Read Time
            StoreRow row = new StoreRow(Util_ReadTime(r));

            for (int i = 0; i < tagCount; i++)
                row.Values.Add(Util_ReadValue(r));

            return row;
        }
        private ulong Util_ReadTime(BinaryReader r) {
            if (TimeBytes == 4)
                return r.ReadUInt32();
            else if (TimeBytes == 8)
                return r.ReadUInt64();
            else
                throw new Exception("Unsupported Time Byte Length!");
        }
        private double Util_ReadValue(BinaryReader r) {
            double v = NullValue;
            if (ValueBytes == 4)
                v = r.ReadSingle();
            else if (ValueBytes == 8)
                v = r.ReadDouble();
            else
                throw new Exception("Unsupported Value Length");

            if (v == NullValue)
                return double.MaxValue; // we don't want to return float.MaxValue here, so we translate this into 8 bytes
            return v;
        }
        private void Util_WriteRow(BinaryWriter w, StoreRow row) {
            // Write Time
            Util_WriteTime(w, row.Time);

            // write each Value
            foreach (var val in row.Values)
                Util_WriteValue(w, val);
        }
        private void Util_WriteTime(BinaryWriter w, ulong time) {
            if (TimeBytes == 4)
                w.Write((UInt32)time);
            else if (TimeBytes == 8)
                w.Write((UInt64)time);
            else
                throw new Exception("Unsupported Time Byte Length!");
        }
        private void Util_WriteValue(BinaryWriter w, double val) {
            if (ValueBytes == 4) {
                if (val == double.MaxValue || val == float.MaxValue) // If we read a max value, its converted from 4bytemax to 8 byte max, so convert back before writing
                    w.Write(float.MaxValue);
                else
                    w.Write(Math.Min((float)val, float.MaxValue - 1));
            }
            else if (ValueBytes == 8) {
                w.Write((double)val);
            }
            else
                throw new Exception("Unsupported Value Byte Length!");
        }
        private void Util_WriteHeader(BinaryWriter w, bool locked = false) {
            Util_WriteHeader(w, Version, locked, OldestTime, NewestTime, TagCount, RowCount);
        }
        private void Util_WriteHeader(BinaryWriter w, UInt32 version, bool locked, ulong oldestTime, ulong newestTime, UInt32 tagCount, UInt64 rowCount) {
            // 4[Version] 1[Locked] 1[TimeLength] 1[ValueLength] 8[OldestTime] 8[NewestTime] 4[TagCount] 8[RowCount]
            w.BaseStream.Position = 0;
            w.Write(version);
            w.Write(locked);
            w.Write((byte)_timeBytes);
            w.Write((byte)_valueBytes);
            w.Write((UInt64)oldestTime);
            w.Write((UInt64)newestTime);
            w.Write(tagCount);
            w.Write(rowCount);
            Debug.Assert(w.BaseStream.Position == HeaderBytes, "Header Size Incorrect!");
        }
        private void Util_WriteTags(BinaryWriter w, List<string> tags) {
            foreach (var tag in tags)
                w.Write(tag);
        }

        // ----------------------------------------------------------------------------------------
        // Locking
        private void LockStore() {
            FileStream stream = new FileStream(Path, FileMode.Open, FileAccess.Write);
            LockStore(stream);
            stream.Flush();
            stream.Dispose();
        }
        private void LockStore(FileStream stream) {
            long origPos = stream.Position;
            stream.Position = 4;
            stream.WriteByte(1);
            stream.Flush(true);
            stream.Position = origPos;
        }
        private void UnlockStore() {
            FileStream stream = new FileStream(Path, FileMode.Open, FileAccess.Write);
            UnlockStore(stream);
            stream.Flush();
            stream.Dispose();
        }
        private void UnlockStore(FileStream stream) {
            long origPos = stream.Position;
            stream.Position = 4;
            stream.WriteByte(0);
            stream.Flush(true);
            stream.Position = origPos;
        }
        private bool IsStoreLocked() {
            using (var stream = new FileStream(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
                stream.Position = 4;
                return (stream.ReadByte() == 1);
            }
        }

        // ----------------------------------------------------------------------------------------
        // Querying       
        public static TagData QueryTag(string path, string tag, ulong start, ulong end) {
            // Create the new store so it can be loaded
            DataStore store = new DataStore(path, StoreAccess.Query);

            // Operation Prevented if Store is Locked against reading
            if (store.Locked)
                return new TagData() { Name = tag };

            // Get column offset
            int col = store.TagOrder.IndexOf(tag);
            if (col < 0)    // Not found
                return new TagData() { Name = tag };

            // Gather the locations to read
            var offsets = new List<Tuple<uint, long>>();
            var times = store.Table.Keys.Where(x => x >= start && x <= end).ToList();
            foreach (var time in times) {
                offsets.Add(store.Table[time]);
            }

            // Read the locations into a tagadata 
            TagData data = new TagData {
                Name = tag
            };

            // Split Offsets list up into a List of different offsets
            List<List<Tuple<uint, long>>> split = null;
            //if (offsets.Last().Item1 > 3)
            //    split = offsets.GroupBy(x => x.Item1).Select(x => x.ToList()).ToList();
            //else if (offsets.Count > 100)
            //    split = SplitList(offsets, (int)(offsets.Count / 10.0d)).ToList();
            //else
            split = new List<List<Tuple<uint, long>>> { offsets };

            foreach (var list in split) {
                var t = GatherQueryFromOffsetList(store, offsets, col);
                data.Values.AddRange(t.Values);
            }

            // Sort by time before we return
            data.Values.Sort((x, y) => { return x.Time.CompareTo(y.Time); });

            return data;
        }
        public static TagData QueryTagThreaded(string path, string tag, ulong start, ulong end) {
            // Create the new store so it can be loaded
            DataStore store = new DataStore(path, StoreAccess.Query);

            // Operation Prevented if Store is Locked against reading
            if (store.Locked)
                return new TagData() { Name = tag };

            // Get column offset
            int col = store.TagOrder.IndexOf(tag);
            if (col < 0)    // Not found
                return new TagData() { Name = tag };

            // Gather the locations to read
            var offsets = new List<Tuple<uint, long>>();
            var times = store.Table.Keys.Where(x => x >= start && x <= end).ToList();
            foreach (var time in times) {
                offsets.Add(store.Table[time]);
            }

            // Read the locations into a tagadata 
            TagData data = new TagData {
                Name = tag
            };

            // Split Offsets list up into a List of different offsets
            List<List<Tuple<uint, long>>> split = null;
            if (offsets.Last().Item1 > 3)
                split = offsets.GroupBy(x => x.Item1).Select(x => x.ToList()).ToList();
            else
                split = SplitList(offsets, (int)(offsets.Count / 10.0d)).ToList();

            // create the tasks
            List<Task<TagData>> tasks = new List<Task<TagData>>();
            foreach (var list in split) {
                tasks.Add(new Task<TagData>(() => {
                    return GatherQueryFromOffsetList(store, list, col);
                }));
            }
            // Multithreaded wait
            Task.WaitAll(tasks.ToArray());

            // Collect the results
            foreach (var tsk in tasks) {
                if (tsk.IsCompleted)
                    data.Values.AddRange(tsk.Result.Values);
            }

            // Sort by time before we return
            data.Values.Sort((x, y) => { return x.Time.CompareTo(y.Time); });

            return data;
        }
        private static TagData GatherQueryFromOffsetList(DataStore store, List<Tuple<uint, long>> offsets, int col) {
            uint currentShard = offsets.First().Item1;
            var stream = new FileStream(store.GetShardPath(currentShard), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            var reader = new BinaryReader(stream);

            TagData data = new TagData();

            foreach (var pair in offsets) {
                uint shard = pair.Item1;
                long offset = pair.Item2;

                // Switch Shards if needed
                if (currentShard != shard) {
                    currentShard = shard;
                    stream.Dispose();
                    reader.Dispose();
                    stream = new FileStream(store.GetShardPath(currentShard), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    reader = new BinaryReader(stream);
                }

                // Seek to start position
                stream.Position = offset;

                // read row's time
                ulong time = store.Util_ReadTime(reader);

                // Seek to column
                stream.Position += store.ValueBytes * col;

                // Read the value
                double val = store.Util_ReadValue(reader);

                // Add them to the tag
                data.Values.Add(new TagValue(time, val));
            }

            reader.Dispose();
            stream.Dispose();

            return data;
        }
        /// <summary>
        /// Returns all rows in a time frame. Useful for Cacheing a timeframe locally
        /// </summary>
        /// <param name="path">Path to datastore (*.store)</param>
        /// <param name="start">Start time in Oracle Time (Inclusive)</param>
        /// <param name="end">End Time in Oracle Time (Inclusive)</param>
        /// <returns>Store of values directly in the DataStore's format</returns>
        public static Store QueryRows(string path, ulong start, ulong end, bool threaded = true) {
            // Create the new store so it can be loaded
            DataStore store = new DataStore(path, StoreAccess.Query);

            // Operation Prevented if Store is Locked against reading
            if (store.Locked)
                return new Store();

            // Gather the row offsets to read
            var offsets = new List<Tuple<uint, long>>();
            var times = store.Table.Keys.Where(x => x >= start && x <= end).ToList();
            foreach (var time in times) {
                offsets.Add(store.Table[time]);
            }

            // This is the return store
            Store rows = new Store {
                TagOrder = store.TagOrder,
                Rows = new List<StoreRow>(offsets.Count)
            };

            // Split Offsets list up into a List of different shards
            List<List<Tuple<uint, long>>> split = offsets.GroupBy(x => x.Item1).Select(x => x.ToList()).ToList();

            if (threaded) {
                // create the tasks
                var tasks = new List<Task<List<StoreRow>>>();
                foreach (var list in split) {
                    tasks.Add(Task<List<StoreRow>>.Factory.StartNew(() => {
                        return GatherRows(store, list);
                    }));
                }
                // Multithreaded wait
                Task.WaitAll(tasks.ToArray());

                // Collect the results
                foreach (var tsk in tasks) {
                    if (tsk.IsCompleted)
                        rows.Rows.AddRange(tsk.Result);
                }
            }
            else {
                // Grab all the rows in the lists
                foreach (var list in split) {
                    var t = GatherRows(store, list);
                    rows.Rows.AddRange(t);
                }
            }

            // Sort by time before we return
            rows.Rows.Sort((x, y) => { return x.Time.CompareTo(y.Time); });

            return rows;
        }
        private static List<StoreRow> GatherRows(DataStore store, List<Tuple<uint, long>> offsets) {
            uint currentShard = offsets.First().Item1;
            var stream = new FileStream(store.GetShardPath(currentShard), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            var reader = new BinaryReader(stream);

            List<StoreRow> rows = new List<StoreRow>(store.TagOrder.Count);

            foreach (var pair in offsets) {
                uint shard = pair.Item1;
                long offset = pair.Item2;

                // Switch Shards if needed
                if (currentShard != shard) {
                    currentShard = shard;
                    stream.Dispose();
                    reader.Dispose();
                    stream = new FileStream(store.GetShardPath(currentShard), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    reader = new BinaryReader(stream);
                }

                // Seek to start position
                stream.Position = offset;

                // read row's time
                StoreRow row = store.Util_ReadRow(reader, (uint)store.TagOrder.Count);
                rows.Add(row);
            }

            reader.Dispose();
            stream.Dispose();

            return rows;
        }
        private static IEnumerable<List<T>> SplitList<T>(List<T> list, int nSize = 30) {
            for (int i = 0; i < list.Count; i += nSize) {
                yield return list.GetRange(i, Math.Min(nSize, list.Count - i));
            }
        }
        public static Tuple<ulong, ulong> QueryLatestTimeRange(string path) {
            // Create the new store so it can be loaded
            // Safe Way
            //DataStore store = new DataStore(path, StoreAccess.Query);
            //return new Tuple<ulong, ulong>(store.OldestTime, store.NewestTime);

            if (!File.Exists(path))
                return new Tuple<ulong, ulong>(0, 0);

            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, ReadBlock)) {
                if (stream.Length >= HeaderBytes) {
                    using (var reader = new BinaryReader(stream)) {
                        // read based on position
                        // Read Header -- 4[Version] 1[Locked] 1[TimeLength] 1[ValueLength] 8[OldestTime] 8[NewestTime] 4[TagCount] 8[RowCount]
                        stream.Position = 4 + 1 + 1 + 1;
                        ulong oldest = reader.ReadUInt64();
                        ulong newest = reader.ReadUInt64();
                        return new Tuple<ulong, ulong>(oldest, newest);
                    }
                }
                else
                    return new Tuple<ulong, ulong>(0, 0);
            }
        }
        public bool IsOrdered() {
            for (int i = 0; i < Table.Count - 1; i++) {
                if (Table.Keys.ElementAt(i) > Table.Keys.ElementAt(i + 1))
                    return false;
            }
            return true;
        }

        struct OffsetTable
        {
            public ulong Time;
            public uint Shard;
            public long Offset;
        }
        public static Store QueryTags(ulong start, ulong end, List<String> tags, string store_path) {
            var store = new Store() { TagOrder = tags, Rows = new List<StoreRow>() };
            if (!File.Exists(store_path)) return store;

            // Open the store file
            var stream = new FileStream(store_path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, ReadBlock);
            if (stream.Length < HeaderBytes) return store;
            var reader = new BinaryReader(stream);

            // -- Read the Header
            var version = reader.ReadUInt32();
            var locked = reader.ReadBoolean();
            var timeBytes = reader.ReadByte();
            var valueBytes = reader.ReadByte();
            var oldestTime = reader.ReadUInt64();
            var newestTime = reader.ReadUInt64();
            var tagCount = reader.ReadUInt32();
            var rowCount = reader.ReadUInt64();

            // -- and the tags
            var TagOrder = new List<string>((int)tagCount);
            for (int i = 0; i < tagCount; i++)
                TagOrder.Add(reader.ReadString());

            // -- and the offsets that matter to this query
            var indexes = new List<OffsetTable>();
            for (ulong i = 0; i < rowCount; i++) {
                var time = reader.ReadUInt64();
                var shard = reader.ReadUInt32();
                var offset = reader.ReadInt64();

                if (start < time && time < end) {
                    indexes.Add(new OffsetTable {
                        Time = time,
                        Shard = shard,
                        Offset = offset
                    });
                }
            }

            // -- Translate Tags into Offsets
            var tag_offsets = new List<uint>((int)tagCount);
            for (uint i = 0; i < tagCount; i++) {
                if (tags.Contains(TagOrder[(int)i]))
                    tag_offsets.Add(i);
            }

            // -- Close the store file
            reader.Dispose();
            stream.Dispose();

            // -- Perform the query if we have times to query
            if (indexes.Count > 0 && tag_offsets.Count > 0) {
                uint currShard = 0;
                FileStream shard_stream = null;
                BinaryReader shard_rdr = null;

                foreach (var row in indexes) {
                    // Check if we've moved to a different shard
                    if (currShard != row.Shard || shard_stream == null || shard_rdr == null) {
                        currShard = row.Shard;
                        shard_rdr?.Dispose();
                        shard_stream?.Dispose();
                        shard_stream = new FileStream(GetShardPath(store_path, currShard), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                        shard_rdr = new BinaryReader(shard_stream);
                    }

                    // Seek to the row start offset
                    shard_stream.Position = row.Offset;

                    // read the time
                    ulong time = 0;
                    if (timeBytes == 4) {
                        time = shard_rdr.ReadUInt32();
                    }
                    else if (timeBytes == 8) {
                        time = shard_rdr.ReadUInt64();
                    }
                    else {
                        throw new Exception("Unknown / Unsupported time bytes read from header");
                    }
                    Debug.Assert(row.Time == time);

                    // Read each value
                    var store_row = new StoreRow(time);
                    foreach (var tag_offset in tag_offsets) {
                        shard_stream.Seek(row.Offset + timeBytes + (tag_offset * valueBytes), SeekOrigin.Begin);

                        double val = 0;
                        if (valueBytes == 4) {
                            val = (double)shard_rdr.ReadSingle();
                        }
                        else if (valueBytes == 8) {
                            val = shard_rdr.ReadDouble();
                        }
                        else {
                            throw new Exception("Unknown / Unsupported value bytes read from header");
                        }
                        store_row.Values.Add(val);
                    }

                    // Add the row
                    store.Rows.Add(store_row);
                }
            }

            return store;
        }
        public Store QueryTags(ulong start, ulong end, List<String> tags) {
            var store = new Store() { TagOrder = tags, Rows = new List<StoreRow>() };

            // -- and the offsets that matter to this query
            var indexes = new List<OffsetTable>();
            var times = Table.Keys.Where(x => x >= start && x <= end).ToList();
            foreach (var time in times) {
                indexes.Add(new OffsetTable() {
                    Time = time,
                    Shard = Table[time].Item1,
                    Offset = Table[time].Item2
                });
            }

            // -- Translate Tags into Offsets
            var tag_offsets = new List<uint>((int)TagCount);
            for (uint i = 0; i < TagCount; i++) {
                if (tags.Contains(TagOrder[(int)i]))
                    tag_offsets.Add(i);
            }

            // -- Perform the query if we have times to query
            if (indexes.Count > 0 && tag_offsets.Count > 0) {
                uint currShard = 0;
                FileStream shard_stream = null;
                BinaryReader shard_rdr = null;

                foreach (var row in indexes) {
                    // Check if we've moved to a different shard
                    if (currShard != row.Shard || shard_stream == null || shard_rdr == null) {
                        currShard = row.Shard;
                        shard_rdr?.Dispose();
                        shard_stream?.Dispose();
                        shard_stream = new FileStream(GetShardPath(currShard), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                        shard_rdr = new BinaryReader(shard_stream);
                    }

                    // Seek to the row start offset
                    shard_stream.Position = row.Offset;

                    // read the time
                    ulong time = Util_ReadTime(shard_rdr);
                    Debug.Assert(row.Time == time);

                    // Read each value
                    var store_row = new StoreRow(time);
                    foreach (var tag_offset in tag_offsets) {
                        shard_stream.Seek(row.Offset + TimeBytes + (tag_offset * ValueBytes), SeekOrigin.Begin);

                        store_row.Values.Add(Util_ReadValue(shard_rdr));
                    }

                    // Add the row
                    store.Rows.Add(store_row);
                }
            }

            return store;
        }
        public void QueryTags_FileProgress(ulong start, ulong end, List<String> tags, string outFile) {
            // Open the file (So we locked the file before we start query)
            using (var file = new FileStream(outFile, FileMode.Create, FileAccess.Write, FileShare.Read, 4096))
            using (var fStream = new StreamWriter(file)) {
                // -- Find the indexes for this query
                var indexes = new List<OffsetTable>();
                var times = Table.Keys.Where(x => x >= start && x <= end).ToList();
                foreach (var time in times) {
                    indexes.Add(new OffsetTable() {
                        Time = time,
                        Shard = Table[time].Item1,
                        Offset = Table[time].Item2
                    });
                }

                // -- Write the header (# of rows)
                fStream.WriteLine(times.Count);
                if (times.Count == 0) return;
                fStream.WriteLine("SECTIME,{0},", string.Join(",", tags));

                // -- Translate Tags into Offsets
                var tag_offsets = new List<uint>((int)TagCount);    // Todo -- What to do about tags that aren't found?
                for (uint i = 0; i < TagCount; i++) {
                    if (tags.Contains(TagOrder[(int)i]))
                        tag_offsets.Add(i);
                }

                // -- Perform the query if we have times to query
                if (indexes.Count > 0 && tag_offsets.Count > 0) {
                    uint currShard = 0;
                    FileStream shard_stream = null;
                    BinaryReader shard_rdr = null;

                    foreach (var row in indexes) {
                        // Check if we've moved to a different shard
                        if (currShard != row.Shard || shard_stream == null || shard_rdr == null) {
                            currShard = row.Shard;
                            shard_rdr?.Dispose();
                            shard_stream?.Dispose();
                            shard_stream = new FileStream(GetShardPath(currShard), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                            shard_rdr = new BinaryReader(shard_stream);
                        }

                        // Seek to the row start offset
                        shard_stream.Position = row.Offset;

                        // read the time
                        ulong time = Util_ReadTime(shard_rdr);
                        Debug.Assert(row.Time == time);
                        fStream.Write("{0},", OracleTime.ToDateTime(time).ToString("yyyy-MM-dd hh:mm:ss"));

                        // Read each value
                        foreach (var tag_offset in tag_offsets) {
                            shard_stream.Seek(row.Offset + TimeBytes + (tag_offset * ValueBytes), SeekOrigin.Begin);
                            var val = Util_ReadValue(shard_rdr);
                            if (val == NullValue) {
                                fStream.Write("Null, ");
                            }
                            else {
                                fStream.Write("{0:0.00},", val);
                            }
                        }

                        // Finish the line
                        fStream.WriteLine();
                    }
                }
            }
        }
        // ----------------------------------------------------------------------------------------
        // Misc
        private int TagOrderBytes(List<string> to) {
            int size = 0;
            foreach (var s in to) {
                size += 1 + s.Length;
            }
            return size;
        }
        private string CleanTag(string tag) {            
            return tag.Substring(0, 26);
        }

        // ----------------------------------------------------------------------------------------
        // Debug
        public void ExportToFile(string path, bool exportRows = false) {
            uint currShard = 0;
            FileStream storeStream = null;
            BinaryReader reader = null;
            var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.Read, WriteBlock);
            var writer = new StreamWriter(fileStream);

            // Dump store layout
            writer.WriteLine("-- Header");
            writer.WriteLine("Version: {0} - TimeBytes: {1}b, ValueBytes: {2}b", Version, TimeBytes, ValueBytes);
            writer.WriteLine("Locked: {0}", IsStoreLocked());
            writer.WriteLine("TagCount: {0}", TagCount);
            writer.WriteLine("RowCount: {0}", RowCount);
            fileStream.Flush();
            writer.WriteLine("--");
            writer.WriteLine("-- Calculated ");
            if (exportRows) {
                writer.WriteLine("Ordered: {0}", (IsOrdered() ? "yes" : "no"));
            }
            writer.WriteLine("Row Bytes: {0}", RowBytes);
            writer.WriteLine("Block: {0}", Block);
            writer.WriteLine("Read Buffer: {0} bytes, Write Buffer: {1} bytes", ReadBlock, WriteBlock);
            fileStream.Flush();
            writer.WriteLine("--");
            writer.WriteLine("-- Tag Order ");
            writer.WriteLine("  [{0}]", string.Join(", ", TagOrder));
            fileStream.Flush();
            writer.WriteLine("--");
            writer.WriteLine("--Files");
            writer.WriteLine("Count: {0}", Files.Count);
            int col = 0;
            foreach (var f in Files) {
                if (col > 1) {
                    writer.WriteLine("{0},", f);
                    col = 0;
                }
                else {
                    writer.Write("{0}, ", f);
                    col++;
                }
                fileStream.Flush();
            }
            writer.WriteLine("--");
            writer.WriteLine("--Table");
            writer.WriteLine("\tMax Offset: {0}", long.MaxValue);
            fileStream.Flush();
            foreach (var pair in Table) {
                if (pair.Value.Item1 != currShard || storeStream == null || reader == null) {
                    currShard = pair.Value.Item1;
                    reader?.Dispose();
                    storeStream?.Dispose();
                    storeStream = new FileStream(GetShardPath(currShard), FileMode.Open, FileAccess.Read, FileShare.Read, ReadBlock);
                    reader = new BinaryReader(storeStream);
                }

                storeStream.Position = pair.Value.Item2;
                writer.WriteLine("  Time: {0} ({1})\t\tShard: {2}\tOffset: {3}\t\tOkay: {4}",
                    pair.Key,
                    OracleTime.ToDateTime(pair.Key).ToString("yyyyMMdd-HHmmss"),
                    pair.Value.Item1,
                    pair.Value.Item2,
                    Util_ReadTime(reader) == pair.Key);
                fileStream.Flush();
            }

            if (exportRows) {
                writer.WriteLine("--");
                writer.WriteLine("--Rows");
                foreach (var pair in Table.Values) {
                    if (pair.Item1 != currShard || storeStream == null || reader == null) {
                        currShard = pair.Item1;
                        reader?.Dispose();
                        storeStream?.Dispose();
                        storeStream = new FileStream(GetShardPath(currShard), FileMode.Open, FileAccess.Read, FileShare.Read, ReadBlock);
                        reader = new BinaryReader(storeStream);
                    }

                    storeStream.Position = pair.Item2;
                    writer.Write(string.Format("  [ {0}::{1:0000000000}: ", currShard, Util_ReadTime(reader)));
                    for (int i = 0; i < TagCount; i++) {
                        // Update: With change of NullValue from 0 to byteMax, dump the values specially
                        double val = Util_ReadValue(reader);
                        if (val == double.MaxValue) {
                            writer.Write(" (NULL) ");
                        }
                        else {
                            writer.Write(string.Format("{0:00000.0} ", val));
                        }
                    }
                    writer.WriteLine(" ]");
                    fileStream.Flush();
                }
            }

            writer?.Dispose();
            reader?.Dispose();
            fileStream?.Dispose();
            storeStream?.Dispose();
        }

        // ----------------------------------------------------------------------------------------
        // OLD
        //public void Append(Store store)
        //{
        //    // Check for mode allows this modification
        //    if (Mode == StoreAccess.Query)
        //        throw new Exception("Store Access called to modify when store mode is only Query!");

        //    // Operation Prevented if Store is Locked against writing
        //    if (IsStoreLocked())
        //        throw new StoreLockedException();

        //    // find the positon to insert the new values (last row or calculated first row location, if Table empty)
        //    long lastRowEnd = Table.Count > 0 ? (Table.Values.Max() + RowBytes) : (HeaderBytes + TagOrderBytes(TagOrder));

        //    // Copy the file so we can work on it
        //    string TempPath = System.IO.Path.ChangeExtension(Path, "~");
        //    File.Copy(Path, TempPath, true);
        //    FileStream stream = new FileStream(TempPath, FileMode.Open, FileAccess.Write, FileShare.ReadWrite, WriteBlock);
        //    BinaryWriter writer = new BinaryWriter(stream);

        //    // Write the new rows onto the end of the current rows
        //    foreach (var row in store.Rows)
        //    {
        //        // Update Existing Row
        //        if (Table.ContainsKey(row.Time))
        //        {
        //            // Skip to the row (skip past time bytes)
        //            stream.Position = Table[row.Time] + TimeBytes;

        //            // Write the tags we have (They may have been previously been inserted as 0's)
        //            for (int i = 0; i < TagOrder.Count; i++)
        //            {
        //                int tag = store.TagOrder.IndexOf(TagOrder[i]);

        //                // If the store has this tag, then write it
        //                if (tag >= 0)
        //                    Util_WriteValue(writer, row.Values[tag]);
        //                else
        //                    stream.Position += ValueBytes; // Skip this double
        //            }
        //        }
        //        // Add new Row to end of file
        //        else
        //        {
        //            // Update the in-memory Table                            
        //            Table.Add(row.Time, lastRowEnd);

        //            // Keep track of the newest rows we're adding
        //            if (row.Time > NewestTime)
        //                NewestTime = row.Time;
        //            if (row.Time < OldestTime)
        //                OldestTime = row.Time;

        //            // Write the new rows to the end of the file
        //            stream.Position = lastRowEnd;
        //            Util_WriteTime(writer, row.Time);
        //            for (int i = 0; i < TagOrder.Count; i++)
        //            {
        //                int tag = store.TagOrder.IndexOf(TagOrder[i]);

        //                Util_WriteValue(writer, (tag < 0 ? 0 : row.Values[tag]));
        //            }

        //            // move past last row
        //            lastRowEnd += RowBytes;
        //            Debug.Assert(stream.Position == lastRowEnd);
        //        }
        //    }

        //    // Update Row count
        //    RowCount = (uint)Table.Count;
        //    Util_WriteHeader(writer, true);

        //    // Cleanup
        //    writer.Dispose();
        //    stream.Dispose();

        //    // Begin copy
        //    LockStore();
        //    //File.Copy(TempPath, Path, true);
        //    //File.Delete(TempPath);
        //    File.Replace(TempPath, Path, null);
        //    WriteTable(Table);
        //    UnlockStore();
        //}
        //public void UpdateTags(List<string> tags)
        //{
        //    // Operation Prevented if Store is Locked against writing
        //    if (IsStoreLocked())
        //        throw new StoreLockedException();

        //    // Calculate a new table
        //    List<String> newTagOrder = new List<string>(TagOrder);
        //    newTagOrder.AddRange(tags);
        //    long newRowSize = (newTagOrder.Count * 8) + 8;

        //    // increasing the tags increases the row size, which pushes each row futher from each other
        //    // read up to 'Block' rows into memory, then transfer them to their new location,
        //    // starting at the end

        //    // Calculate the new positons to move the rows to
        //    Dictionary<ulong, long> newTable = new Dictionary<ulong, long>((int)RowCount);
        //    long pos = HeaderBytes + TagOrderBytes(newTagOrder);    // Calculate new initial position
        //    var times = Table.Keys.ToList();    // These should be in Row order
        //    for (int i = 0; i < RowCount; i++)
        //    {
        //        newTable.Add(times[i], pos);
        //        pos += newRowSize;
        //    }

        //    // Start the moving
        //    int newColumns = newTagOrder.Count - TagOrder.Count;
        //    int rowsRead = 0;
        //    long rowsMoved = 0;
        //    List<StoreRow> rows = new List<StoreRow>(Block);
        //    var moveTimes = Table.OrderBy(x => x.Value).Select(x => x.Key).ToList();    // List of times sorted by offset

        //    string TempPath = System.IO.Path.ChangeExtension(Path, "~");
        //    FileStream sStream = new FileStream(Path, FileMode.Open, FileAccess.Read);
        //    FileStream nStream = new FileStream(TempPath, FileMode.Create, FileAccess.Write);
        //    BinaryReader reader = new BinaryReader(sStream);
        //    BinaryWriter writer = new BinaryWriter(nStream);

        //    sStream.Position = 0;
        //    nStream.Position = 0;

        //    // -------------------
        //    // Write Header
        //    Util_WriteHeader(writer, Version, true, 0, (uint)newTagOrder.Count, (uint)times.Count);

        //    // -------------------
        //    // Write Tags              
        //    Util_WriteTags(writer, newTagOrder);

        //    // -------------------
        //    // Transfer Rows
        //    while (rowsMoved < RowCount)
        //    {
        //        // Read a block of rows
        //        while (rowsRead < moveTimes.Count && rows.Count < Block)
        //        {
        //            var time = moveTimes[rowsRead];
        //            long readAddress = Table[time];
        //            sStream.Position = readAddress + 8;

        //            StoreRow row = new StoreRow(time);
        //            for (int i = 0; i < TagCount; i++)
        //                row.Values.Add(reader.ReadDouble());
        //            rows.Add(row);
        //            rowsRead++;
        //        }

        //        // Write them
        //        foreach (var row in rows)
        //        {
        //            // Move to new row position
        //            nStream.Position = newTable[row.Time];
        //            // Write the row
        //            writer.Write(row.Time);
        //            foreach (var val in row.Values)
        //                writer.Write(val);
        //            // write the new values as 0's
        //            for (int i = 0; i < newColumns; i++)
        //                writer.Write((double)0);

        //            rowsMoved++;
        //        }
        //        rows.Clear();
        //    }

        //    // All rows Moved, now write the table and count
        //    Table = newTable;
        //    TagOrder = newTagOrder;
        //    TagCount = (uint)newTagOrder.Count;

        //    // Cleanup
        //    reader.Dispose();
        //    writer.Dispose();
        //    sStream.Dispose();
        //    nStream.Dispose();

        //    File.Replace(TempPath, Path, System.IO.Path.ChangeExtension(Path, "t"));
        //    WriteTable(Table);
        //    UnlockStore();
        //}
        //public void UpdateTags_InPlace(List<string> tags)
        //{
        //    // Operation Prevented if Store is Locked against writing
        //    if (IsStoreLocked())
        //        throw new StoreLockedException();

        //    // Calculate a new table
        //    List<String> newTagOrder = new List<string>(TagOrder);
        //    newTagOrder.AddRange(tags);
        //    long newRowSize = (newTagOrder.Count * 8) + 8;

        //    // increasing the tags increases the row size, which pushes each row futher from each other
        //    // read up to 'Block' rows into memory, then transfer them to their new location,
        //    // starting at the end

        //    // Calculate the new positons to move the rows to
        //    Dictionary<ulong, long> newTable = new Dictionary<ulong, long>((int)RowCount);
        //    long pos = HeaderBytes + TagOrderBytes(newTagOrder);    // Calculate new initial position
        //    var times = Table.Keys.ToList();    // These should be in Row order
        //    for (int i = 0; i < RowCount; i++)
        //    {
        //        newTable.Add(times[i], pos);
        //        pos += newRowSize;
        //    }

        //    // Start the moving
        //    int newColumns = newTagOrder.Count - TagOrder.Count;
        //    int rowsRead = 0;
        //    long rowsMoved = 0;
        //    List<StoreRow> rows = new List<StoreRow>(Block);
        //    var moveTimes = Table.OrderBy(x => x.Value).Select(x => x.Key).ToList();    // List of times sorted by offset
        //    moveTimes.Reverse();

        //    FileStream stream = new FileStream(Path, FileMode.Open, FileAccess.ReadWrite);
        //    BinaryReader reader = new BinaryReader(stream);
        //    BinaryWriter writer = new BinaryWriter(stream);

        //    LockStore(stream);

        //    while (rowsMoved < RowCount)
        //    {
        //        // Read a block of rows
        //        while (rowsRead < moveTimes.Count && rows.Count < Block)
        //        {
        //            var time = moveTimes[rowsRead];
        //            long readAddress = Table[time];
        //            stream.Position = readAddress + 8;

        //            StoreRow row = new StoreRow(time);
        //            for (int i = 0; i < TagCount; i++)
        //                row.Values.Add(reader.ReadDouble());
        //            rows.Add(row);
        //            rowsRead++;
        //        }

        //        // Write them
        //        foreach (var row in rows)
        //        {
        //            // Move to new row position
        //            stream.Position = newTable[row.Time];
        //            // Write the row
        //            writer.Write(row.Time);
        //            foreach (var val in row.Values)
        //                writer.Write(val);
        //            // write the new values as 0's
        //            for (int i = 0; i < newColumns; i++)
        //                writer.Write((double)0);

        //            rowsMoved++;
        //        }
        //        rows.Clear();
        //    }

        //    // All rows Moved, now write the table and count
        //    Table = newTable;
        //    TagOrder = newTagOrder;
        //    TagCount = (uint)newTagOrder.Count;

        //    // -------------------
        //    // Write Header Info
        //    stream.Position = 0;
        //    writer.Write((UInt32)TagOrder.Count); // tag count
        //    writer.Write((UInt32)RowCount);     // row count

        //    // -------------------
        //    // Write Tags                    
        //    foreach (var tag in TagOrder)
        //        writer.Write(tag);

        //    UnlockStore(stream);

        //    reader.Dispose();
        //    writer.Dispose();
        //    stream.Dispose();
        //}
        //public void Insert_InPlace(Store store)
        //{
        //    // Operation Prevented if Store is Locked against writing
        //    if (IsStoreLocked())
        //        throw new StoreLockedException();

        //    // Check for new tags before inserting. 
        //    var tags = store.TagOrder.Except(TagOrder).ToList();

        //    // Calculate a new table
        //    List<String> newTagOrder = new List<string>(TagOrder);
        //    newTagOrder.AddRange(tags);
        //    long newRowSize = (newTagOrder.Count * 8) + 8;

        //    // Calculate the new positons to move the rows to, and insert the new rows in order
        //    var times = Table.Keys.ToList();
        //    foreach (var row in store.Rows) // Add new row times
        //    {
        //        if (!times.Contains(row.Time))
        //            times.Add(row.Time);
        //    }
        //    times.Sort();   // Sort the times, since we will be reordering the rows anyway
        //    Dictionary<ulong, long> newTable = new Dictionary<ulong, long>((int)RowCount);
        //    long pos = HeaderBytes + (31 * newTagOrder.Count);    // Calculate new initial position after any added tags            
        //    foreach (var time in times)
        //    {
        //        newTable.Add(time, pos);
        //        pos += newRowSize;
        //    }

        //    // Start the moving
        //    int rowsRead = 0;
        //    long rowsMoved = 0;
        //    List<StoreRow> rows = new List<StoreRow>(Block);
        //    var moveTimes = newTable.Keys.ToList();    // Should already be sorted
        //    moveTimes.Reverse();    // Start with last time, should be largest offset

        //    var changingTags = store.TagOrder.Intersect(TagOrder);  // Tags that exist in both TagOrders are being updated by new data
        //    List<bool> changingTagIndexes = new List<bool>(newTagOrder.Count);
        //    foreach (var t in newTagOrder)
        //    {
        //        changingTagIndexes.Add(
        //            store.TagOrder.Contains(t)
        //        );
        //    }

        //    using (FileStream stream = new FileStream(Path, FileMode.Open, FileAccess.ReadWrite))
        //    {
        //        BinaryReader reader = new BinaryReader(stream);
        //        BinaryWriter writer = new BinaryWriter(stream);

        //        while (rowsMoved < RowCount)
        //        {
        //            // Read a block of rows
        //            while (rowsRead < moveTimes.Count && rows.Count < Block)
        //            {
        //                var time = moveTimes[rowsRead];

        //                // Data exists in datastore
        //                if (Table.ContainsKey(time))
        //                {
        //                    long readAddress = Table[time];
        //                    stream.Position = readAddress + 8;
        //                    StoreRow row = new StoreRow(time);
        //                    for (int i = 0; i < newTagOrder.Count; i++)
        //                    {
        //                        // If this tag is updated / new, read the value from the new data
        //                        var r = store.Rows.Find(x => x.Time == time);
        //                        if (changingTagIndexes[i] &&
        //                            store.Rows.Find(x => x.Time == time) != null)
        //                        {
        //                            var storeRow = store.Rows.Find(y => y.Time == time);
        //                            int x = store.TagOrder.IndexOf(newTagOrder[i]);
        //                            if (x >= 0)
        //                                row.Values.Add(storeRow.Values[x]);
        //                            stream.Position += 8;   // Skip this tag's column
        //                        }
        //                        // If it's not changing, then read the value from the DataStore
        //                        else if (i < TagOrder.Count)
        //                            row.Values.Add(reader.ReadDouble());
        //                        // If it doesn't exist in either, then this is a new value so we should pad with 0
        //                        else
        //                            row.Values.Add(0d);
        //                    }

        //                    rows.Add(row);
        //                }
        //                // Data is from row to be added, doesn't exist in datastore yet
        //                else
        //                {
        //                    var row = new StoreRow(time);
        //                    var toAdd = store.Rows.Find(x => x.Time == time);
        //                    if (toAdd != null)
        //                    {
        //                        // Add each value in the order of the new TagOrder
        //                        foreach (var tag in newTagOrder)
        //                        {
        //                            // If this tag is in new store, take it's value, otherwise pad with 0
        //                            int i = store.TagOrder.IndexOf(tag);
        //                            if (i >= 0)
        //                                row.Values.Add(toAdd.Values[i]);
        //                            else
        //                                row.Values.Add(0d);
        //                        }
        //                    }
        //                    rows.Add(row);
        //                }
        //                rowsRead++;
        //            }

        //            // Write them
        //            foreach (var row in rows)
        //            {
        //                Debug.Assert(row.Values.Count == newTagOrder.Count, "Row doesn't have correct value count when writing");

        //                // Move to new row position
        //                stream.Position = newTable[row.Time];
        //                // Write the row
        //                writer.Write(row.Time);
        //                foreach (var val in row.Values)
        //                    writer.Write(val);
        //                // write any remaining values as 0's
        //                for (int i = row.Values.Count; i < newTagOrder.Count; i++)
        //                    writer.Write((double)0);

        //                rowsMoved++;
        //            }
        //            rows.Clear();
        //        }

        //        // All rows Moved, now write the table and count
        //        Table = newTable;
        //        TagOrder = newTagOrder;
        //        TagCount = (uint)newTagOrder.Count;
        //        RowCount = (uint)newTable.Count;

        //        // -------------------
        //        // Write Header Info
        //        stream.Position = 0;
        //        writer.Write((UInt32)TagCount);     // tag count
        //        writer.Write((UInt32)RowCount);     // row count

        //        // -------------------
        //        // Write Tags                    
        //        foreach (var tag in TagOrder)
        //            writer.Write(tag);

        //        reader.Dispose();
        //        writer.Dispose();
        //    }
        //}

        public class StoreLockedException : Exception
        {
            public StoreLockedException()
                : base("Store is locked!") {
            }

            public StoreLockedException(string message)
                : base(message) {
            }

            public StoreLockedException(string message, Exception inner)
                : base(message, inner) {
            }
        }
    }
}
