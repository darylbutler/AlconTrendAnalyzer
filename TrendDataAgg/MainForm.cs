﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Windows.Threading;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace TrendDataAgg
{
    public partial class MainForm : Form
    {
        private AggregatorApp App;
        private int hoveredItemIndex = -1;
        public MainForm(AggregatorApp parent)
        {
            App = parent;   // Keep a link to the parent for comms
#if DEBUG
            TopMost = false;
#else
            TopMost = true;
#endif
            InitializeComponent();
            LoadUI();
        }
        private void LoadUI()
        {
            // Load State
            UpdateUIBecauseActivityStateChanged();

            // Load Database Path
            DatabasePathTextBox.Text = App.Settings.DataStorePath;

            // Load Watch Paths
            if (App.Settings.DataPaths != null)
            {
                foreach (var path in App.Settings.DataPaths)
                    WatchPathsListBox.Items.Add(path);
            }

            // Load Update Interval
            UpdateIntervalEdit.Value = App.Settings.UpdateInterval;

            // Archive Path
            ArchivePathBox.Text = App.Settings.ArchivePath;

            // Blacklist Path
            BlacklistPathBox.Text = App.Settings.BlacklistPath;

            // Remove Files After parse
            RemoveAfterParseCheck.Checked = App.Settings.RemoveFileAfterParse;
        }
        private void Save()
        {
            // Watch Paths
            App.Settings.DataPaths = new List<string>(WatchPathsListBox.Items.Count);
            var toDelete = new List<string>();
            foreach (string path in WatchPathsListBox.Items)
            {
                if (Directory.Exists(path))
                    App.Settings.DataPaths.Add(path);
                else
                    toDelete.Add(path);
            }
            toDelete.ForEach((x) => WatchPathsListBox.Items.Remove(x));

            // Update Interval
            App.Settings.UpdateInterval = (uint)UpdateIntervalEdit.Value;

            // Database Path
            App.Settings.DataStorePath = DatabasePathTextBox.Text;

            // Archive Path
            App.Settings.ArchivePath = ArchivePathBox.Text;

            // Blacklist Path
            App.Settings.BlacklistPath = BlacklistPathBox.Text;

            // Remove Files After parse
            App.Settings.RemoveFileAfterParse = RemoveAfterParseCheck.Checked;

            App.Settings.Save();
        }

        private void StartBtn_Click(object sender, EventArgs e)
        {
            if (App.State == ActivityState.NotConfiged || App.State == ActivityState.NotRunning)
            {
                Save();
                App.Start();
            }
            else if (App.State == ActivityState.Running)
            {
                App.Stop();
            }
            else if (App.State == ActivityState.Updating)
            {
                StatusLbl.Text = "Canceling Update";
                StartBtn.Text = "Canceling...";
                StartBtn.Enabled = false;
                App.CancelUpdate = true;
            }
        }        

        public void UpdateUIBecauseActivityStateChanged()
        {
            switch (App.State)
            {
                case ActivityState.NotConfiged:
                    StatusLbl.Text = "Cannot Run, Not Configured. Check Settings below";
                    ToolTip.SetToolTip(StatusLbl, "Do we have any Watch Paths? Do we have a valid Database, Archive, and Blacklist paths?");
                    StartBtn.Text = "Start";
                    StartBtn.Enabled = true;
                    StatusLbl.ForeColor = Color.IndianRed;
                    break;

                case ActivityState.NotRunning:
                    StatusLbl.Text = "Not Running";
                    ToolTip.SetToolTip(StatusLbl, "Start to begin Updating");
                    StartBtn.Text = "Start";
                    StartBtn.Enabled = true;
                    StatusLbl.ForeColor = Color.BurlyWood;
                    break;

                case ActivityState.Running:
                    StatusLbl.Text = "Running";
                    ToolTip.SetToolTip(StatusLbl, "Everything is okay, we're in-between updates right now");
                    StartBtn.Text = "Stop";
                    StartBtn.Enabled = true;
                    StatusLbl.ForeColor = Color.Green;
                    break;

                case ActivityState.Updating:
                    StatusLbl.Text = "Performing Update...";
                    ToolTip.SetToolTip(StatusLbl, "The aggregator is currently parsing and adding files to the database");
                    StartBtn.Text = "Cancel";
                    //StartBtn.Enabled = false;
                    StatusLbl.ForeColor = Color.YellowGreen;
                    break;
            }
        }
        public void UILog(string text)
        {
            ActivityLog.Items.Add(string.Format("{0} -- {1}", DateTime.Now.ToString(), text));

            // Scroll to the bottom
            int visibleItems = ActivityLog.ClientSize.Height / ActivityLog.ItemHeight;
            ActivityLog.TopIndex = Math.Max(ActivityLog.Items.Count - visibleItems + 1, 0);
        }

        private void RemovePathBtn_Click(object sender, EventArgs e)
        {
            if (WatchPathsListBox.SelectedItem == null) return;

            WatchPathsListBox.Items.Remove(WatchPathsListBox.SelectedItem);
        }
        private void NewPathBtn_Click(object sender, EventArgs e)
        {
            using (CommonOpenFileDialog dlg = new CommonOpenFileDialog())
            {
                dlg.IsFolderPicker = true;
                dlg.Title = "Select a data location";
                dlg.EnsurePathExists = true;
                dlg.Multiselect = false;

                if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
                    WatchPathsListBox.Items.Add(dlg.FileName);
            }
        }        
        private void DatabasePathBrowseBtn_Click(object sender, EventArgs e)
        {
            using (CommonOpenFileDialog dlg = new CommonOpenFileDialog())
            {
                dlg.IsFolderPicker = false;
                dlg.Title = "Select the database location";
                dlg.EnsurePathExists = true;
                dlg.EnsureFileExists = false;
                dlg.Multiselect = false;

                if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
                    DatabasePathTextBox.Text = dlg.FileName;
            }
        }
        private void ArchivePathBrowseBtn_Click(object sender, EventArgs e)
        {
            using (CommonOpenFileDialog dlg = new CommonOpenFileDialog())
            {
                dlg.IsFolderPicker = true;
                dlg.Title = "Select the Archive Directory Location";
                dlg.EnsurePathExists = true;
                dlg.Multiselect = false;

                if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
                    ArchivePathBox.Text = dlg.FileName;
            }
        }
        private void BlacklistPathBrowseBtn_Click(object sender, EventArgs e)
        {
            using (CommonOpenFileDialog dlg = new CommonOpenFileDialog())
            {
                dlg.IsFolderPicker = true;
                dlg.Title = "Select the Blacklist Directory Location";
                dlg.EnsurePathExists = true;
                dlg.Multiselect = false;

                if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
                    BlacklistPathBox.Text = dlg.FileName;
            }
        }
        private void DumpStoreBrowseBtn_Click(object sender, EventArgs e)
        {
            using (CommonOpenFileDialog dlg = new CommonOpenFileDialog())
            {
                dlg.IsFolderPicker = false;
                dlg.Title = "Select the export location";
                dlg.EnsurePathExists = true;
                dlg.EnsureFileExists = false;
                dlg.Multiselect = false;

                if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
                    DumpStorePathBox.Text = dlg.FileName;
            }
        }

        private async void DumpStoreBtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(DumpStorePathBox.Text) || App.State == ActivityState.Updating)
                return;

            ActivityLog.Items.Add("Stopping App for an Export.");
            App.Stop();
            ActivityLog.Items.Add("Starting Export...");
            
            await Task.Run(async () => {
               await Task.Run(() =>
               {
                   App.DumpStore(DumpStorePathBox.Text);
               });
            });
            ActivityLog.Items.Add("Completed Export.");
        }
        private async void DefragBtn_Click(object sender, EventArgs e)
        {
            ActivityLog.Items.Add("Stopping App for a Defrag...");
            App.Stop();
            ActivityLog.Items.Add("Starting Defrag...");
            
            await Task.Run(async () => {
               await Task.Run(() =>
               {
                   App.Defrag();
               });
            });
            ActivityLog.Items.Add("Completed Defrag.");
        }

        // Display tooltip of log entry hovered
        private void ActivityLog_MouseMove(object sender, MouseEventArgs e)
        {
            // See which row is currently under the mouse:
            int newHoveredIndex = ActivityLog.IndexFromPoint(e.Location);

            // If the row has changed since last moving the mouse:
            if (hoveredItemIndex != newHoveredIndex)
            {
                // Change the variable for the next timw we move the mouse:
                hoveredItemIndex = newHoveredIndex;

                // If over a row showing data (rather than blank space):
                if (hoveredItemIndex > -1)
                {
                    //Set tooltip text for the row now under the mouse:
                    ToolTip.Active = false;
                    ToolTip.SetToolTip(ActivityLog, ActivityLog.Items[hoveredItemIndex].ToString());
                    ToolTip.Active = true;
                }
            }
        }
        private void ActivityLog_MouseLeave(object sender, EventArgs e)
        {
            hoveredItemIndex = -1;
            ToolTip.Active = false;
        }

        private async void QueryFileBtn_Click(object sender, EventArgs e)
        {
            using (CommonOpenFileDialog dlg = new CommonOpenFileDialog()) {
                dlg.IsFolderPicker = false;
                dlg.Title = "Select the query file";
                dlg.EnsurePathExists = true;
                dlg.Multiselect = false;

                if (dlg.ShowDialog() == CommonFileDialogResult.Ok) {
                    ActivityLog.Items.Add("Executing Query File...");

                    var sw = new Stopwatch();
                    sw.Start();
                    await Task.Run(async () => {
                        await Task.Run(() =>
                        {
                            App.ExecuteQueryFile(dlg.FileName);
                        });
                    });
                    sw.Stop();

                    ActivityLog.Items.Add(string.Format("Query Finished, took {0}ms", sw.ElapsedMilliseconds));
                }
            }
        }
    }
}
