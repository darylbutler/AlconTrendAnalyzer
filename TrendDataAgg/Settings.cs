﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendDataAgg
{
    public class Settings
    {
        // Settings to Manage
        public List<string> DataPaths = new List<string>();
        public List<string> FilesToDelete = new List<string>();
        public string DataStorePath = "";
        public string ArchivePath = "";
        public string BlacklistPath = "";
        public uint UpdateInterval = 120;
        public bool RemoveFileAfterParse = false;
        public ulong MaxRowsPerShard = 17280 * 30 * 2;

        public DateTime LastDefragTime = DateTime.MinValue;       

        // private members
        private string Path;

        // Constructor
        public Settings(string path = "settings")
        {
            Path = path;
            Load();
        }

        public void Load()
        {
            if (!File.Exists(Path))
            {
                Save();
                return;
            }

            var lines = File.ReadAllLines(Path);
            foreach (var line in lines)
            {
                var split = line.Split('=');
                if (split.Length < 2)
                    continue;

                string key = split[0];
                string value = split[1];

                switch (key.ToLower())
                {
                    case "datapaths":
                        DataPaths = new List<string>(value.Split(','));
                        break;
                    case "datastorepath":
                        DataStorePath = value;
                        break;
                    case "archivepath":
                        ArchivePath = value;
                        break;
                    case "blacklistpath":
                        BlacklistPath = value;
                        break;
                    case "updateinterval":
                        UpdateInterval = uint.Parse(value);
                        break;
                    case "removefileafterparse":
                        RemoveFileAfterParse = value == "true";
                        break;
                    case "lastdefragtime":
                        LastDefragTime = DateTime.Parse(value);
                        break;
                    case "filestodelete":
                        FilesToDelete = value.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                        break;
                    case "maxrowspershard":
                        if (ulong.TryParse(value, out ulong r))
                            MaxRowsPerShard = r;
                        break;
                }
            }
        }
        public void Save()
        {
            using (var stream = new FileStream(Path, FileMode.Create, FileAccess.Write))
            using (var writer = new StreamWriter(stream))
            {
                writer.WriteLine("DataPaths={0}", string.Join(",", DataPaths));
                writer.WriteLine("FilesToDelete={0}", string.Join(",", FilesToDelete));
                writer.WriteLine("DataStorePath={0}", DataStorePath);
                writer.WriteLine("ArchivePath={0}", ArchivePath);
                writer.WriteLine("BlacklistPath={0}", BlacklistPath);
                writer.WriteLine("UpdateInterval={0}", UpdateInterval);
                writer.WriteLine("RemoveFileAfterParse={0}", RemoveFileAfterParse ? "true" : "false");
                writer.WriteLine("LastDefragTime={0}", LastDefragTime.ToString());
                writer.WriteLine("MaxRowsPerShard={0}", MaxRowsPerShard);
            }
        }

    }
}
