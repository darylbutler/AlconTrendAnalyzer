﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace TrendDataAgg
{
    /****
     * Changes: 
     *  - Time changed from ulong to uint: ulong.MaxValue was > DateTime.MaxValue, and uint32.MaxValue == year 2116, pretty sure we aren't going to be using this then
     *  - Values changed from double to float: float is precise to 7 digits, which should be plenty for this application
     * 
     * Old version. Tested this live on the server and it worked well. The only problem was that the store file got larger
     * than can be seekable (Stream.Position is a signed long, limiting size). New version is a store with multiple data files / shards
     ***/

    public class DataStore_v1
    {
        // 4[Version] 1[Locked] 1[TimeLength] 1[ValueLength] 8[OldestTime] 8[NewestTime] 4[TagCount] 4[RowCount]
        const int HeaderBytes = 4 + 1 + 1 + 1 + 8 + 8 + 4 + 4;
        // Max # of rows to read into memory before moving them
        const int Block = 1024;
        // Buffer amounts for reading / writing to datastore
        const int ReadBlock = Block * 500;
        const int WriteBlock = Block * 500;
        // This version of the DataStore implementation is v1
        const int DefaultVersion = 1;

        // Public
        public bool TruncateOldTimes = true;    // If true, when we do a defrag, we'll erase any rows older than TruncateTimesBefore
        public ulong TruncateTimesBefore = 0;   // Default to no deletions
        public uint TagCount { get; private set; }  // Count of tags in this database                  
        public uint RowCount { get; private set; }  // Count of rows in the database
        public ulong NewestTime { get; private set; }    // Latest (Greatest) time in database
        public ulong OldestTime { get; private set; }    // Earliest (Least) time in database
        public List<string> Files;
        public List<string> TagNames { get { return TagOrder; } }
        public List<TagData> Tags {
            get {
                List<TagData> tags = new List<TagData>();
                foreach (var name in TagOrder)
                {
                    string desc = string.Empty;
                    // First, check our custom list of descriptions
                    if (TagDescriptions.ContainsKey(name))
                        desc = TagDescriptions[name];
                    else
                    {
                        // Fall back to CSV reader
                        desc = CSVReader.GetTagDescription(name);
                    }
                    // No matter what, prevent a tag with no description
                    if (desc == string.Empty)
                        desc = name;

                    tags.Add(new TagData() { Name = name, Description = desc });
                }
                return tags;
            }
        }
        public Dictionary<string, string> TagDescriptions;

        /// <summary>
        /// If true, we will not update the table after an update. Turn this on to start a batch update, then turn it back off
        /// </summary>
        public bool DelayWritingTable
        {
            get { return !DoWriteTable; }
            set
            {
                if (value)
                {
                    // Invalidate the current HD table (Table in memory stays okay)
                    if (TableExists())
                        File.Delete(TablePath);
                    DoWriteTable = false;
                }
                else
                {
                    // Sync the memory table to HD
                    DoWriteTable = true;
                    WriteTable(Table);
                }
            }
        }

        // Private
        private StoreAccess Mode = StoreAccess.Query;   // The rights we have to this database
        private UInt32 Version = 0;             // Version of the file (Not Used)
        private bool Locked = false;            // True is store is locked (Because it's being reorganized via Defrag)
        string Path;                            // Path to the hardrive file
        Dictionary<ulong, long> Table = null;   // Keep the Offset Table in memory
        List<string> TagOrder;                  // List of Tag Names for their position in the rows
        private bool DoWriteTable = true;       // background for DelayWritingTable
        private int _timeBytes = 4;             // Byte length of time as start of a row
        private int _valueBytes = 4;            // Byte length of values in a row

        // Calculated values        
        private int TimeBytes { get { return _timeBytes; } }
        private int ValueBytes { get { return _valueBytes; } }
        private long RowBytes { get { return TimeBytes + (TagCount * ValueBytes); } }
        private string TagDescPath { get { return string.IsNullOrEmpty(Path) ? string.Empty : System.IO.Path.ChangeExtension(Path, "tags"); } }
        private string TablePath { get { return string.IsNullOrEmpty(Path) ? string.Empty : System.IO.Path.ChangeExtension(Path, "table"); } }
        private string FilesPath { get { return string.IsNullOrEmpty(Path) ? string.Empty : System.IO.Path.ChangeExtension(Path, "files"); } }

        // ----------------------------------------------------------------------------------------
        // Creation
        public DataStore_v1(StoreAccess mode = StoreAccess.Query) { InitialLoad(mode); }
        public DataStore_v1(string p, StoreAccess mode = StoreAccess.Query) { Path = p; InitialLoad(mode); }
        private void InitialLoad(StoreAccess mode)
        {
            // Save the access Mode
            Mode = mode;

            // True if this data store was correct on the disk
            bool opened = false;

            // If the file exists, then attempt to load from it
            if (File.Exists(Path) && (mode == StoreAccess.Append || mode == StoreAccess.Query))
            {
                using (var stream = new FileStream(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, ReadBlock))
                {
                    if (stream.Length >= HeaderBytes)
                    {
                        using (var reader = new BinaryReader(stream))
                        {
                            // Read Header -- 4[Version] 1[Locked] 1[TimeLength] 1[ValueLength] 8[OldestTime] 8[NewestTime] 4[TagCount] 4[RowCount]
                            Version = reader.ReadUInt32();
                            Locked = reader.ReadBoolean();
                            _timeBytes = reader.ReadByte();
                            _valueBytes = reader.ReadByte();
                            OldestTime = reader.ReadUInt64();
                            NewestTime = reader.ReadUInt64();
                            TagCount = reader.ReadUInt32();
                            RowCount = reader.ReadUInt32();
                            Debug.Assert(stream.Position == HeaderBytes, "Header reading in InitialLoad incorrect position after header load");

                            opened = true;

                            // Read Tag Order
                            TagOrder = new List<string>((int)TagCount);
                            for (int i = 0; i < TagCount; i++)
                                TagOrder.Add(reader.ReadString());

                            // Read In OffsetTable - we don't store the offset anymore, the stream pos is where rows start
                            if (TableExists())
                                Table = ReadTable();
                            else
                            {
                                // Manually read table
                                Table = new Dictionary<ulong, long>((int)RowCount);

                                // Don't try to read past the RowCount
                                for (int i = 0; i < RowCount; i++)
                                {
                                    long pos = stream.Position;
                                    ulong time = reader.ReadUInt32();
                                    Table.Add(time, pos);
                                    stream.Position = pos + RowBytes;
                                }
                                // Write the table so we don't have to do this again
                                if (mode != StoreAccess.Query)
                                    WriteTable(Table);
                            }

                            // Read in files list (Files we've already processed)
                            if (FilesExists())
                                Files = ReadFiles();
                            else
                            {
                                Files = new List<string>();
                                WriteFiles(Files);
                            }

                            // Read Tag descriptions
                            if (TagDescExists())
                                TagDescriptions = ReadTagDescriptions();
                            else
                            {
                                TagDescriptions = new Dictionary<string, string>();
                                WriteTagDesc(TagDescriptions);
                            }
                        }
                    }
                    
                }
            }

            if (!opened)
            {
                if (mode == StoreAccess.Query)
                {
                    // We cannot recreate the datastore with query access, throw error
                    throw new Exception("Datastore not found! Cannot create new with Query access");
                }

                // Empty Offset table, no rows
                Version = DefaultVersion;
                TagCount = 0;
                RowCount = 0;
                NewestTime = OldestTime = 0;
                TagOrder = new List<string>();
                Table = new Dictionary<ulong, long>();
                Files = new List<string>();
                TagDescriptions = new Dictionary<string, string>();
                using (var stream = new FileStream(Path, FileMode.Create, FileAccess.ReadWrite, FileShare.Read, WriteBlock))
                {
                    using (var writer = new BinaryWriter(stream))
                    {
                        // -------------------
                        // Write Header Info
                        stream.Position = 0;
                        Locked = false;
                        Util_WriteHeader(writer);

                        // -------------------
                        // Write Tags                    
                        foreach (var tag in TagOrder)
                            writer.Write(tag);

                        // Write the empty table to empty table file if it already exists
                        WriteTable(Table);
                    }
                }
            }
        }

        // ----------------------------------------------------------------------------------------
        // Table
        private bool TableExists()
        {
            return File.Exists(TablePath);
        }
        private Dictionary<ulong, long> ReadTable()
        {
            if (!TableExists()) return new Dictionary<ulong, long>();

            using (var stream = new FileStream(TablePath, FileMode.Open, FileAccess.Read, FileShare.Read, ReadBlock))
            {
                using (var reader = new BinaryReader(stream))
                {
                    uint rows = reader.ReadUInt32();
                    var table = new Dictionary<ulong, long>((int)rows);

                    for (int i = 0; i < rows; i++)
                    {
                        table.Add(reader.ReadUInt64(), reader.ReadInt64());
                    }
                    return table;
                }
            }
        }
        private void WriteTable(Dictionary<ulong, long> table)
        {
            // Skip if we're in a batch update
            if (!DoWriteTable)
                return;

            // Find the new time bounds
            ulong smallest = ulong.MaxValue,
                  greatest = ulong.MinValue;

            using (var stream = new FileStream(TablePath, FileMode.Create, FileAccess.Write, FileShare.Read, 4 + (16 * table.Count)))
            {
                using (var writer = new BinaryWriter(stream))
                {
                    writer.Write((UInt32)table.Count);
                    foreach (var row in table)
                    {
                        writer.Write(row.Key);
                        writer.Write(row.Value);

                        // Time bounds
                        if (row.Key > greatest)
                            greatest = row.Key;
                        if (row.Key < smallest)
                            smallest = row.Key;
                    }
                }
            }
            NewestTime = greatest;
            OldestTime = smallest;
        }

        // ----------------------------------------------------------------------------------------
        // Files
        private bool FilesExists()
        {
            return File.Exists(FilesPath);
        }
        private List<string> ReadFiles()
        {
            if (!FilesExists()) return new List<string>();

            using (var stream = new FileStream(FilesPath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = new BinaryReader(stream))
                {
                    uint rows = reader.ReadUInt32();
                    var files = new List<string>((int)rows);

                    for (int i = 0; i < rows; i++)
                    {
                        files.Add(reader.ReadString());
                    }
                    return files;
                }
            }
        }
        private void WriteFiles(List<string> files)
        {
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            using (var stream = new FileStream(FilesPath, FileMode.Create, FileAccess.Write))
            {
                using (var writer = new BinaryWriter(stream))
                {
                    writer.Write(files.Count);
                    foreach (var row in files)
                        writer.Write(row);
                }
            }
        }
        public bool HasFileBeenProcessed(string file)
        {
            return Files.Contains(file);
        }
        public void AddProcessedFile(string file)
        {
            // Check for mode allows this modification
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            if (!HasFileBeenProcessed(file))
            {
                Files.Add(file);
                WriteFiles(Files);
            }
        }

        // ----------------------------------------------------------------------------------------
        // Tag Descriptions
        private bool TagDescExists()
        {
            return File.Exists(TagDescPath);
        }
        private Dictionary<string,string> ReadTagDescriptions()
        {
            var tags = new Dictionary<string, string>();
            if (!TagDescExists()) return tags;

            using (var stream = new FileStream(TagDescPath, FileMode.Open, FileAccess.Read))
            using (var reader = new StreamReader(stream))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    var split = line.Split(':', ';');

                    if (split.Length > 1)
                    {
                        tags.Add(split[0], split[1]);
                    }
                }
            }
            return tags;
        }
        private void WriteTagDesc(Dictionary<string, string> tags)
        {
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            using (var stream = new FileStream(TagDescPath, FileMode.Create, FileAccess.Write))
            using (var writer = new StreamWriter(stream))
            {
                foreach (var line in tags)
                {
                    writer.WriteLine("{0}:{1}", line.Key, line.Value);
                }
            }
        }
        public void SaveTagDescriptions() => WriteTagDesc(TagDescriptions);        

        // ----------------------------------------------------------------------------------------
        // Modifying
        public void AddData(List<TagData> data, string file = null)
        {
            // Check for mode allows this modification
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            if (file != null && HasFileBeenProcessed(file))
                return;

            // Turn this data into a Store
            AddData(new Store(data), file);
        }
        public void AddData(Store store, string file = null)
        {
            // Check for mode allows this modification
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            if (file != null && HasFileBeenProcessed(file))
                return;

            // First, check if this data is new enough for us to add.
            if (TruncateOldTimes)
            {
                int i = 0;
                while (i < store.Rows.Count)
                {
                    if (store.Rows[i].Time < TruncateTimesBefore)
                        store.Rows.RemoveAt(i);
                    else
                        i++;
                }
            }

            // Only add if we have any data left
            if (store.Rows.Count > 0)
            {
                // Check for new tags before appending new rows
                if (store.TagOrder.Except(TagOrder).Count() > 0)
                    Insert(store);
                else
                    Append_InPlace(store);
            }

            if (file != null)
                AddProcessedFile(file);
        }
        public void Defrag()
        {
            // Reorganize the rows in order of time, compacting the file and dropping old
            // rows in the process

            // Check for mode allows this modification
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            // Operation Prevented if Store is Locked against writing
            if (IsStoreLocked())
                throw new StoreLockedException();

            // Calculate the new positons to move the rows to
            Dictionary<ulong, long> newTable = new Dictionary<ulong, long>((int)RowCount);
            // Sort the times, since we will be reordering the rows anyway
            var times = Table.Keys.ToList();
            times.Sort();
            // Calculate initial stream position, just after tags
            long pos = HeaderBytes + TagOrderBytes(TagOrder);
            ulong newestTime = ulong.MinValue, oldestTime = ulong.MaxValue;
            foreach (var time in times)
            {
                // Throw the old times away to constrain the db size
                if (!TruncateOldTimes || time >= TruncateTimesBefore)
                {
                    newTable.Add(time, pos);
                    pos += RowBytes;

                    // Time bounds
                    if (time > newestTime)
                        newestTime = time;
                    if (time < oldestTime)
                        oldestTime = time;
                }

                if (pos > long.MaxValue - 10)
                    throw new Exception("File Too Large for defrag");
            }

            // Start the moving
            List<StoreRow> rows = new List<StoreRow>(Block);
            int rowsRead = 0;
            long rowsMoved = 0;
            times = newTable.Keys.ToList(); // Already sorted above

            // Open the stream
            string TempPath = System.IO.Path.ChangeExtension(Path, "~");
            FileStream sStream = new FileStream(Path, FileMode.Open, FileAccess.Read, FileShare.Read, ReadBlock);
            FileStream nStream = new FileStream(TempPath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite, WriteBlock);
            BinaryReader reader = new BinaryReader(sStream);
            BinaryWriter writer = new BinaryWriter(nStream);

            // -------------------
            // Write Header
            Util_WriteHeader(writer, Version, true, oldestTime, newestTime, (uint)TagOrder.Count, (uint)times.Count);

            // -------------------
            // Write Tags                    
            Util_WriteTags(writer, TagOrder);

            // -------------------
            // Transfer the rows
            while (rowsMoved < newTable.Count)
            {
                // Read a block of rows
                while (rowsRead < times.Count && rows.Count < Block)
                {
                    var time = times[rowsRead];

                    // Read in this row
                    sStream.Position = Table[time];
                    var row = Util_ReadRow(reader, (uint)TagOrder.Count);
                    Debug.Assert(row.Time == time, "Read Row || New Table Error!");
                    rows.Add(row);
                    rowsRead++;
                }

                // Write them
                foreach (var row in rows)
                {
                    Debug.Assert(newTable[row.Time] == nStream.Position, "Stream / Table Off Sync");

                    // Write the row                   
                    Util_WriteRow(writer, row);
                    rowsMoved++;
                }
                rows.Clear();
            }

            // -------------------
            // Remember the new table so we don't have to refresh from the HD
            Table = newTable;
            NewestTime = NewestTime;
            OldestTime = oldestTime;
            RowCount = (uint)times.Count;

            // Cleanup
            reader.Dispose();
            writer.Dispose();
            sStream.Dispose();
            nStream.Dispose();

            File.Replace(TempPath, Path, System.IO.Path.ChangeExtension(Path, "t"));
            WriteTable(Table);
            UnlockStore();
        }
        public void Append(Store store)
        {
            // Check for mode allows this modification
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            // Operation Prevented if Store is Locked against writing
            if (IsStoreLocked())
                throw new StoreLockedException();

            // find the positon to insert the new values (last row or calculated first row location, if Table empty)
            long lastRowEnd = Table.Count > 0 ? (Table.Values.Max() + RowBytes) : (HeaderBytes + TagOrderBytes(TagOrder));

            // Copy the file so we can work on it
            string TempPath = System.IO.Path.ChangeExtension(Path, "~");
            File.Copy(Path, TempPath, true);
            FileStream stream = new FileStream(TempPath, FileMode.Open, FileAccess.Write, FileShare.ReadWrite, WriteBlock);
            BinaryWriter writer = new BinaryWriter(stream);

            // Write the new rows onto the end of the current rows
            foreach (var row in store.Rows)
            {
                // Update Existing Row
                if (Table.ContainsKey(row.Time))
                {
                    // Skip to the row (skip past time bytes)
                    stream.Position = Table[row.Time] + TimeBytes;

                    // Write the tags we have (They may have been previously been inserted as 0's)
                    for (int i = 0; i < TagOrder.Count; i++)
                    {
                        int tag = store.TagOrder.IndexOf(TagOrder[i]);

                        // If the store has this tag, then write it
                        if (tag >= 0)
                            Util_WriteValue(writer, row.Values[tag]);
                        else
                            stream.Position += ValueBytes; // Skip this double
                    }
                }
                // Add new Row to end of file
                else
                {
                    // Update the in-memory Table                            
                    Table.Add(row.Time, lastRowEnd);

                    // Keep track of the newest rows we're adding
                    if (row.Time > NewestTime)
                        NewestTime = row.Time;
                    if (row.Time < OldestTime)
                        OldestTime = row.Time;

                    // Write the new rows to the end of the file
                    stream.Position = lastRowEnd;
                    Util_WriteTime(writer, row.Time);
                    for (int i = 0; i < TagOrder.Count; i++)
                    {
                        int tag = store.TagOrder.IndexOf(TagOrder[i]);

                        Util_WriteValue(writer, (tag < 0 ? 0 : row.Values[tag]));
                    }

                    // move past last row
                    lastRowEnd += RowBytes;
                    Debug.Assert(stream.Position == lastRowEnd);
                }
            }

            // Update Row count
            RowCount = (uint)Table.Count;
            Util_WriteHeader(writer, true);

            // Cleanup
            writer.Dispose();
            stream.Dispose();

            // Begin copy
            LockStore();
            //File.Copy(TempPath, Path, true);
            //File.Delete(TempPath);
            File.Replace(TempPath, Path, null);
            WriteTable(Table);
            UnlockStore();
        }
        public void Append_InPlace(Store store)
        {
            // Check for mode allows this modification
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            // Operation Prevented if Store is Locked against writing
            if (IsStoreLocked())
                throw new StoreLockedException();

            // find the positon to insert the new values (last row or calculated first row location, if Table empty)
            long lastRowEnd = Table.Count > 0 ? (Table.Values.Max() + RowBytes) : (HeaderBytes + TagOrderBytes(TagOrder));

            FileStream stream = new FileStream(Path, FileMode.Open, FileAccess.Write, FileShare.ReadWrite, WriteBlock);
            BinaryWriter writer = new BinaryWriter(stream);

            // Write the new rows onto the end of the current rows
            foreach (var row in store.Rows)
            {
                // Update Existing Row
                if (Table.ContainsKey(row.Time))
                {
                    // Skip to the row (skip past time bytes)
                    stream.Position = Table[row.Time] + TimeBytes;

                    // Write the tags we have (They may have been previously been inserted as 0's)
                    for (int i = 0; i < TagOrder.Count; i++)
                    {
                        int tag = store.TagOrder.IndexOf(TagOrder[i]);

                        // If the store has this tag, then write it
                        if (tag >= 0)
                            Util_WriteValue(writer, row.Values[tag]);
                        else
                            stream.Position += ValueBytes; // Skip this double
                    }
                }
                // Add new Row to end of file
                else
                {
                    // Update the in-memory Table                            
                    Table.Add(row.Time, lastRowEnd);

                    // Keep track of the newest rows we're adding
                    if (row.Time > NewestTime)
                        NewestTime = row.Time;
                    if (row.Time < OldestTime)
                        OldestTime = row.Time;

                    // Write the new rows to the end of the file
                    stream.Position = lastRowEnd;
                    Util_WriteTime(writer, row.Time);
                    for (int i = 0; i < TagOrder.Count; i++)
                    {
                        int tag = store.TagOrder.IndexOf(TagOrder[i]);

                        Util_WriteValue(writer, (tag < 0 ? 0 : row.Values[tag]));
                    }

                    // move past last row
                    lastRowEnd += RowBytes;
                    Debug.Assert(stream.Position == lastRowEnd);
                }
            }

            // Update Row count
            RowCount = (uint)Table.Count;            
            Util_WriteHeader(writer);
            WriteTable(Table);

            stream.Flush();
            writer.Dispose();
            stream.Dispose();
        }
        public void Insert(Store store)
        {
            // Check for mode allows this modification
            if (Mode == StoreAccess.Query)
                throw new Exception("Store Access called to modify when store mode is only Query!");

            // Operation Prevented if Store is Locked against writing
            if (IsStoreLocked())
                throw new StoreLockedException();

            // Calculate a new table
            List<String> newTagOrder = new List<string>(TagOrder);
            foreach (var tag in store.TagOrder)
            {
                if (!TagOrder.Contains(tag))
                    newTagOrder.Add(tag);
            }
            long newRowSize = (newTagOrder.Count * ValueBytes) + TimeBytes;

            // Calculate the new positons to move the rows to, and insert the new rows in order
            var times = Table.Keys.ToList();
            foreach (var row in store.Rows) // Add new row times
            {
                if (!times.Contains(row.Time))
                    times.Add(row.Time);
            }
            times.Sort();   // Sort the times, since we will be reordering the rows anyway
            Dictionary<ulong, long> newTable = new Dictionary<ulong, long>((int)RowCount);
            long pos = HeaderBytes + TagOrderBytes(newTagOrder);    // Calculate new initial position after any added tags   
            ulong oldestTime = ulong.MaxValue, newestTime = ulong.MinValue;
            foreach (var time in times)
            {
                newTable.Add(time, pos);
                pos += newRowSize;

                // Time bounds
                if (time > newestTime)
                    newestTime = time;
                if (time < oldestTime)
                    oldestTime = time;
            }

            // Start the moving
            List<StoreRow> rows = new List<StoreRow>(Block);
            int rowsRead = 0;
            long rowsMoved = 0;
            var moveTimes = newTable.Keys.ToList();    // Should already be sorted

            // Get tags that exist in both TagOrders are being updated by new data
            var changingTags = store.TagOrder.Intersect(TagOrder);
            List<bool> changingTagIndexes = new List<bool>(newTagOrder.Count);
            foreach (var t in newTagOrder)
            {
                changingTagIndexes.Add(
                    store.TagOrder.Contains(t)
                );
            }
            // Only check for updates on times between these
            ulong storeStartTime = store.Rows.Min(x => x.Time);
            ulong storeEndTime = store.Rows.Max(x => x.Time);

            // Open The File
            string TempPath = System.IO.Path.ChangeExtension(Path, "~");
            FileStream sStream = new FileStream(Path, FileMode.Open, FileAccess.Read, FileShare.Read, ReadBlock);
            BinaryReader reader = new BinaryReader(sStream);
            FileStream nStream = new FileStream(TempPath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite, WriteBlock);
            BinaryWriter writer = new BinaryWriter(nStream);

            // -------------------
            // Write Header
            Util_WriteHeader(writer, 1, true, oldestTime, newestTime, (uint)newTagOrder.Count, (uint)moveTimes.Count);

            // -------------------
            // Write Tags                    
            Util_WriteTags(writer, newTagOrder);

            // -------------------
            // Transfer Rows
            while (rowsMoved < moveTimes.Count)
            {
                // Read a block of rows
                while (rowsRead < moveTimes.Count && rows.Count < Block)
                {
                    var time = moveTimes[rowsRead];
                    var row = new StoreRow(time);

                    // Data exists in datastore
                    if (Table.ContainsKey(time))
                    {
                        long readAddress = Table[time];
                        sStream.Position = readAddress + TimeBytes;

                        // If this time isn't changing, then just copy the row
                        if (time < storeStartTime || time > storeEndTime)
                        {
                            for (int i = 0; i < newTagOrder.Count; i++)
                            {
                                if (i < TagOrder.Count)
                                    row.Values.Add(Util_ReadValue(reader));
                                else
                                    row.Values.Add(0d);
                            }
                        }
                        else
                        {
                            // This row might be chaning, so we have to check
                            for (int i = 0; i < newTagOrder.Count; i++)
                            {
                                var storeRow = store.Rows.Find(y => y.Time == time);
                                if (changingTagIndexes[i] && storeRow != null)
                                {
                                    // If this tag is updated | new, read the value from the new data
                                    int x = store.TagOrder.IndexOf(newTagOrder[i]);
                                    if (x >= 0)
                                        row.Values.Add(storeRow.Values[x]);
                                    else
                                        row.Values.Add(0d);
                                    sStream.Position += ValueBytes;   // Skip this tag's column
                                }
                                else if (i < TagOrder.Count)
                                {
                                    // If it's not changing, then read the value from the DataStore
                                    row.Values.Add(Util_ReadValue(reader));
                                }
                                else
                                {
                                    // If it doesn't exist in either, then this is a new value so we should pad with 0
                                    sStream.Position += ValueBytes;     // Skip this tag's column
                                    row.Values.Add(0d);                 // Add a default value
                                }

                            }
                        }
                    }
                    // Data is from row to be added, doesn't exist in datastore yet
                    else
                    {
                        var toAdd = store.Rows.Find(x => x.Time == time);
                        if (toAdd != null)
                        {
                            // Add each value in the order of the new TagOrder
                            foreach (var tag in newTagOrder)
                            {
                                // If this tag is in new store, take it's value, otherwise pad with 0
                                int i = store.TagOrder.IndexOf(tag);
                                if (i >= 0)
                                    row.Values.Add(toAdd.Values[i]);
                                else
                                    row.Values.Add(0d);
                            }
                        }
                    }
                    rows.Add(row);
                    rowsRead++;
                }

                // Write them
                foreach (var row in rows)
                {
                    Debug.Assert(row.Values.Count == newTagOrder.Count, "Row doesn't have correct value count when writing");
                    Debug.Assert(newTable[row.Time] == nStream.Position, "Stream / Table Off Sync");

                    // Write the row
                    Util_WriteRow(writer, row);
                    rowsMoved++;
                }
                rows.Clear();
            }

            // Writing Done
            reader.Dispose();
            sStream.Dispose();
            writer.Dispose();
            nStream.Dispose();

            // All rows Moved, transfer file over
            Table = newTable;
            TagOrder = newTagOrder;
            RowCount = (uint)newTable.Count;
            TagCount = (uint)TagOrder.Count;
            NewestTime = NewestTime;
            OldestTime = oldestTime;

            OverwriteWith(TempPath);
            WriteTable(Table);
            Debug.Assert(!IsStoreLocked(), "Store not unlocked after Insert Completed");
        }

        // Sep the Reading / Writing
        private void OverwriteWith(string temp, bool delete = true)
        {
            if (!File.Exists(temp))
                throw new ArgumentException("Path doesn't exist");

            using (FileStream r = new FileStream(temp, FileMode.Open, FileAccess.Read, FileShare.Read, ReadBlock))
            {
                using (FileStream w = new FileStream(Path, FileMode.Open, FileAccess.Write, FileShare.ReadWrite, WriteBlock))
                {
                    // Before we change the file, update the header to indicate we're locking the file (Don't read)
                    LockStore(w);
                    // Overwrite ourselves with this data
                    r.CopyTo(w);
                    // Indicate we're done writing (Reading possible)
                    UnlockStore(w); // Calls flush
                }
            }
            // Cleanup if requested
            if (delete)
                File.Delete(temp);
        }
        private StoreRow Util_ReadRow(BinaryReader r, uint tagCount)
        {
            // Read Time
            StoreRow row = new StoreRow(Util_ReadTime(r));

            for (int i = 0; i < tagCount; i++)
                row.Values.Add(Util_ReadValue(r));

            return row;
        }
        private ulong Util_ReadTime(BinaryReader r)
        {
            if (TimeBytes == 4)
                return r.ReadUInt32();
            else if (TimeBytes == 8)
                return r.ReadUInt64();
            else
                throw new Exception("Unsupported Time Byte Length!");
        }
        private double Util_ReadValue(BinaryReader r)
        {
            if (ValueBytes == 4)
                return r.ReadSingle();
            else if (ValueBytes == 8)
                return r.ReadDouble();
            else
                throw new Exception("Unsupported Value Length");
        }
        private void Util_WriteRow(BinaryWriter w, StoreRow row)
        {
            // Write Time
            Util_WriteTime(w, row.Time);

            // write each Value
            foreach (var val in row.Values)
                Util_WriteValue(w, val);
        }
        private void Util_WriteTime(BinaryWriter w, ulong time)
        {
            if (TimeBytes == 4)
                w.Write((UInt32)time);
            else if (TimeBytes == 8)
                w.Write((UInt64)time);
            else
                throw new Exception("Unsupported Time Byte Length!");
        }
        private void Util_WriteValue(BinaryWriter w, double val)
        {
            if (ValueBytes == 4)
                w.Write((float)val);
            else if (ValueBytes == 8)
                w.Write((double)val);
            else
                throw new Exception("Unsupported Value Byte Length!");
        }
        private void Util_WriteHeader(BinaryWriter w, bool locked = false)
        {
            Util_WriteHeader(w, Version, locked, OldestTime, NewestTime, TagCount, RowCount);
        }
        private void Util_WriteHeader(BinaryWriter w, UInt32 version, bool locked, ulong oldestTime, ulong newestTime, UInt32 tagCount, UInt32 rowCount)
        {
            // 4[Version] 1[Locked] 1[TimeLength] 1[ValueLength] 8[OldestTime] 8[NewestTime] 4[TagCount] 4[RowCount]
            w.BaseStream.Position = 0;
            w.Write(version);
            w.Write(locked);
            w.Write((byte)_timeBytes);
            w.Write((byte)_valueBytes);
            w.Write((UInt64)oldestTime);
            w.Write((UInt64)newestTime);
            w.Write(tagCount);
            w.Write(rowCount);
            Debug.Assert(w.BaseStream.Position == HeaderBytes, "Header Size Incorrect!");
        }
        private void Util_WriteTags(BinaryWriter w, List<string> tags)
        {
            foreach (var tag in tags)
                w.Write(tag);
        }

        // ----------------------------------------------------------------------------------------
        // Locking
        private void LockStore()
        {
            FileStream stream = new FileStream(Path, FileMode.Open, FileAccess.Write);
            LockStore(stream);
            stream.Flush();
            stream.Dispose();
        }
        private void LockStore(FileStream stream)
        {
            long origPos = stream.Position;
            stream.Position = 4;
            stream.WriteByte(1);
            stream.Flush(true);
            stream.Position = origPos;
        }
        private void UnlockStore()
        {
            FileStream stream = new FileStream(Path, FileMode.Open, FileAccess.Write);
            UnlockStore(stream);
            stream.Flush();
            stream.Dispose();
        }
        private void UnlockStore(FileStream stream)
        {
            long origPos = stream.Position;
            stream.Position = 4;
            stream.WriteByte(0);
            stream.Flush(true);
            stream.Position = origPos;
        }
        private bool IsStoreLocked()
        {
            using (var stream = new FileStream(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                stream.Position = 4;
                return (stream.ReadByte() == 1);
            }
        }

        // ----------------------------------------------------------------------------------------
        // Querying       
        //public static TagData QueryTag(string path, string tag, ulong start, ulong end)
        //{
        //    // Create the new store so it can be loaded
        //    DataStore store = new DataStore(path, StoreAccess.Query);

        //    // Operation Prevented if Store is Locked against reading
        //    if (store.Locked)
        //        return new TagData() { Name = tag };

        //    // Get column offset
        //    int col = store.TagOrder.IndexOf(tag);
        //    if (col < 0)    // Not found
        //        return new TagData() { Name = tag };

        //    // Gather the locations to read
        //    var offsets = new List<long>();
        //    var times = store.Table.Keys.Where(x => x >= start && x <= end).ToList();
        //    foreach (var time in times)
        //    {
        //        offsets.Add(store.Table[time]);
        //    }

        //    // Read the locations into a tagadata 
        //    TagData data = new TagData
        //    {
        //        Name = tag
        //    };

        //    var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
        //    var reader = new BinaryReader(stream);

        //    foreach (var offset in offsets)
        //    {
        //        // Seek to start position
        //        stream.Position = offset;

        //        // read row's time
        //        ulong time = store.Util_ReadTime(reader);
        //        Debug.Assert(start <= time && time <= end, "Query returned a time outside of query range");

        //        // Seek to column
        //        stream.Position += store.ValueBytes * col;

        //        // Read the value
        //        double val = store.Util_ReadValue(reader);

        //        // Add them to the tag
        //        data.Values.Add(new TagValue(time, val));
        //    }

        //    reader.Dispose();
        //    stream.Dispose();

        //    // Sort by time before we return
        //    data.Values.Sort((x, y) => { return x.Time.CompareTo(y.Time); });

        //    return data;
        //}
        //public static Tuple<ulong, ulong> QueryLatestTimeRange(string path)
        //{
        //    // Create the new store so it can be loaded
        //    // Safe Way
        //    //DataStore store = new DataStore(path, StoreAccess.Query);
        //    //return new Tuple<ulong, ulong>(store.OldestTime, store.NewestTime);

        //    if (!File.Exists(path))
        //        return new Tuple<ulong, ulong>(0, 0);

        //    using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, Block))
        //    {
        //        if (stream.Length >= HeaderBytes)
        //        {
        //            using (var reader = new BinaryReader(stream))
        //            {
        //                // read based on position
        //                // Read Header -- 4[Version] 1[Locked] 1[TimeLength] 1[ValueLength] 8[OldestTime] 8[NewestTime] 4[TagCount] 4[RowCount]
        //                stream.Position = 4 + 1 + 1 + 1;
        //                ulong oldest = reader.ReadUInt64();
        //                ulong newest = reader.ReadUInt64();
        //                return new Tuple<ulong, ulong>(oldest, newest);
        //            }
        //        }
        //        else
        //            return new Tuple<ulong, ulong>(0, 0);
        //    }
        //}
        public bool IsOrdered()
        {
            for (int i = 0; i < Table.Count - 1; i++)
            {
                if (Table.Keys.ElementAt(i) > Table.Keys.ElementAt(i + 1))
                    return false;
            }
            return true;
        }

        // ----------------------------------------------------------------------------------------
        // Misc
        private int TagOrderBytes(List<string> to)
        {
            int size = 0;
            foreach (var s in to)
            {
                size += 1 + s.Length;
            }
            return size;
        }

        // ----------------------------------------------------------------------------------------
        // Debug
        public void ExportToFile(string path, bool exportRows = false)
        {
            var storeStream = new FileStream(Path, FileMode.Open, FileAccess.Read, FileShare.Read, ReadBlock);
            var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write);
            var reader = new BinaryReader(storeStream);
            var writer = new StreamWriter(fileStream);

            // Dump store layout
            writer.WriteLine("-- Header");
            writer.WriteLine("Version: {0} - TimeBytes: {1}b, ValueBytes: {2}b", Version, TimeBytes, ValueBytes);
            writer.WriteLine("Locked: {0}", IsStoreLocked());
            writer.WriteLine("TagCount: {0}", TagCount);
            writer.WriteLine("RowCount: {0}", RowCount);
            fileStream.Flush();
            writer.WriteLine("--");
            writer.WriteLine("-- Calculated ");
            writer.WriteLine("Ordered: {0}", (IsOrdered() ? "yes" : "no"));
            writer.WriteLine("Row Bytes: {0}", RowBytes);
            writer.WriteLine("Block: {0}", Block);
            writer.WriteLine("Read Buffer: {0} bytes, Write Buffer: {1} bytes", ReadBlock, WriteBlock);
            fileStream.Flush();
            writer.WriteLine("--");
            writer.WriteLine("-- Tag Order ");
            writer.WriteLine("  [{0}]", string.Join(", ", TagOrder));
            fileStream.Flush();
            writer.WriteLine("--");
            writer.WriteLine("--Files");
            writer.WriteLine("Count: {0}", Files.Count);
            int col = 0;
            foreach (var f in Files)
            {
                if (col > 1)
                {
                    writer.WriteLine("{0},", f);
                    col = 0;
                }
                else
                {
                    writer.Write("{0}, ", f);
                    col++;
                }
                fileStream.Flush();
            }
            writer.WriteLine("--");
            writer.WriteLine("--Table");
            writer.WriteLine("\tMax Offset: {0}", long.MaxValue);
            fileStream.Flush();
            foreach (var pair in Table)
            {
                storeStream.Position = pair.Value;
                writer.WriteLine("  Time: {0} ({1})\t\tOffset: {2}\t\tOkay: {3}",
                    pair.Key,
                    OracleTime.ToDateTime(pair.Key).ToString("yyyyMMdd-HHmmss"), 
                    pair.Value,
                    Util_ReadTime(reader) == pair.Key);
                fileStream.Flush();
            }

            if (exportRows)
            {
                writer.WriteLine("--");
                writer.WriteLine("--Rows");
                foreach (var offset in Table.Values)
                {
                    storeStream.Position = offset;
                    writer.Write(string.Format("  [ {0:0000000000}: ", Util_ReadTime(reader)));
                    for (int i = 0; i < TagCount; i++)
                        writer.Write(string.Format("{0:00000.0} ", Util_ReadValue(reader)));
                    writer.WriteLine(" ]");
                    fileStream.Flush();
                }
            }            

            writer.Dispose();
            reader.Dispose();
            fileStream.Dispose();
            storeStream.Dispose();
        }

        // ----------------------------------------------------------------------------------------
        // OLD
        //public void UpdateTags(List<string> tags)
        //{
        //    // Operation Prevented if Store is Locked against writing
        //    if (IsStoreLocked())
        //        throw new StoreLockedException();

        //    // Calculate a new table
        //    List<String> newTagOrder = new List<string>(TagOrder);
        //    newTagOrder.AddRange(tags);
        //    long newRowSize = (newTagOrder.Count * 8) + 8;

        //    // increasing the tags increases the row size, which pushes each row futher from each other
        //    // read up to 'Block' rows into memory, then transfer them to their new location,
        //    // starting at the end

        //    // Calculate the new positons to move the rows to
        //    Dictionary<ulong, long> newTable = new Dictionary<ulong, long>((int)RowCount);
        //    long pos = HeaderBytes + TagOrderBytes(newTagOrder);    // Calculate new initial position
        //    var times = Table.Keys.ToList();    // These should be in Row order
        //    for (int i = 0; i < RowCount; i++)
        //    {
        //        newTable.Add(times[i], pos);
        //        pos += newRowSize;
        //    }

        //    // Start the moving
        //    int newColumns = newTagOrder.Count - TagOrder.Count;
        //    int rowsRead = 0;
        //    long rowsMoved = 0;
        //    List<StoreRow> rows = new List<StoreRow>(Block);
        //    var moveTimes = Table.OrderBy(x => x.Value).Select(x => x.Key).ToList();    // List of times sorted by offset

        //    string TempPath = System.IO.Path.ChangeExtension(Path, "~");
        //    FileStream sStream = new FileStream(Path, FileMode.Open, FileAccess.Read);
        //    FileStream nStream = new FileStream(TempPath, FileMode.Create, FileAccess.Write);
        //    BinaryReader reader = new BinaryReader(sStream);
        //    BinaryWriter writer = new BinaryWriter(nStream);

        //    sStream.Position = 0;
        //    nStream.Position = 0;

        //    // -------------------
        //    // Write Header
        //    Util_WriteHeader(writer, Version, true, 0, (uint)newTagOrder.Count, (uint)times.Count);

        //    // -------------------
        //    // Write Tags              
        //    Util_WriteTags(writer, newTagOrder);

        //    // -------------------
        //    // Transfer Rows
        //    while (rowsMoved < RowCount)
        //    {
        //        // Read a block of rows
        //        while (rowsRead < moveTimes.Count && rows.Count < Block)
        //        {
        //            var time = moveTimes[rowsRead];
        //            long readAddress = Table[time];
        //            sStream.Position = readAddress + 8;

        //            StoreRow row = new StoreRow(time);
        //            for (int i = 0; i < TagCount; i++)
        //                row.Values.Add(reader.ReadDouble());
        //            rows.Add(row);
        //            rowsRead++;
        //        }

        //        // Write them
        //        foreach (var row in rows)
        //        {
        //            // Move to new row position
        //            nStream.Position = newTable[row.Time];
        //            // Write the row
        //            writer.Write(row.Time);
        //            foreach (var val in row.Values)
        //                writer.Write(val);
        //            // write the new values as 0's
        //            for (int i = 0; i < newColumns; i++)
        //                writer.Write((double)0);

        //            rowsMoved++;
        //        }
        //        rows.Clear();
        //    }

        //    // All rows Moved, now write the table and count
        //    Table = newTable;
        //    TagOrder = newTagOrder;
        //    TagCount = (uint)newTagOrder.Count;

        //    // Cleanup
        //    reader.Dispose();
        //    writer.Dispose();
        //    sStream.Dispose();
        //    nStream.Dispose();

        //    File.Replace(TempPath, Path, System.IO.Path.ChangeExtension(Path, "t"));
        //    WriteTable(Table);
        //    UnlockStore();
        //}
        //public void UpdateTags_InPlace(List<string> tags)
        //{
        //    // Operation Prevented if Store is Locked against writing
        //    if (IsStoreLocked())
        //        throw new StoreLockedException();

        //    // Calculate a new table
        //    List<String> newTagOrder = new List<string>(TagOrder);
        //    newTagOrder.AddRange(tags);
        //    long newRowSize = (newTagOrder.Count * 8) + 8;

        //    // increasing the tags increases the row size, which pushes each row futher from each other
        //    // read up to 'Block' rows into memory, then transfer them to their new location,
        //    // starting at the end

        //    // Calculate the new positons to move the rows to
        //    Dictionary<ulong, long> newTable = new Dictionary<ulong, long>((int)RowCount);
        //    long pos = HeaderBytes + TagOrderBytes(newTagOrder);    // Calculate new initial position
        //    var times = Table.Keys.ToList();    // These should be in Row order
        //    for (int i = 0; i < RowCount; i++)
        //    {
        //        newTable.Add(times[i], pos);
        //        pos += newRowSize;
        //    }

        //    // Start the moving
        //    int newColumns = newTagOrder.Count - TagOrder.Count;
        //    int rowsRead = 0;
        //    long rowsMoved = 0;
        //    List<StoreRow> rows = new List<StoreRow>(Block);
        //    var moveTimes = Table.OrderBy(x => x.Value).Select(x => x.Key).ToList();    // List of times sorted by offset
        //    moveTimes.Reverse();

        //    FileStream stream = new FileStream(Path, FileMode.Open, FileAccess.ReadWrite);
        //    BinaryReader reader = new BinaryReader(stream);
        //    BinaryWriter writer = new BinaryWriter(stream);

        //    LockStore(stream);

        //    while (rowsMoved < RowCount)
        //    {
        //        // Read a block of rows
        //        while (rowsRead < moveTimes.Count && rows.Count < Block)
        //        {
        //            var time = moveTimes[rowsRead];
        //            long readAddress = Table[time];
        //            stream.Position = readAddress + 8;

        //            StoreRow row = new StoreRow(time);
        //            for (int i = 0; i < TagCount; i++)
        //                row.Values.Add(reader.ReadDouble());
        //            rows.Add(row);
        //            rowsRead++;
        //        }

        //        // Write them
        //        foreach (var row in rows)
        //        {
        //            // Move to new row position
        //            stream.Position = newTable[row.Time];
        //            // Write the row
        //            writer.Write(row.Time);
        //            foreach (var val in row.Values)
        //                writer.Write(val);
        //            // write the new values as 0's
        //            for (int i = 0; i < newColumns; i++)
        //                writer.Write((double)0);

        //            rowsMoved++;
        //        }
        //        rows.Clear();
        //    }

        //    // All rows Moved, now write the table and count
        //    Table = newTable;
        //    TagOrder = newTagOrder;
        //    TagCount = (uint)newTagOrder.Count;

        //    // -------------------
        //    // Write Header Info
        //    stream.Position = 0;
        //    writer.Write((UInt32)TagOrder.Count); // tag count
        //    writer.Write((UInt32)RowCount);     // row count

        //    // -------------------
        //    // Write Tags                    
        //    foreach (var tag in TagOrder)
        //        writer.Write(tag);

        //    UnlockStore(stream);

        //    reader.Dispose();
        //    writer.Dispose();
        //    stream.Dispose();
        //}
        //public void Insert_InPlace(Store store)
        //{
        //    // Operation Prevented if Store is Locked against writing
        //    if (IsStoreLocked())
        //        throw new StoreLockedException();

        //    // Check for new tags before inserting. 
        //    var tags = store.TagOrder.Except(TagOrder).ToList();

        //    // Calculate a new table
        //    List<String> newTagOrder = new List<string>(TagOrder);
        //    newTagOrder.AddRange(tags);
        //    long newRowSize = (newTagOrder.Count * 8) + 8;

        //    // Calculate the new positons to move the rows to, and insert the new rows in order
        //    var times = Table.Keys.ToList();
        //    foreach (var row in store.Rows) // Add new row times
        //    {
        //        if (!times.Contains(row.Time))
        //            times.Add(row.Time);
        //    }
        //    times.Sort();   // Sort the times, since we will be reordering the rows anyway
        //    Dictionary<ulong, long> newTable = new Dictionary<ulong, long>((int)RowCount);
        //    long pos = HeaderBytes + (31 * newTagOrder.Count);    // Calculate new initial position after any added tags            
        //    foreach (var time in times)
        //    {
        //        newTable.Add(time, pos);
        //        pos += newRowSize;
        //    }

        //    // Start the moving
        //    int rowsRead = 0;
        //    long rowsMoved = 0;
        //    List<StoreRow> rows = new List<StoreRow>(Block);
        //    var moveTimes = newTable.Keys.ToList();    // Should already be sorted
        //    moveTimes.Reverse();    // Start with last time, should be largest offset

        //    var changingTags = store.TagOrder.Intersect(TagOrder);  // Tags that exist in both TagOrders are being updated by new data
        //    List<bool> changingTagIndexes = new List<bool>(newTagOrder.Count);
        //    foreach (var t in newTagOrder)
        //    {
        //        changingTagIndexes.Add(
        //            store.TagOrder.Contains(t)
        //        );
        //    }

        //    using (FileStream stream = new FileStream(Path, FileMode.Open, FileAccess.ReadWrite))
        //    {
        //        BinaryReader reader = new BinaryReader(stream);
        //        BinaryWriter writer = new BinaryWriter(stream);

        //        while (rowsMoved < RowCount)
        //        {
        //            // Read a block of rows
        //            while (rowsRead < moveTimes.Count && rows.Count < Block)
        //            {
        //                var time = moveTimes[rowsRead];

        //                // Data exists in datastore
        //                if (Table.ContainsKey(time))
        //                {
        //                    long readAddress = Table[time];
        //                    stream.Position = readAddress + 8;
        //                    StoreRow row = new StoreRow(time);
        //                    for (int i = 0; i < newTagOrder.Count; i++)
        //                    {
        //                        // If this tag is updated / new, read the value from the new data
        //                        var r = store.Rows.Find(x => x.Time == time);
        //                        if (changingTagIndexes[i] &&
        //                            store.Rows.Find(x => x.Time == time) != null)
        //                        {
        //                            var storeRow = store.Rows.Find(y => y.Time == time);
        //                            int x = store.TagOrder.IndexOf(newTagOrder[i]);
        //                            if (x >= 0)
        //                                row.Values.Add(storeRow.Values[x]);
        //                            stream.Position += 8;   // Skip this tag's column
        //                        }
        //                        // If it's not changing, then read the value from the DataStore
        //                        else if (i < TagOrder.Count)
        //                            row.Values.Add(reader.ReadDouble());
        //                        // If it doesn't exist in either, then this is a new value so we should pad with 0
        //                        else
        //                            row.Values.Add(0d);
        //                    }

        //                    rows.Add(row);
        //                }
        //                // Data is from row to be added, doesn't exist in datastore yet
        //                else
        //                {
        //                    var row = new StoreRow(time);
        //                    var toAdd = store.Rows.Find(x => x.Time == time);
        //                    if (toAdd != null)
        //                    {
        //                        // Add each value in the order of the new TagOrder
        //                        foreach (var tag in newTagOrder)
        //                        {
        //                            // If this tag is in new store, take it's value, otherwise pad with 0
        //                            int i = store.TagOrder.IndexOf(tag);
        //                            if (i >= 0)
        //                                row.Values.Add(toAdd.Values[i]);
        //                            else
        //                                row.Values.Add(0d);
        //                        }
        //                    }
        //                    rows.Add(row);
        //                }
        //                rowsRead++;
        //            }

        //            // Write them
        //            foreach (var row in rows)
        //            {
        //                Debug.Assert(row.Values.Count == newTagOrder.Count, "Row doesn't have correct value count when writing");

        //                // Move to new row position
        //                stream.Position = newTable[row.Time];
        //                // Write the row
        //                writer.Write(row.Time);
        //                foreach (var val in row.Values)
        //                    writer.Write(val);
        //                // write any remaining values as 0's
        //                for (int i = row.Values.Count; i < newTagOrder.Count; i++)
        //                    writer.Write((double)0);

        //                rowsMoved++;
        //            }
        //            rows.Clear();
        //        }

        //        // All rows Moved, now write the table and count
        //        Table = newTable;
        //        TagOrder = newTagOrder;
        //        TagCount = (uint)newTagOrder.Count;
        //        RowCount = (uint)newTable.Count;

        //        // -------------------
        //        // Write Header Info
        //        stream.Position = 0;
        //        writer.Write((UInt32)TagCount);     // tag count
        //        writer.Write((UInt32)RowCount);     // row count

        //        // -------------------
        //        // Write Tags                    
        //        foreach (var tag in TagOrder)
        //            writer.Write(tag);

        //        reader.Dispose();
        //        writer.Dispose();
        //    }
        //}

        public class StoreLockedException : Exception
        {
            public StoreLockedException()
                : base("Store is locked!")
            {
            }

            public StoreLockedException(string message)
                : base(message)
            {
            }

            public StoreLockedException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
    }
}
