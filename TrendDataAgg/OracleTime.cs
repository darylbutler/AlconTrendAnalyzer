﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendDataAgg
{
    public class OracleTime
    {
        public static ulong ToLong(DateTime time)
        {
            //return (long)(time - (new DateTime(1980, 01, 1, 0, 0, 00))).TotalSeconds;
            return (ulong)(time - new DateTime(1980, 1, 1, 1, 59, 50)).TotalSeconds;
        }
        public static DateTime ToDateTime(ulong time)
        {
            var start = new DateTime(1980, 01, 1, 1, 59, 50);
            if ((DateTime.MaxValue - start).TotalSeconds < time)
                return DateTime.MaxValue;
            
            return new DateTime(1980, 01, 1, 1, 59, 50).AddSeconds(time);           
        }
    }
}
