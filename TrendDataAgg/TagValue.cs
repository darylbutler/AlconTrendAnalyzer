﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrendDataAgg
{
    [DebuggerDisplay("{DebuggerDisplay,nq}")]
    public class TagValue
	{
		public ulong Time;
		public double Value;

		public TagValue(ulong time, double val)
		{
			Time = time;
			Value = val;
		}

		public DateTime DateTime
		{
			get
			{
                return OracleTime.ToDateTime(Time);
			}
		}

        public override String ToString()
        {
            return string.Format("({0}: {1})", Time, Value);
        }
        private string DebuggerDisplay
        {
            get { return ToString(); }
        }
    }
}
