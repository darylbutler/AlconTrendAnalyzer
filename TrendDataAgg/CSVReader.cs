﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Globalization;
using System.IO.Compression;

namespace TrendDataAgg
{
	public class CSVReader
	{
        private const int ReadBlockBufferSize = 4096;
        private const int WriteBlockBufferSize = 4096;
        private static bool CharIsSeperator(char chr)
        {
            return chr == '\n' || chr == '\r' || chr == ' ' || chr == ',' || chr == ';';
        }

        // Perferred Way to load a CSV
        public static List<TagData> LoadCSV(string path)
        {
            // Convert this mess into readable data
            if (!File.Exists(path))
                return null;

            if (new FileInfo(path).Length < 1524)
                throw new CSVParseException("File too short.");

            List<TagData> tags = new List<TagData>();
            StringBuilder value = new StringBuilder();
            int tagCnt = -1, curCol = -1;
            ulong secTime = 0;

            // Open file
            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    // Read until character	
                    while (CharIsSeperator((char)reader.Peek()) && !reader.EndOfStream)
                        reader.Read();

                    // Start the Loop
                    while (!reader.EndOfStream)
                    {
                        // Grab the current Character
                        char Character = (char)reader.Read();

                        if (CharIsSeperator(Character))
                        {
                            // End of string, beginning of seperator
                            // So save the string and reset its value

                            // Throw away string with just hyphens (Header underlines in a csv)
                            if (value.Length > 1 && value[0] == '-' && value[1] == '-')
                                value = new StringBuilder();
                            else if (value.Length > 0)
                            {
                                // see if in tags section
                                if (tagCnt < 0)
                                {
                                    if (value[0] != 'A' && value[0] != 'a' &&   // tags begin w/'APL'
                                        value[0] != 'S' && value[0] != 's' &&   // SECTIME exemption)
                                        ulong.TryParse(value.ToString(), out ulong val))
                                    {
                                        // tags ended, values start
                                        tagCnt = tags.Count;
                                        curCol = 1;
                                        secTime = val;
                                    }
                                    else if (value[0] == 'A' || value[0] == 'a')
                                    {
                                        var tag = new TagData
                                        {
                                            Name = value.ToString()
                                        };

                                        if (string.IsNullOrEmpty(tag.Description))
                                            tag.Description = tag.Name;

                                        tags.Add(tag);
                                    }
                                    else if (value.ToString() != "SECTIME")
                                        throw new CSVParseException("Tag doesn't begin with 'A' or time cannot be parsed.");
                                }
                                else  // Value
                                {
                                    if (curCol == 0)
                                    {
                                        if (!ulong.TryParse(value.ToString(), out ulong val))
                                            throw new CSVParseException("Time value could not be Parsed.");
                                        else
                                            secTime = val;

                                        if (secTime < 1000000000)
                                        {
                                            string rest = reader.ReadToEnd();
                                            if (secTime == (ulong)tags.First().Values.Count && rest == "rows selected.\r\n\r\n")
                                                break;
                                            else
                                                throw new CSVParseException("Malformed CSV!");
                                        }
                                    }
                                    else
                                    {
                                        // Tag Value
                                        if (!double.TryParse(value.ToString(), out double parsedValue))
                                            throw new CSVParseException("Value could not be Parsed.");
                                        
                                        var val = new TagValue(
                                            secTime,
                                            parsedValue);
                                        tags[curCol - 1].Values.Add(val);
                                    }

                                    // Move to correct column
                                    if (curCol >= tagCnt)
                                        curCol = 0;     // Start over
                                    else
                                        curCol++;       // Move to next column
                                }

                                // Clear the string
                                value = new StringBuilder();
                            }
                        }
                        else
                        {
                            // Save the character to the string
                            value.Append(Character);
                        }
                    }
                }
            }

            // return unique tags
            return tags.GroupBy(x => x.Name).Select(x => x.FirstOrDefault()).ToList();
        }
        public static Store ParseCSV(string path2)
        {
            string path = UNCPathConverter.GetUNCPath(path2);
            if (!File.Exists(path))
                throw new CSVParseException("File not found.");

            if (new FileInfo(path).Length < 1524)
                throw new CSVParseException("File too short.");

            if (!path2.ToLower().EndsWith("csv"))
                throw new CSVParseException("Not a CSV!");

            // Open file
            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, ReadBlockBufferSize))
            {
                return ParseCSV(stream);
            }
        }
        public static Store ParseCSV(Stream stream)
        {
            // Map between a tag's index in tag list and the csv column (If we insert a -1, the column is skipped)
            List<bool> col_to_tag = new List<bool>(200);
            // How we build the token
            StringBuilder value = new StringBuilder();
            int tagCnt = -1, curCol = -1;
            bool tagsLoaded = false;

            Store store = new Store();
            StoreRow row = null;

            using (StreamReader reader = new StreamReader(stream, Encoding.Default, false, ReadBlockBufferSize, true)) {
                // Read until a character (Skip the beginning whitespace)	
                while (CharIsSeperator((char)reader.Peek()) && !reader.EndOfStream)
                    reader.Read();

                // Start the Loop
                while (!reader.EndOfStream) {
                    // Grab the current Character
                    char Character = (char)reader.Read();

                    if (!CharIsSeperator(Character)) {
                        // Save the character to the string
                        value.Append(Character);
                    }
                    else {
                        // End of string, beginning of seperator
                        // So save the string and reset its value

                        // Throw away string with just hyphens (Header underlines in a csv)
                        if (value.Length > 1 && value[0] == '-' && value[1] == '-')
                            value = new StringBuilder();
                        else if (value.Length > 0) {
                            // We've moved to another token
                            curCol++;

                            // see if in tags section
                            if (!tagsLoaded) {
                                if (curCol == 0 && value.ToString() != "SECTIME") {
                                    // Bugfix for the crap csvs I generated. Forgot the preceeding SECTIME header
                                    col_to_tag.Add(false);
                                    curCol++;
                                }

                                if (value[0] != 'A' && value[0] != 'a' && // tags begin w/'APL'
                                    value[0] != 'S' && value[0] != 's')   // SECTIME exemption
                                {
                                    // tags ended, values start
                                    tagsLoaded = true;
                                    tagCnt = curCol - 1;
                                    curCol = 0; // This is a sec time value
                                    if (!ulong.TryParse(value.ToString(), out ulong t))
                                        throw new CSVParseException(string.Format("Cannot parse time! Col: 0, value: {0}.", value.ToString()));

                                    row = new StoreRow(t);
                                    Debug.Assert(tagCnt == (col_to_tag.Count - 1), "Count of tags vs col_to_tag mapping differs");
                                }
                                else if (value.ToString() != "SECTIME") {
                                    // check for duplicates
                                    if (store.TagOrder.Contains(value.ToString())) {
                                        col_to_tag.Add(false);
                                    }
                                    else {
                                        // Map this value
                                        col_to_tag.Add(true);
                                        store.TagOrder.Add(value.ToString());
                                    }
                                    Debug.Assert(curCol == (col_to_tag.Count - 1), "Skipped different # of cols than current column.");
                                }
                                else {
                                    // We're skipping this tag because we don't know wtf it is
                                    col_to_tag.Add(false);
                                    Debug.Assert(curCol == 0, "SECTIME column stored not as 0 col");
                                }
                            }
                            else  // Value
                            {
                                if (curCol == 0) {
                                    // Start a new Row
                                    if (!ulong.TryParse(value.ToString(), out ulong t))
                                        throw new CSVParseException(string.Format("Time could not be parsed. Col: 0, Value: {0}.", value.ToString()));
                                    row = new StoreRow(t);

                                    // Make sure we don't include the shitty summary at the end
                                    if (row.Time < 10000000) {
                                        string rest = reader.ReadToEnd();
                                        if (rest == "rows selected.\r\n\r\n") {
                                            break;
                                        }
                                        else
                                            throw new CSVParseException("Malformed CSV!");
                                    }
                                }
                                // If this is a value we're keeping...
                                else if (col_to_tag[curCol]) {
                                    if (!double.TryParse(value.ToString(), out double d))
                                        throw new CSVParseException(string.Format("Could not parse value. Col: {0}, value: {1}.", curCol, value.ToString()));
                                    row.Values.Add(d);
                                }

                                if (curCol >= tagCnt) {
                                    // End of row
                                    curCol = -1;

                                    // -- DEBUG error
                                    Debug.Assert(row.Values.Count == store.TagOrder.Count, "Error in CSV, row's value count and header count differ!");

                                    // Add the row if it is valid
                                    if (row.Time > 100 && row.Values.Count == store.TagOrder.Count)
                                        store.Rows.Add(row);
                                }

                            }

                            // Clear the string
                            value = new StringBuilder();
                        }
                    }
                }
            }

            return store;
        }
        public static Store ParseTXT(string path2, DataStore dStore = null)
        {
            string path = UNCPathConverter.GetUNCPath(path2);

            // Convert this mess into readable data
            if (!File.Exists(path))
                return null;

            // Return CSV File struct
            Store store = new Store();

            /*
             * As of the time of this writing, the 7+ text files are in this format:
             * [Tag.Name].ActualValue;[Tag.Description];[Value];[Units (Farenheight/mm/Celcius/seconds/etc)];SPONT (???);[Date (M/dd/yyyy)];[Time (hh:mm:ss am/pm)].0 
             * 
             * Notes:
             *  - Data is semicolon separated (But we will assume anything)
             *  - Tags are in order, (Rows of Tag1's value over time, then rows of tag2's value over time)
             *  - Drop the '.ActualValue' from the Tag.Name
             *  - Drop the '.0' from the time string
             */

            var file = File.ReadAllLines(path).ToList();

            // Sort text file because I can't get one good clean csv file to save my life
            file.Sort(CompareTXTLines);

            // Parse the sorted file
            int timeIndex = 0;
            foreach (string line in file)
            {
                var data = line.Split(',', ';');
                string name = data[0].Substring(0, data[0].IndexOf('.')).Replace("female", "FC").Replace("male", "BC");

                // If tag is new (Must not be in the list already)
                if (!store.TagOrder.Contains(name))
                {
                    store.TagOrder.Add(name);
                    timeIndex = 0;

                    // Check if we have this description. If not, add it
                    if (dStore != null && !dStore.TagDescriptions.ContainsKey(name))
                        dStore.TagDescriptions.Add(name, data[1]);
                }

                // Value time
                ulong time = OracleTime.ToLong(DateTime.Parse(string.Format("{0} {1}", data[5], data[6].Substring(0, data[6].IndexOf('.')))));
                double value = double.Parse(data[2]);

                if (timeIndex >= store.Rows.Count)
                {
                    StoreRow row = new StoreRow(time);
                    row.Values = new List<double>() { value };
                    store.Rows.Add(row);
                }
                else
                {
                    // Testing
                    if (store.Rows[timeIndex].Time != time)
                        throw new Exception("Malformed Text Format!");

                    store.Rows[timeIndex].Values.Add(value);
                }

                timeIndex++; // Move to next row
            }

            return store;
        }
        private static int CompareTXTLines(string left, string right)
        {
            //string keyLeft = left.Substring(0, left.IndexOf(':'));
            //string keyRight = right.Substring(0, right.IndexOf(':'));

            var data = left.Split(',', ';');
            string keyLeft = string.Concat(data[0], ";", data[5], ";", data[6]);
            data = right.Split(',', ';');
            string keyRight = string.Concat(data[0], ";", data[5], ";", data[6]);

            return string.Compare(keyLeft, keyRight);
        }
        public static void WriteCSV(string path, Store store)
        {
            // Generate a filename
            // pattern: 2017-12-13_0700-trendata-apl02.csv
            var lastTime = OracleTime.ToDateTime(store.Rows.Max(x => x.Time));
            string APL = "apl0" + store.TagOrder[0][4];
            string file = string.Format("{0}-trendata-{1}.csv",
                lastTime.ToString("yyyy-MM-dd_HHmm"),
                APL);

            string filePath = Path.Combine(path, file);
            if (File.Exists(filePath))
                throw new ArgumentException("Supplied store's generated file name / path already exist!");

            using (FileStream stream = new FileStream(filePath, FileMode.CreateNew, 
                FileAccess.Write, FileShare.None, WriteBlockBufferSize))
            {
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    WriteCSV(store, writer);
                }
            }
        }
        public static void WriteCSV(Store store, StreamWriter writer)
        {
            // Write the tag string
            writer.Write("SECTIME,");
            writer.Write(string.Join(",", store.TagOrder));
            writer.WriteLine();

            // Write each row
            foreach (var row in store.Rows)
            {
                writer.Write("{0},", row.Time);
                writer.WriteLine(string.Join(",", row.Values));
            }
        }

        // TODO After obsoletion, delete
        /*public static void StoreCSVInWeeklyZip(string path, Store toSave)
        {
            // The zip's file name is based on the week of this data's time
            // CSVs name based on last timeframe as well
            var lastTime = OracleTime.ToDateTime(toSave.Rows.Max(x => x.Time));
            string APL = string.Format("apl0{0}", toSave.TagOrder[0][4]);
            string CSVFileName = string.Format("{0}_{1:00}00-trendata-{2}.csv",
                lastTime.ToString("yyyy-MM-dd"),
                lastTime.Minute >= 30 ? lastTime.Hour + 1 : lastTime.Hour,
                APLZipStringFromTagName(toSave.TagOrder[0]));
            string zipPath = string.Format("Archive_Year{0:0000}_Week{1:00}_{2}.zip",
                lastTime.Year,
                GetIso8601WeekOfYear(lastTime),
                APL.ToUpper());

            // Open the zip file
            Store store = null;
            zipPath = Path.Combine(path, zipPath);
            using (FileStream zip = new FileStream(zipPath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                using (ZipArchive archive = new ZipArchive(zip, ZipArchiveMode.Update))
                {
                    // If file already exists in archive, merge with it instead of creating two
                    var existing = archive.GetEntry(CSVFileName);
                    Stream EntryStream = null;
                    int i = 0;
                    while (archive.GetEntry(CSVFileName) != null) {                         
                        CSVFileName = string.Format("{0}_{1:00}00-trendata-{2}-{3}.csv",
                                        lastTime.ToString("yyyy-MM-dd"),
                                        lastTime.Minute >= 30 ? lastTime.Hour + 1 : lastTime.Hour,
                                        APLZipStringFromTagName(toSave.TagOrder[0]),
                                        i++);
                    }

                    // Write the data
                    var newCSVEntry = archive.CreateEntry(CSVFileName, CompressionLevel.Optimal);
                    EntryStream = newCSVEntry.Open();
                    using (StreamWriter writer = new StreamWriter(EntryStream))
                    {
                        WriteCSV(store, writer);
                    }
                }
            }
        }*/
        public static void StoreCSVInWeeklyZip(string ZipPath, String CsvName, Store toSave) {
            // The zip's file name is based on the week of this data's time
            // CSVs name based on last timeframe as well
            var lastTime = OracleTime.ToDateTime(toSave.Rows.Max(x => x.Time));
            var CSVFileName = string.Empty;
            string APL = string.Format("apl0{0}", toSave.TagOrder[0][4]);

            // Zip Path 
            string zipPath = string.Format("Archive_Year{0:0000}_Week{1:00}_{2}.zip",
                lastTime.Year,
                GetIso8601WeekOfYear(lastTime),
                APL.ToUpper());
            // CSV Name
            if (toSave.TagOrder[0][4] == '1' || toSave.TagOrder[0][4] == '2') {  // 6- (Filename pattern has the date already in it)
                var firstTime = OracleTime.ToDateTime(toSave.Rows.Min(x => x.Time));
                CSVFileName = string.Format("{0}.csv", CsvName.ToUpper());   // Use the CSVName SCADA already gave us
            } else {    // 7+, generate a name
                CSVFileName = string.Format("{0}_{1:00}00-{2}-trendata-{3}.csv",
                                                lastTime.ToString("yyyy-MM-dd"),
                                                lastTime.Minute >= 30 ? lastTime.Hour + 1 : lastTime.Hour,
                                                toSave.TagOrder[0].Substring(0, 11), 
                                                CsvName.ToUpper());   // Use the CSVName SCADA already gave us
            }
            
            // Open the zip file
            zipPath = Path.Combine(ZipPath, zipPath);
            using (FileStream zip = new FileStream(zipPath, FileMode.OpenOrCreate, FileAccess.ReadWrite)) {
                using (ZipArchive archive = new ZipArchive(zip, ZipArchiveMode.Update)) {
                    // Replace this file if it already exists, else may a new one
                    ZipArchiveEntry newCSVEntry = archive.GetEntry(CSVFileName);
                    if  (newCSVEntry == null) {
                        newCSVEntry = archive.CreateEntry(CSVFileName, CompressionLevel.Optimal);
                    }

                    // Write the data                   
                    Stream EntryStream = newCSVEntry.Open();
                    using (StreamWriter writer = new StreamWriter(EntryStream)) {
                        WriteCSV(toSave, writer);
                    }
                }
            }
        }
        private static string APLZipStringFromTagName(string tag)
        {
            if (tag[4] == '1')
                return "apl01";
            if (tag[4] == '2')
                return "apl02";

            // 7+, store by machine
            return string.Format("apl0{0}-{1}", tag[4], tag.Substring(6, 5));
        }

        // djb: Thanks StackOverflow User!
        // djb: https://stackoverflow.com/questions/11154673/get-the-correct-week-number-of-a-given-date
        // This presumes that weeks start with Monday.
        // Week 1 is the 1st week of the year with a Thursday in it.
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        /// <summary>
        /// Parses a 7+ Data file (.txt) into the Tag Data structure incrementally. Somehow, this is slower.
        /// </summary>
        /// <param name="path2">Regular path to file to parse</param>
        /// <returns>CSV converted into List of TagData's holding TagValues</returns>
        public static List<TagData> LoadDataTXT_slow(string path2)
        {
            string path = UNCPathConverter.GetUNCPath(path2);
            // Convert this mess into readable data
            if (!File.Exists(path))
                return null;

            // Return Tag Collection
            List<TagData> tags = new List<TagData>();

            // How we build the token
            StringBuilder value = new StringBuilder();

            // Current Column counter -- Note, not columns of data, but columns of separated values
            int curCol = 0;

            // Tag data and value built row by row
            TagData tag = null;
            string date = string.Empty;
            double val = -1;

            /*
             * As of the time of this writing, the 7+ text files are in this format:
             * [Tag.Name].ActualValue;[Tag.Description];[Value];[Units (Farenheight/mm/Celcius/seconds/etc)];SPONT (???);[Date (M/dd/yyyy)];[Time (hh:mm:ss am/pm)].0 
             * 
             * Notes:
             *  - Data is semicolon separated (But we will assume anything)
             *  - Tags are in order, (Rows of Tag1's value over time, then rows of tag2's value over time)
             *  - Drop the '.ActualValue' from the Tag.Name
             *  - Drop the '.0' from the time string
             */
            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    // Read until a character (Skip the beginning whitespace)	
                    while (CharIsSeperator((char)reader.Peek()) && !reader.EndOfStream)
                        reader.Read();

                    // Start the Loop
                    while (!reader.EndOfStream)
                    {
                        // Grab the current Character
                        char Character = (char)reader.Read();

                        if (Character == ',' || Character == ';' || Character == '\n' || Character == '\r')
                        {
                            if (value.Length < 1)
                                continue;

                            // Change based on value
                            switch (curCol)
                            {
                                case 0:     // Tag Name
                                    // Remove the '.ActualValue'
                                    string name = value.ToString();
                                    if (name.Contains(".")) // TODO tag names should always have a period, there's no use in checking, but it helps keep errors away
                                        name = name.Substring(0, name.IndexOf('.'));
                                    // Check if we're adding this tag already
                                    if (tag == null || tag.Name != name)
                                    {
                                        // Save the last tag in to list
                                        if (tag != null)
                                            tags.Add(tag);

                                        // Start a new tag with this name
                                        tag = new TagData()
                                        {
                                            Name = name
                                        };
                                    }

                                    // Move to the next column
                                    curCol = 1;
                                    break;
                                case 1:     // Tag Description
                                    tag.Description = value.ToString(); 
                                    // Move to the next column
                                    curCol = 2;
                                    break;
                                case 2:     // Tag Value
                                    val = double.Parse(value.ToString());
                                    curCol = 3;
                                    break;
                                case 5:     // Value Date
                                    date = value.ToString();
                                    curCol = 6;
                                    break;
                                case 6:     // Value Time
                                    string time = value.ToString();
                                    // Remove the '.0' !!
                                    if (time.EndsWith(".0"))
                                        time = time.Substring(0, time.IndexOf('.'));
                                    // Add the timestamp to the end of the date string and convert it to a datetime
                                    DateTime dt = DateTime.Parse(string.Format("{0} {1}", date, time));
                                    tag.Values.Add(new TagValue(OracleTime.ToLong(dt), val));
                                    curCol = 0; // Reset curcol to 0 because we're on the last column here
                                    break;
                                default:
                                    curCol++;
                                    break;
                            }
                            value = new StringBuilder();
                        }
                        else
                        {
                            // Save the character to the string
                            value.Append(Character);
                        }
                    }

                    // -- Add the tag we've been working on
                    if (tag != null && tag.Name != string.Empty && tag.Values.Count > 0)
                        tags.Add(tag);
                }                
            }
            return tags;
        }
        /// <summary>
        /// Parses a 7+ Data file (.txt) into the Tag Data structure via built in methods.
        /// </summary>
        /// <param name="path2">Regular path to file to parse</param>
        /// <returns>CSV converted into List of TagData's holding TagValues</returns>
        public static List<TagData> LoadDataTXT(string path2)
        {
            string path = UNCPathConverter.GetUNCPath(path2);
            // Convert this mess into readable data
            if (!File.Exists(path))
                return null;

            // Return Tag Collection
            List<TagData> tags = new List<TagData>();

            /*
             * As of the time of this writing, the 7+ text files are in this format:
             * [Tag.Name].ActualValue;[Tag.Description];[Value];[Units (Farenheight/mm/Celcius/seconds/etc)];SPONT (???);[Date (M/dd/yyyy)];[Time (hh:mm:ss am/pm)].0 
             * 
             * Notes:
             *  - Data is semicolon separated (But we will assume anything)
             *  - Tags are in order, (Rows of Tag1's value over time, then rows of tag2's value over time)
             *  - Drop the '.ActualValue' from the Tag.Name
             *  - Drop the '.0' from the time string
             */

            var file = File.ReadAllLines(path);
            foreach (string line in file)
            {
                var data = line.Split(',', ';');
                string name = data[0].Substring(0, data[0].IndexOf('.')).Replace("female", "FC").Replace("male", "BC");

                if (tags.Count < 1 || tags.Last().Name != name)
                {
                    TagData tag = new TagData()
                    {
                        Name = name,
                        Description = data[1]
                    };
                    tags.Add(tag);
                }

                // Create the tag value
                tags.Last().Values.Add(new TagValue(
                    OracleTime.ToLong(DateTime.Parse(string.Format("{0} {1}", data[5], data[6].Substring(0, data[6].IndexOf('.'))))), 
                    double.Parse(data[2])));
            }

            return tags;
        }

        #region -- Tag Descriptions ----------------------------------------------------------------------------------
        private static Dictionary<string, string> TagToDesc;
        public static void LoadTagDescriptions()
        {
            TagToDesc = new Dictionary<string, string>();
            string[] lines = Properties.Resources.tag_to_desc.Split('\n');

            foreach (var line in lines)
            {
                var split = line.Replace("\r", "").Split(',');

                if (string.IsNullOrWhiteSpace(split[0]) || string.IsNullOrWhiteSpace(split[1]) ||
                    (split[0] == "Tag key" && split[1] == "Description"))
                    continue;

                if (!TagToDesc.ContainsKey(split[0].ToUpper()))
                    TagToDesc.Add(split[0].ToUpper(), split[1]);
            }
        }
        public static string GetTagDescription(string tagName)
        {
            if (TagToDesc == null || TagToDesc.Count < 1)
                LoadTagDescriptions();

            string token = string.Empty;

            if (tagName.Contains("PP1T1") || tagName.Contains("PP1T2"))
            {
                token = string.Format("PP_{0}", tagName.Substring(12, tagName.Length - 16)).ToUpper();
            }
            else if (tagName.Contains("DS"))
            {
                token = tagName.Substring(9, tagName.Length - 13).ToUpper();
            }
            else
                token = tagName.Substring(9, tagName.Length - 13).ToUpper();            

            if (TagToDesc.ContainsKey(token))
                return TagToDesc[token];
            else
                return string.Empty;
        }
        #endregion

        public class CSVParseException : Exception
        {
            public CSVParseException()
            {
            }

            public CSVParseException(string message)
                : base("CSV Parse Error: " + message)
            {
            }

            public CSVParseException(string message, Exception inner)
                : base("CSV Parse Error: " + message, inner)
            {
            }
        }    
    }
}
