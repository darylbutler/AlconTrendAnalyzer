﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite;
using System.Diagnostics;

namespace TrendDataAgg
{
    public class Database
    {
        // Constants        
        private const uint TableCount = 4;
        public static uint RetainMonths = 1;  // Months of data to keep in database
        public static string DatabasePath = "database.db";

        // delegates
        public delegate void LogActivity(string message);
        public static LogActivity LogMsg;

        // Private Members
        public static SQLiteConnection DatabaseConnection;

        #region -- Database Maintenance ------------------------------------------------------------------------------------
        public static bool CheckDatabase()
        {
            if (File.Exists(DatabasePath))
            {
                // Count the tables
                try
                {
                    SQLiteConnection connection = Connect();

                    using (SQLiteCommand cmd = new SQLiteCommand(connection))
                    {
                        cmd.CommandText = "SELECT count(*) FROM sqlite_master WHERE type = 'table';";
                        cmd.CommandType = System.Data.CommandType.Text;

                        return (int)cmd.ExecuteScalar() == TableCount;
                    }
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            else
            {
                CreateDatabase();
                return true;
            }
        }
        private static SQLiteConnection Connect()
        {
            if (DatabaseConnection == null || DatabaseConnection.State != System.Data.ConnectionState.Open)
            {
                // Load the database
                SQLiteConnectionStringBuilder conn = new SQLiteConnectionStringBuilder();
                conn.DataSource = DatabasePath;
                conn.Version = 3;

                // Start Open off new Database file
                DatabaseConnection = new SQLiteConnection(conn.ConnectionString, true);
                DatabaseConnection.Open();

                // djb -- Use Full sync because mes server kicks people off
                // Turn off waiting for Data to Hit HD before continuing to write
                //using (SQLiteCommand cmd = new SQLiteCommand(DatabaseConnection))
                //{
                //    cmd.CommandText = "PRAGMA synchronous = NORMAL;";
                //    cmd.ExecuteNonQuery();
                //}
            }

            return DatabaseConnection;
        }
        public static void CreateDatabase(SQLiteConnection conn = null)
        {
            // Default value is to recreate the default database
            if (conn == null)
                conn = Connect();

            StringBuilder sb = new StringBuilder();
            sb.Append(
                @"
				BEGIN TRANSACTION;
                DROP TABLE IF EXISTS `admin`;
				DROP TABLE IF EXISTS `tags`;
				DROP TABLE IF EXISTS `data`;
				DROP TABLE IF EXISTS `files`;
                DROP TABLE IF EXISTS `data`;
				DROP TRIGGER IF EXISTS `addFile`;

                CREATE TABLE `admin` (
                    `key`       TEXT NOT NULL PRIMARY KEY,
                    `val`       TEXT
                );

				CREATE TABLE `tags` (
					`id`	    INTEGER NOT NULL PRIMARY KEY,
					`name`	    TEXT NOT NULL UNIQUE,
					`desc`	    TEXT
				);

                CREATE TABLE `files` (
					`path`		TEXT PRIMARY KEY,
					`added`		TEXT
				);
				CREATE TRIGGER `addFile` AFTER INSERT ON `files` 
				BEGIN
					UPDATE `files` SET `added`=(SELECT DATETIME('now')) WHERE `path`=new.`path`;
				END;

				CREATE TABLE `data` (
					`time`		INTEGER NOT NULL PRIMARY KEY UNIQUE	
				);
				
				COMMIT;");
            ;

            // Create a brand new database file			 
            if (!File.Exists(DatabasePath))
            {
                SQLiteConnection.CreateFile(DatabasePath);
            }

            using (SQLiteCommand cmd = conn.CreateCommand())
            {
                // Create the tables
                cmd.CommandText = sb.ToString();
                cmd.ExecuteNonQuery();
            }
        }
        public static void DropOldRecords()
        {
#if DEBUG
            return;
#endif
            // Deletes old values from database
            DateTime RetainDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddMonths((int)RetainMonths * -1);
            ulong time = OracleTime.ToLong(RetainDate);
            ulong now = OracleTime.ToLong(DateTime.Now);

            // Connect and drop the rows
            SQLiteConnection connection = Connect();
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "DELETE FROM data WHERE data.time < @time;VACUUM;";
                cmd.Parameters.AddWithValue("@time", time);
                cmd.CommandType = System.Data.CommandType.Text;
                
                cmd.ExecuteNonQuery();
            }          
        }
        #endregion

        #region -- Tags ----------------------------------------------------------------------------------------------------
        public static List<TagData> GetAllTags()
        {
            List<TagData> ret = new List<TagData>();

            SQLiteConnection connection = Connect();

            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "SELECT * FROM `tags`;";
                cmd.CommandType = System.Data.CommandType.Text;

                using (var rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        TagData tag = new TagData()
                        {
                            ID = (long)rdr["id"],
                            Name = (string)rdr["name"],
                            Description = (string)rdr["desc"]
                        };
                        ret.Add(tag);
                    }
                }
            }
            return ret;
        }
        public static List<string> GetAllTagNames()
        {
            List<string> ret = new List<string>();

            SQLiteConnection connection = Connect();

            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "SELECT * FROM `tags`;";
                cmd.CommandType = System.Data.CommandType.Text;

                using (var rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {                        
                        ret.Add((string)rdr["name"]);
                    }
                }
            }
            return ret;
        }
        public static void AddNewTag(TagData tag)
        {
            if (tag == null || string.IsNullOrWhiteSpace(tag.Name))
                return;

            // Ensure tag has a description
            if (string.IsNullOrWhiteSpace(tag.Description))
                tag.Description = CSVReader.GetTagDescription(tag.Name);

            // Do the insert
            SQLiteConnection connection = Connect();
            using (SQLiteCommand cmd = connection.CreateCommand())
            {
                // Add tag to column
                cmd.CommandText = string.Format("ALTER TABLE `data` ADD COLUMN {0} REAL;",
                    tag.Name);
                cmd.ExecuteNonQuery();

                // Add tag to tags table
                cmd.CommandText = "INSERT INTO `tags` (`name`, `desc`) VALUES(@name, @desc);";
                cmd.Parameters.AddWithValue("@name", tag.Name);
                cmd.Parameters.AddWithValue("@desc", tag.Description);

                cmd.ExecuteNonQuery();
            }
        }

        #endregion

        #region -- Update with new Data ------------------------------------------------------------------------------------
        //public static long UpdateFromData_old(List<TagData> data)
        //{
        //    if (data.Count < 1)
        //        return 0;

        //    long AddedValuesCount = 0;
        //    List<TagData> ExistingTags = GetAllTags();

        //    // Start the transaction
        //    SQLiteConnection connection = Connect();

        //    // Ensure all tags exist
        //    foreach (var tag in data)
        //    {
        //        if (ExistingTags.Find((t) => t.Name == tag.Name) == null)
        //            AddNewTag(tag);
        //    }

        //    // Start the bulk insert
        //    using (SQLiteTransaction transaction = connection.BeginTransaction())
        //    {
        //        using (SQLiteCommand cmd = new SQLiteCommand(connection))
        //        {
        //            cmd.CommandType = System.Data.CommandType.Text;
        //            cmd.Transaction = transaction;

        //            // Add tags and values
        //            foreach (var tag in data)
        //            {                       
        //                // Add the data
        //                foreach (var value in tag.Values)
        //                {
        //                    //cmd.Reset(); // TODO Do we need to call this? Test without

        //                    // Try to update time row first
        //                    cmd.CommandText = String.Format("UPDATE `data` SET `{0}`= @value WHERE time = @time;", tag.Name);
        //                    cmd.Parameters.AddWithValue("@time", value.Time);
        //                    cmd.Parameters.AddWithValue("@value", value.Value);
        //                    cmd.ExecuteNonQuery();
                            
        //                    // If the update didn't work, do an insert
        //                    if (connection.Changes < 1)
        //                    {
        //                        cmd.CommandText = String.Format("INSERT INTO `data` (`time`, `{0}`) VALUES(@time, @value);", tag.Name);
        //                        cmd.ExecuteNonQuery();
        //                    }

        //                    AddedValuesCount++;
        //                }
        //            }
        //        }

        //        // Commit the transaction
        //        transaction.Commit();
        //        return AddedValuesCount;
        //    }
        //}
        //public static long UpdateFromData(List<TagData> data)
        //{
        //    if (data.Count < 1)
        //        return 0;

        //    List<string> ExistingTags = GetAllTagNames();

        //    // Start the transaction
        //    SQLiteConnection connection = Connect();

        //    // Ensure all tags exist
        //    foreach (var tag in data)
        //    {
        //        if (!ExistingTags.Contains(tag.Name))
        //            AddNewTag(tag);
        //    }

        //    // Start the bulk insert
        //    using (SQLiteTransaction transaction = connection.BeginTransaction())
        //    {
        //        using (SQLiteCommand cmd = new SQLiteCommand(connection))
        //        {
        //            cmd.CommandType = System.Data.CommandType.Text;
        //            cmd.Transaction = transaction;

        //            // Add tags and values
        //            // -- Assuming all values have the same time infos

        //            //var times = data.First().Values.Select(x => x.Time);
        //            List<string> names = new List<string>(data.Count);
        //            List<string> values = new List<string>(data.Count);
        //            List<string> updateStrings = new List<string>(data.Count);

        //            for (int i = 0; i < data.First().Values.Count; i++)
        //            {
        //                long time = data.First().Values[i].Time;
        //                cmd.Parameters.AddWithValue("@time", time);

        //                values.Clear();
        //                updateStrings.Clear();

        //                foreach (var tag in data)
        //                {
        //                    // For insert
        //                    if (i == 0)
        //                        names.Add(tag.Name);
        //                    values.Add(tag.Values[i].Value.ToString());

        //                    // For update
        //                    updateStrings.Add(string.Format("`{0}`={1}", tag.Name, tag.Values[i].Value.ToString()));
        //                }

        //                // Try to update row
        //                cmd.CommandText = string.Format("UPDATE `data` SET {0} WHERE time = @time;",
        //                    string.Join(",", updateStrings));
        //                int x = cmd.ExecuteNonQuery();

        //                // But if that time row didn't exist, insert it
        //                if (x < 1)
        //                {
        //                    cmd.CommandText = string.Format("INSERT INTO `data` (`time`, {0}) VALUES(@time, {1});",
        //                    string.Join(",", names),
        //                    string.Join(",", values));
        //                    cmd.ExecuteNonQuery();
        //                }
        //            }
        //        }

        //        // Commit the transaction
        //        transaction.Commit();                
        //    }

        //    return data.Count * data.First().Values.Count;  // Computed
        //}
        //public static long UpdateFromData_Threaded(List<TagData> data, int threads = 2, SQLiteConnection conn = null)
        //{
        //    if (conn == null)
        //        conn = Connect();

        //    // Verify we have data
        //    if (data.Count < 1)
        //        return 0;

        //    // Ensure all tags exist in the database
        //    VerifyTagsExistFromTagData(data);

        //    // Ranges
        //    int CountPerThread = data.First().Values.Count / threads;

        //    // Create a task pool
        //    Task[] tasks = Enumerable.Range(0, threads).Select(i =>
        //        Task.Run(() =>
        //        {
        //            UpdateFromDataSeparated(data, i * CountPerThread, CountPerThread, conn);
        //        })).ToArray();

        //    // Wait on all the tasks.
        //    Task.WaitAll(tasks);

        //    // Compute return
        //    return data.Count * data.First().Values.Count;
        //}
        //private static void UpdateFromDataSeparated(List<TagData> data, int startIndex, int count, SQLiteConnection connection)
        //{
        //    if (data.Count < 1)
        //        return;

        //    // Start the bulk insert
        //    using (SQLiteTransaction transaction = connection.BeginTransaction())
        //    {
        //        using (SQLiteCommand cmd = new SQLiteCommand(connection))
        //        {
        //            cmd.CommandType = System.Data.CommandType.Text;
        //            cmd.Transaction = transaction;

        //            // Add tags and values
        //            // -- Assuming all values have the same time infos
        //            List<string> names = new List<string>(data.Count);
        //            List<string> values = new List<string>(data.Count);
        //            List<string> updateStrings = new List<string>(data.Count);

        //            // For Each Time Index / Database Row
        //            for (int i = startIndex; i < Math.Min(data.First().Values.Count, startIndex + count); i++)
        //            {
        //                long time = data.First().Values[i].Time;
        //                cmd.Parameters.AddWithValue("@time", time);

        //                values.Clear();
        //                updateStrings.Clear();

        //                foreach (var tag in data)
        //                {
        //                    // For insert
        //                    if (i == startIndex)
        //                        names.Add(tag.Name);

        //                    values.Add(tag.Values[i].Value.ToString());

        //                    // For update
        //                    updateStrings.Add(string.Format("`{0}`={1}", tag.Name, tag.Values[i].Value.ToString()));
        //                }

        //                // Try to update row
        //                cmd.CommandText = string.Format("UPDATE `data` SET {0} WHERE time = @time;",
        //                    string.Join(",", updateStrings));
        //                int x = cmd.ExecuteNonQuery();

        //                // But if that time row didn't exist, insert it
        //                if (x < 1)
        //                {
        //                    cmd.CommandText = string.Format("INSERT INTO `data` (`time`, {0}) VALUES(@time, {1});",
        //                    string.Join(",", names),
        //                    string.Join(",", values));
        //                    cmd.ExecuteNonQuery();
        //                }
        //            }
        //        }

        //        // Commit the transaction
        //        transaction.Commit();
        //    }
        //}
        //private static void VerifyTagsExistFromTagData(List<TagData> data)
        //{
        //    List<TagData> ExistingTags = GetAllTags();
        //    foreach (var tag in data)
        //    {
        //        if (ExistingTags.Find((t) => t.Name == tag.Name) == null)
        //            AddNewTag(tag);
        //    }
        //}
        public static long UpdateFromCSVData(CSVReader.CSVFile file)
        {
            if (file.Tags.Count < 1 || file.Rows.Count < 1)
                return 0;

            // -- Verify Tags Exist
            List<string> ExistingTags = GetAllTagNames();
            for (int i = 0; i < file.Tags.Count; i++)
            {
                if (!ExistingTags.Contains(file.Tags[i]))
                {
                    AddNewTag(new TagData() { Name = file.Tags[i], Description = file.Descriptions[i] });
                    ExistingTags.Add(file.Tags[i]);
                }
            }

            // Start the bulk insert
            List<string> updateStrings = new List<string>();
            long rowsAdded = 0;
            var connection = Connect();
            StringBuilder query = new StringBuilder();
            using (SQLiteTransaction transaction = connection.BeginTransaction())
            {
                using (SQLiteCommand updateCmd = connection.CreateCommand())
                {
                    using (SQLiteCommand insertCmd = connection.CreateCommand())
                    {
                        updateCmd.CommandType = System.Data.CommandType.Text;
                        updateCmd.Transaction = transaction;
                        insertCmd.CommandType = System.Data.CommandType.Text;
                        insertCmd.Transaction = transaction;

                        // For Each Time Index / Database Row
                        foreach (var row in file.Rows)
                        {
                            //insertCmd.CommandText = "INSERT OR IGNORE INTO `data`(`time`) VALUES(@time);";
                            updateCmd.Parameters.AddWithValue("@time", row.Time);
                            insertCmd.Parameters.AddWithValue("@time", row.Time);

                            //insertCmd.ExecuteNonQuery();

                            updateStrings.Clear();
                            for (int i = 0; i < row.Values.Count; i++)
                            {
                                updateStrings.Add(string.Format("`{0}`={1}", file.Tags[i], row.Values[i].ToString()));
                            }

                            // Update row                            
                            updateCmd.CommandText = string.Format("UPDATE `data` SET {0} WHERE time = @time;",
                                string.Join(",", updateStrings));                            
                            int x = updateCmd.ExecuteNonQuery();

                            //But if that time row didn't exist, insert it
                            if (x < 1)
                            {
                                insertCmd.CommandText = string.Format("INSERT INTO `data` (`time`, {0}) VALUES(@time, {1});",
                                string.Join(",", file.Tags),
                                string.Join(",", row.Values));
                                x = insertCmd.ExecuteNonQuery();
                            }
                            rowsAdded += x;
                        }
                    }
                }
                
                //Commit the transaction
                transaction.Commit();
            }
            return rowsAdded;
        }

        #endregion

        #region -- Files ---------------------------------------------------------------------------------------------------
        /// <summary>
        /// Returns from the database all the previously loaded files.
        /// These files should not be loaded again
        /// </summary>
        public static List<string> GetLoadedFiles()
        {
            List<string> files = new List<string>();

            SQLiteConnection connection = Connect();

            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "SELECT `path` FROM `files`;";
                cmd.CommandType = System.Data.CommandType.Text;

                using (var rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read() && !rdr.IsDBNull(0))
                        files.Add((string)rdr[0]);
                }
            }
            return files;
        }

        /// <summary>
        /// Returns whether this file's path is in the database as previously processed.
        /// </summary>
        /// <param name="path">The file to check</param>
        /// <returns>true if path is in database</returns>
        public static bool GetFileHasBeenRead(string path)
        {
            SQLiteConnection connection = Connect();
            using (SQLiteCommand cmd = new SQLiteCommand(connection))
            {
                cmd.CommandText = "SELECT COUNT(*) FROM `files` where `path`=@path;";
                cmd.Parameters.AddWithValue("@path", path);
                cmd.CommandType = System.Data.CommandType.Text;

                // If count does not equal 0, it is already loaded so skip this file
                var result = cmd.ExecuteScalar();
                if (result != DBNull.Value && (long)result > 0)
                    return true;
                return false;
            }
        }

        /// <summary>
        /// Adds a list of files to the database
        /// </summary>
        /// <param name="files">File paths to add</param>
        public static void AddFiles(IEnumerable<string> files)
        {
            if (files.Count() < 1)
                return;

            // Start a transaction
            SQLiteConnection connection = Connect();

            // Add the data to the database only where the last data time is above what we already have
            using (SQLiteTransaction transaction = connection.BeginTransaction())
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Transaction = transaction;
                    cmd.CommandType = System.Data.CommandType.Text;

                    foreach (var file in files)
                    {
                        cmd.CommandText = "INSERT OR IGNORE INTO files (`path`) VALUES(@path);";
                        cmd.Parameters.AddWithValue("@path", file);
                        cmd.ExecuteNonQuery();
                    }
                }

                // Commit the transaction
                transaction.Commit();
            }
        }
        public static void AddFile(string file)
        {
            AddFiles(new List<string> { file });
        }
        
        /// <summary>
        /// This mothod takes a list of files, processes them, and adds their data to the database. Also marks in the db
        /// that these files have been processed
        /// </summary>
        /// <param name="files">List of files to process and add</param>
        /// <param name="checkFiles">If false, files will be processed, regardless of whether they've already been processed before</param>
        /// <returns></returns>
        public async static Task<Tuple<int, long>> LoadAndAddFiles(IEnumerable<string> files, bool checkFiles = true, 
            IProgress<Tuple<int, int>> progress = null)
        {
            if (files.Count() < 1)
                return new Tuple<int,long>(0, 0);

            // Start a transaction
            SQLiteConnection connection = Connect();

            // TagData to add to the database
            List<string> filesToAdd = new List<string>();
            long addedRows = 0;
            int filesAdded = 0;

            // Add the data to the database only where the last data time is above what we already have
            using (SQLiteTransaction transaction = connection.BeginTransaction())
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Transaction = transaction;
                    cmd.CommandType = System.Data.CommandType.Text;

                    // -- Check if tese files are in the database before processing them
                    if (checkFiles)
                    {
                        foreach (var file in files)
                        {
                            if (!GetFileHasBeenRead(file)) // Skip file if its already in Database and if we're checking
                                filesToAdd.Add(file);
                        }
                    }
                    else
                        filesToAdd.AddRange(files);

                    // -- Prase and add the files' data to the database
                    CSVReader.LoadTagDescriptions();

                    await Task.Run(() =>
                    {
                        foreach (string file in filesToAdd)
                        {
                            CSVReader.CSVFile data = null;
                            try // to Parse
                            {                                
                                data = file.EndsWith("csv") ? CSVReader.ReadCSV(file) : CSVReader.ReadTXT(file);                                
                            }
                            catch (Exception e)
                            {
                                LogMsg?.Invoke(string.Format("Database.LoadAndAddFiles- Error while parsing / inserting file!\n\tFile Was: {0}\n\tError was: {1}\n\tFile was skipped.",
                                                                file,
                                                                e.Message));
                            }

                            // If parsed Okay
                            if (data != null)
                            {
                                // Insert
                                addedRows += UpdateFromCSVData(data);                                

                                // Log
                                cmd.CommandText = "INSERT OR IGNORE INTO files (`path`) VALUES(@path);";
                                cmd.Parameters.AddWithValue("@path", file);
                                cmd.ExecuteNonQuery();                                
                            }

                            filesAdded++;

                            // Report updated progress
                            if (progress != null)
                            {
                                progress.Report(new Tuple<int, int>(filesAdded, filesToAdd.Count));
                            }
                        }
                    });              
                }

                // Commit the transaction
                transaction.Commit();

                return new Tuple<int, long>(filesAdded, addedRows);
            }
        }

        /// <summary>
        /// This method checks a series of paths for new files in the database. If it finds any new files,
        /// it calls 'LoadAndAddFiles' on them to add them to the database
        /// </summary>
        /// <returns>Awaitable Tuple: Files added, Rows Added</returns>
        public async static Task<Tuple<int, long>> CheckPathsForNewFiles(IEnumerable<string> paths, IProgress<Tuple<int,int>> progress = null, int maxFiles = 0)
        {
            if (paths == null || paths.Count() < 1 || string.IsNullOrWhiteSpace(DatabasePath))
                return new Tuple<int, long>(0, 0);            

            // -- Get list of files in directory and see which haven't been read
            DateTime RetainTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).
                AddMonths((int)RetainMonths * -1);

            List<FileInfo> fileinfos = new List<FileInfo>();

            int filesAddedFromThisPath = 0;
            foreach (string path in paths)
            {
                filesAddedFromThisPath = 0;

                // FIX for the shitty folder layout on the Terminal Server
                // it currently has APL01 and APL02 data, in csv format, in the root directory, 
                // then folders for APL03 and APL04, which need to be read differently becuase the files
                //  are stored under folder by machine center
                //
                // It sucks but I think this is the best way                

                // -- Get list of files from last 3 months
                SearchOption opt = (path.ToLower().Contains("apl03") || path.ToLower().Contains("apl04")) ?
                    SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
                    
                var files = 
                    new DirectoryInfo(path).
                    GetFiles("*", opt).
                    Where(f => (f.Extension.ToLower() == ".csv" || f.Extension.ToLower() == ".txt") &&
                                f.CreationTime > RetainTime);

                // Include Only files that are not in the database already
                foreach (var file in files)
                {
                    // Only load max file count if provided
                    if (maxFiles > 0 && filesAddedFromThisPath > maxFiles)
                        break;

                    if (!GetFileHasBeenRead(file.FullName))
                    {
                        fileinfos.Add(file);
                        filesAddedFromThisPath++;
                    }
                }                                          
            }

            // Sort the files into a list of paths (So newest files get added first)
            fileinfos.Sort((x, y) => -1 * x.CreationTime.CompareTo(y.CreationTime));
            List<string> filesToAdd = new List<string>(fileinfos.Select(e => e.FullName));

            // If we have no files to add, we can return here
            if (filesToAdd.Count < 1)
                return new Tuple<int, long>(0, 0);

            int filesAdded = 0;
            long rowsAdded = 0;

            foreach (var files in SplitList(filesToAdd, 12))
            {
                var x = LoadAndAddFiles(files, false, new Progress<Tuple<int, int>>(prog =>
                {
                    if (progress != null)
                    {
                        progress.Report(new Tuple<int, int>(prog.Item1 + filesAdded, filesToAdd.Count));
                    }
                }));

                await x;

                filesAdded += x.Result.Item1;
                rowsAdded += x.Result.Item2;
            }
           
            return new Tuple<int, long>(filesAdded, rowsAdded);
        }

        private static List<CSVReader.CSVFile> MergeFiles(List<CSVReader.CSVFile> files)
        {
            List<CSVReader.CSVFile> ret = new List<CSVReader.CSVFile>();
            Stack<CSVReader.CSVFile> toProcess = new Stack<CSVReader.CSVFile>(files);

            // While there are files to process
            while (toProcess.Count > 0)
            {
                var file = toProcess.Pop();

                if (file.Rows.Count < 1 || file.Tags.Count < 1 || file.Tags.Count != file.Descriptions.Count)
                    continue;   // Exclude weird files

                if (ret.Count < 1)
                    ret.Add(file);
                else
                {
                    // See if an already inserted csv has the same tag line
                    foreach (var csv in ret)
                    {
                        // If tag string is equal
                        if (csv.Tags.Count == file.Tags.Count &&
                            csv.Tags.First() == file.Tags.First() &&
                            csv.Tags.Last() == file.Tags.Last())
                        {
                            csv.Rows.AddRange(file.Rows);
                            file = null;
                            break;
                        }
                    }

                    // If we didn't find a matching file, add it to the list
                    if (file != null)
                        ret.Add(file);
                }
            }

            // Now join all the files into one file for db insertion
            if (ret.Count < 1)
                return null;
            if (ret.Count == 1)
                return ret;

            toProcess = new Stack<CSVReader.CSVFile>(ret);
            ret = new List<CSVReader.CSVFile>();

            // While there are files to process
            while (toProcess.Count > 0)
            {
                var file = toProcess.Pop();

                if (file.Rows.Count < 1 || file.Tags.Count < 1 || file.Tags.Count != file.Descriptions.Count)
                    continue;   // Exclude weird files

                if (ret.Count < 1)
                    ret.Add(file);
                else
                {
                    // See if an already inserted csv has the same time starts
                    foreach (var csv in ret)
                    {
                        // If tag string is equal
                        if (csv.Rows.Count == file.Rows.Count &&
                            csv.Rows.First().Time == file.Rows.First().Time &&
                            csv.Rows.Last().Time == file.Rows.Last().Time)
                        {
                            file.Tags.AddRange(csv.Tags);
                            file.Descriptions.AddRange(csv.Descriptions);

                            for (int i = 0; i < csv.Rows.Count; i++)
                                file.Rows[i].Values.AddRange(csv.Rows[i].Values);

                            file = null;
                            break;
                        }
                    }

                    // If we didn't find a matching file, add it to the list
                    if (file != null)
                        ret.Add(file);
                }
            }
            return ret;
        }

        /// <summary>
        /// Splits an input list into a list of smaller lists: Chunking a list
        /// </summary>
        /// <param name="list">The List to split</param>
        /// <param name="nSize">The Preferred list of the smaller lists</param>
        /// <returns></returns>
        private static IEnumerable<List<T>> SplitList<T>(List<T> list, int nSize = 30)
        {
            for (int i = 0; i < list.Count; i += nSize)
            {
                yield return list.GetRange(i, Math.Min(nSize, list.Count - i));
            }
        }
        #endregion       
    }
}