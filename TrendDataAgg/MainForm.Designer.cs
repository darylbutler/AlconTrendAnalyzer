﻿namespace TrendDataAgg
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.WatchPathsListBox = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.RemovePathBtn = new System.Windows.Forms.Button();
            this.NewPathBtn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.QueryFileBtn = new System.Windows.Forms.Button();
            this.DefragBtn = new System.Windows.Forms.Button();
            this.DumpStoreBtn = new System.Windows.Forms.Button();
            this.DumpStorePathBox = new System.Windows.Forms.TextBox();
            this.DumpStoreBrowseBtn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.RemoveAfterParseCheck = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DatabasePathTextBox = new System.Windows.Forms.TextBox();
            this.DatabasePathBrowseBtn = new System.Windows.Forms.Button();
            this.BlacklistPathBox = new System.Windows.Forms.TextBox();
            this.BlacklistPathBrowseBtn = new System.Windows.Forms.Button();
            this.ArchivePathBox = new System.Windows.Forms.TextBox();
            this.ArchivePathBrowseBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.UpdateIntervalEdit = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.ActivityLog = new System.Windows.Forms.ListBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.StartBtn = new System.Windows.Forms.Button();
            this.StatusLbl = new System.Windows.Forms.Label();
            this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpdateIntervalEdit)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 342);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(830, 326);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.WatchPathsListBox);
            this.groupBox2.Controls.Add(this.panel2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 16);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(492, 307);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Watch Paths:";
            // 
            // WatchPathsListBox
            // 
            this.WatchPathsListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WatchPathsListBox.FormattingEnabled = true;
            this.WatchPathsListBox.HorizontalScrollbar = true;
            this.WatchPathsListBox.Location = new System.Drawing.Point(3, 16);
            this.WatchPathsListBox.Name = "WatchPathsListBox";
            this.WatchPathsListBox.Size = new System.Drawing.Size(486, 257);
            this.WatchPathsListBox.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.RemovePathBtn);
            this.panel2.Controls.Add(this.NewPathBtn);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 273);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(486, 31);
            this.panel2.TabIndex = 0;
            // 
            // RemovePathBtn
            // 
            this.RemovePathBtn.Location = new System.Drawing.Point(6, 3);
            this.RemovePathBtn.Name = "RemovePathBtn";
            this.RemovePathBtn.Size = new System.Drawing.Size(111, 23);
            this.RemovePathBtn.TabIndex = 1;
            this.RemovePathBtn.Text = "Remove Path";
            this.RemovePathBtn.UseVisualStyleBackColor = true;
            this.RemovePathBtn.Click += new System.EventHandler(this.RemovePathBtn_Click);
            // 
            // NewPathBtn
            // 
            this.NewPathBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NewPathBtn.Location = new System.Drawing.Point(123, 3);
            this.NewPathBtn.Name = "NewPathBtn";
            this.NewPathBtn.Size = new System.Drawing.Size(360, 23);
            this.NewPathBtn.TabIndex = 0;
            this.NewPathBtn.Text = "Add New Path...";
            this.NewPathBtn.UseVisualStyleBackColor = true;
            this.NewPathBtn.Click += new System.EventHandler(this.NewPathBtn_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox6);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(495, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(332, 307);
            this.panel1.TabIndex = 1;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.QueryFileBtn);
            this.groupBox6.Controls.Add(this.DefragBtn);
            this.groupBox6.Controls.Add(this.DumpStoreBtn);
            this.groupBox6.Controls.Add(this.DumpStorePathBox);
            this.groupBox6.Controls.Add(this.DumpStoreBrowseBtn);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Location = new System.Drawing.Point(0, 181);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(332, 107);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Dump Store To:";
            // 
            // QueryFileBtn
            // 
            this.QueryFileBtn.Location = new System.Drawing.Point(6, 75);
            this.QueryFileBtn.Name = "QueryFileBtn";
            this.QueryFileBtn.Size = new System.Drawing.Size(158, 23);
            this.QueryFileBtn.TabIndex = 4;
            this.QueryFileBtn.Text = "Query";
            this.QueryFileBtn.UseVisualStyleBackColor = true;
            this.QueryFileBtn.Click += new System.EventHandler(this.QueryFileBtn_Click);
            // 
            // DefragBtn
            // 
            this.DefragBtn.Location = new System.Drawing.Point(165, 46);
            this.DefragBtn.Name = "DefragBtn";
            this.DefragBtn.Size = new System.Drawing.Size(161, 23);
            this.DefragBtn.TabIndex = 3;
            this.DefragBtn.Text = "Defrag";
            this.DefragBtn.UseVisualStyleBackColor = true;
            this.DefragBtn.Click += new System.EventHandler(this.DefragBtn_Click);
            // 
            // DumpStoreBtn
            // 
            this.DumpStoreBtn.Location = new System.Drawing.Point(6, 46);
            this.DumpStoreBtn.Name = "DumpStoreBtn";
            this.DumpStoreBtn.Size = new System.Drawing.Size(158, 23);
            this.DumpStoreBtn.TabIndex = 2;
            this.DumpStoreBtn.Text = "Dump";
            this.DumpStoreBtn.UseVisualStyleBackColor = true;
            this.DumpStoreBtn.Click += new System.EventHandler(this.DumpStoreBtn_Click);
            // 
            // DumpStorePathBox
            // 
            this.DumpStorePathBox.Location = new System.Drawing.Point(6, 19);
            this.DumpStorePathBox.Name = "DumpStorePathBox";
            this.DumpStorePathBox.Size = new System.Drawing.Size(267, 20);
            this.DumpStorePathBox.TabIndex = 1;
            // 
            // DumpStoreBrowseBtn
            // 
            this.DumpStoreBrowseBtn.Location = new System.Drawing.Point(275, 17);
            this.DumpStoreBrowseBtn.Name = "DumpStoreBrowseBtn";
            this.DumpStoreBrowseBtn.Size = new System.Drawing.Size(51, 23);
            this.DumpStoreBrowseBtn.TabIndex = 0;
            this.DumpStoreBrowseBtn.Text = "Browse";
            this.DumpStoreBrowseBtn.UseVisualStyleBackColor = true;
            this.DumpStoreBrowseBtn.Click += new System.EventHandler(this.DumpStoreBrowseBtn_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.RemoveAfterParseCheck);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.DatabasePathTextBox);
            this.groupBox4.Controls.Add(this.DatabasePathBrowseBtn);
            this.groupBox4.Controls.Add(this.BlacklistPathBox);
            this.groupBox4.Controls.Add(this.BlacklistPathBrowseBtn);
            this.groupBox4.Controls.Add(this.ArchivePathBox);
            this.groupBox4.Controls.Add(this.ArchivePathBrowseBtn);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.UpdateIntervalEdit);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(332, 181);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Settings:";
            // 
            // RemoveAfterParseCheck
            // 
            this.RemoveAfterParseCheck.AutoSize = true;
            this.RemoveAfterParseCheck.Location = new System.Drawing.Point(6, 157);
            this.RemoveAfterParseCheck.Name = "RemoveAfterParseCheck";
            this.RemoveAfterParseCheck.Size = new System.Drawing.Size(201, 17);
            this.RemoveAfterParseCheck.TabIndex = 12;
            this.RemoveAfterParseCheck.Text = "Remove Files After Parse && Archiving";
            this.RemoveAfterParseCheck.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Blacklist Path:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Archive Path:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Database Path:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Update Interval (In Secs):";
            // 
            // DatabasePathTextBox
            // 
            this.DatabasePathTextBox.Location = new System.Drawing.Point(6, 32);
            this.DatabasePathTextBox.Name = "DatabasePathTextBox";
            this.DatabasePathTextBox.Size = new System.Drawing.Size(267, 20);
            this.DatabasePathTextBox.TabIndex = 7;
            // 
            // DatabasePathBrowseBtn
            // 
            this.DatabasePathBrowseBtn.Location = new System.Drawing.Point(275, 30);
            this.DatabasePathBrowseBtn.Name = "DatabasePathBrowseBtn";
            this.DatabasePathBrowseBtn.Size = new System.Drawing.Size(51, 23);
            this.DatabasePathBrowseBtn.TabIndex = 6;
            this.DatabasePathBrowseBtn.Text = "Browse";
            this.DatabasePathBrowseBtn.UseVisualStyleBackColor = true;
            this.DatabasePathBrowseBtn.Click += new System.EventHandler(this.DatabasePathBrowseBtn_Click);
            // 
            // BlacklistPathBox
            // 
            this.BlacklistPathBox.Location = new System.Drawing.Point(6, 131);
            this.BlacklistPathBox.Name = "BlacklistPathBox";
            this.BlacklistPathBox.Size = new System.Drawing.Size(267, 20);
            this.BlacklistPathBox.TabIndex = 5;
            // 
            // BlacklistPathBrowseBtn
            // 
            this.BlacklistPathBrowseBtn.Location = new System.Drawing.Point(275, 129);
            this.BlacklistPathBrowseBtn.Name = "BlacklistPathBrowseBtn";
            this.BlacklistPathBrowseBtn.Size = new System.Drawing.Size(51, 23);
            this.BlacklistPathBrowseBtn.TabIndex = 4;
            this.BlacklistPathBrowseBtn.Text = "Browse";
            this.BlacklistPathBrowseBtn.UseVisualStyleBackColor = true;
            this.BlacklistPathBrowseBtn.Click += new System.EventHandler(this.BlacklistPathBrowseBtn_Click);
            // 
            // ArchivePathBox
            // 
            this.ArchivePathBox.Location = new System.Drawing.Point(6, 93);
            this.ArchivePathBox.Name = "ArchivePathBox";
            this.ArchivePathBox.Size = new System.Drawing.Size(267, 20);
            this.ArchivePathBox.TabIndex = 3;
            // 
            // ArchivePathBrowseBtn
            // 
            this.ArchivePathBrowseBtn.Location = new System.Drawing.Point(275, 91);
            this.ArchivePathBrowseBtn.Name = "ArchivePathBrowseBtn";
            this.ArchivePathBrowseBtn.Size = new System.Drawing.Size(51, 23);
            this.ArchivePathBrowseBtn.TabIndex = 2;
            this.ArchivePathBrowseBtn.Text = "Browse";
            this.ArchivePathBrowseBtn.UseVisualStyleBackColor = true;
            this.ArchivePathBrowseBtn.Click += new System.EventHandler(this.ArchivePathBrowseBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(279, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "in Secs";
            // 
            // UpdateIntervalEdit
            // 
            this.UpdateIntervalEdit.Location = new System.Drawing.Point(153, 59);
            this.UpdateIntervalEdit.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.UpdateIntervalEdit.Name = "UpdateIntervalEdit";
            this.UpdateIntervalEdit.Size = new System.Drawing.Size(120, 20);
            this.UpdateIntervalEdit.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.ActivityLog);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(0, 68);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(830, 274);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Activity Log:";
            // 
            // ActivityLog
            // 
            this.ActivityLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ActivityLog.FormattingEnabled = true;
            this.ActivityLog.Location = new System.Drawing.Point(3, 16);
            this.ActivityLog.Name = "ActivityLog";
            this.ActivityLog.Size = new System.Drawing.Size(824, 255);
            this.ActivityLog.TabIndex = 0;
            this.ActivityLog.MouseLeave += new System.EventHandler(this.ActivityLog_MouseLeave);
            this.ActivityLog.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ActivityLog_MouseMove);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.StartBtn);
            this.panel3.Controls.Add(this.StatusLbl);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(830, 68);
            this.panel3.TabIndex = 3;
            // 
            // StartBtn
            // 
            this.StartBtn.Location = new System.Drawing.Point(3, 3);
            this.StartBtn.Name = "StartBtn";
            this.StartBtn.Size = new System.Drawing.Size(102, 59);
            this.StartBtn.TabIndex = 1;
            this.StartBtn.Text = "Start";
            this.StartBtn.UseVisualStyleBackColor = true;
            this.StartBtn.Click += new System.EventHandler(this.StartBtn_Click);
            // 
            // StatusLbl
            // 
            this.StatusLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.StatusLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusLbl.Location = new System.Drawing.Point(111, 9);
            this.StatusLbl.Name = "StatusLbl";
            this.StatusLbl.Size = new System.Drawing.Size(710, 48);
            this.StatusLbl.TabIndex = 0;
            this.StatusLbl.Text = "Status Label";
            this.StatusLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 668);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alcon Trend Data Aggregator - v1.0.1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpdateIntervalEdit)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button NewPathBtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown UpdateIntervalEdit;
        private System.Windows.Forms.Button RemovePathBtn;
        private System.Windows.Forms.ListBox WatchPathsListBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ListBox ActivityLog;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label StatusLbl;
        private System.Windows.Forms.ToolTip ToolTip;
        private System.Windows.Forms.Button StartBtn;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button DumpStoreBtn;
        private System.Windows.Forms.TextBox DumpStorePathBox;
        private System.Windows.Forms.Button DumpStoreBrowseBtn;
        private System.Windows.Forms.Button DefragBtn;
        private System.Windows.Forms.TextBox DatabasePathTextBox;
        private System.Windows.Forms.Button DatabasePathBrowseBtn;
        private System.Windows.Forms.TextBox BlacklistPathBox;
        private System.Windows.Forms.Button BlacklistPathBrowseBtn;
        private System.Windows.Forms.TextBox ArchivePathBox;
        private System.Windows.Forms.Button ArchivePathBrowseBtn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox RemoveAfterParseCheck;
        private System.Windows.Forms.Button QueryFileBtn;
    }
}

